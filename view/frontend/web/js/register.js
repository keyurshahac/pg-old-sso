/**
 * @copyright   2018 Orba. All rights reserved.
 */
define(
  ['Pg_Sso/js/register_base'],
  function (register) {
    return function (config, node) {
      return new register(config, node);
    };
  });