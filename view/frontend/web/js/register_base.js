/**
 * @copyright   2018 Orba. All rights reserved.
 */
define(
    [
        'jquery',
        'Pg_Customer/js/register_base'
    ],
    function ($, register) {
        var registerExtended = function (config, node) {
            var self = this;
            var defaultConfig = {
                customerSsoIdInput: 'input[name="customer_sso_id"]',
                responseCustomerSsoIdFieldName: 'customerDomainSso',
                responseDomainCustomerSsoIdFieldName: 'showField',
                additionalFieldSsoParentWrapper: '.additional-field.sso-field',
                additionalFieldSsoInactiveClass: 'inactive',

            };
            var extendedConfig = $.extend(true, {}, defaultConfig, config);
            register.call(self, extendedConfig, node);
        };

        registerExtended.prototype = Object.create(register.prototype);

        registerExtended.prototype.updateCustomerSsoIdField = function (config) {

            var element = $(this.config.customerSsoIdInput);
            if (!(config.hasOwnProperty(this.config.responseDomainCustomerSsoIdFieldName) && config[this.config.responseDomainCustomerSsoIdFieldName] === false)) {
                element.prop("disabled", false);
                element.closest(this.config.additionalFieldSsoParentWrapper).removeClass(this.config.additionalFieldSsoInactiveClass);
            } else {
                element.prop("disabled", true);
                element.closest(this.config.additionalFieldSsoParentWrapper).addClass(this.config.additionalFieldSsoInactiveClass);
            }
        };

        registerExtended.prototype.parseVerifyResponse = function (response) {
            register.prototype.parseVerifyResponse.call(this, response);

            if (response.hasOwnProperty(this.config.responseCustomerSsoIdFieldName)) {
                this.updateCustomerSsoIdField(response[this.config.responseCustomerSsoIdFieldName]);
            } else {
                this.updateCustomerSsoIdField(false);
            }
        };
        return registerExtended;
    });