<?php

namespace Pg\Sso\Block\Login;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Pg\Sso\Model\Config as SsoConfig;

class Saml extends Template
{
    const LOGIN_URL = '%s/saml/ssologin';

    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    public function __construct(
        Context $context,
        SsoConfig $ssoConfig,
        array $data = []
    ) {
        $this->ssoConfig = $ssoConfig;

        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        $routePath = sprintf(self::LOGIN_URL, SsoConfig::PREFIX);

        return $this->_urlBuilder->getUrl($routePath);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->ssoConfig->isSsoFrontendEnabled();
    }

    /**
     * @return string
     */
    public function getSamlText()
    {
        return $this->ssoConfig->getSamlTextFrontend();
    }

    /**
     * @return bool
     */
    public function isPasswordResetEnabled()
    {
        return $this->ssoConfig->isPasswordResetEnable();
    }

    /**
     * @return string
     */
    public function getPasswordResetUrl()
    {
        return $this->ssoConfig->getPasswordResetUrl();
    }

    /**
     * @return Template
     */
    protected function _prepareLayout()
    {
        $keepNativeLoginFrontend = $this->ssoConfig->keepFrontendSamlNativeLogin();
        if (false === $keepNativeLoginFrontend) {
            $this->pageConfig->getTitle()->set(__('Customer Login'));
        }

        return parent::_prepareLayout();
    }
}
