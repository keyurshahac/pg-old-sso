<?php

namespace Pg\Sso\Block\Login;

use Magento\Customer\Block\Form\Login\Info;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Registration;
use Magento\Customer\Model\Url;
use Magento\Checkout\Helper\Data as CheckoutData;
use Magento\Framework\Url\Helper\Data as CoreData;

class NativeInfo extends Info
{
    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * NativeInfo constructor.
     * @param SsoConfig $ssoConfig
     * @param Context $context
     * @param Registration $registration
     * @param Url $customerUrl
     * @param CheckoutData $checkoutData
     * @param CoreData $coreUrl
     * @param array $data
     */
    public function __construct(
        SsoConfig $ssoConfig,
        Context $context,
        Registration $registration,
        Url $customerUrl,
        CheckoutData $checkoutData,
        CoreData $coreUrl,
        array $data = []
    ) {
        parent::__construct($context, $registration, $customerUrl, $checkoutData, $coreUrl, $data);
        $this->ssoConfig = $ssoConfig;
    }

    public function toHtml()
    {
        if ($this->ssoConfig->isSsoFrontendEnabled() &&
            false === $this->ssoConfig->keepFrontendSamlNativeLogin()) {
            return '';
        }

        return parent::toHtml();
    }
}
