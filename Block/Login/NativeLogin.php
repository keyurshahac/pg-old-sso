<?php

namespace Pg\Sso\Block\Login;

use Magento\Customer\Block\Form\Login;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url;
use Pg\Sso\Model\Config as SsoConfig;

class NativeLogin extends Login
{
    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * NativeLogin constructor.
     * @param SsoConfig $ssoConfig
     * @param Context $context
     * @param Session $customerSession
     * @param Url $customerUrl
     * @param array $data
     */
    public function __construct(
        SsoConfig $ssoConfig,
        Context $context,
        Session $customerSession,
        Url $customerUrl,
        array $data = []
    ) {
        parent::__construct($context, $customerSession, $customerUrl, $data);
        $this->ssoConfig = $ssoConfig;
    }

    /**
     * @return string
     */
    public function toHtml()
    {
        if ($this->ssoConfig->isSsoFrontendEnabled() &&
            false === $this->ssoConfig->keepFrontendSamlNativeLogin()) {
            return '';
        }

        return parent::toHtml();
    }
}
