<?php

namespace Pg\Sso\Block\Adminhtml\Login;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Pg\Sso\Model\Config as SsoConfig;

class Saml extends Template
{
    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    public function __construct(
        Context $context,
        SsoConfig $ssoConfig,
        array $data = []
    ) {
        $this->ssoConfig = $ssoConfig;

        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function isEnbaled()
    {
        return $this->ssoConfig->isSsoAdminSamlEnabled();
    }

    /**
     * @return mixed
     */
    public function getSamlText()
    {
        return $this->ssoConfig->getSamlTextAdminhtml();
    }
}
