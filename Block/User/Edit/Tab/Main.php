<?php
namespace Pg\Sso\Block\User\Edit\Tab;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Locale\OptionInterface;
use Magento\User\Model\User;
use Pg\Security\Api\Provider\AdminFeaturesInterface;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Locale\ListsInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Backend\Block\Widget\Form as FormWidget;
use Magento\Framework\Data\Form;
use Magento\Framework\Data\Form\Element\Fieldset;

/**
 * Cms page edit form main tab
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class Main extends Generic
{
    /**
     * @var Session
     */
    protected $_authSession;

    /**
     * @var ListsInterface
     */
    protected $_LocaleLists;

    /**
     * Operates with deployed locales.
     *
     * @var OptionInterface
     */
    private $deployedLocales;

    /**
     * @var AdminFeaturesInterface
     */
    protected $adminFeaturesHelper;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Session $authSession
     * @param ListsInterface $localeLists
     * @param AdminFeaturesInterface $adminFeaturesHelper
     * @param OptionInterface $deployedLocales Operates with deployed locales.
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Session $authSession,
        ListsInterface $localeLists,
        AdminFeaturesInterface $adminFeaturesHelper,
        OptionInterface $deployedLocales = null,
        array $data = []
    ) {
        $this->_authSession = $authSession;
        $this->_LocaleLists = $localeLists;
        $this->deployedLocales = $deployedLocales
            ?: ObjectManager::getInstance()->get(OptionInterface::class);
        $this->adminFeaturesHelper = $adminFeaturesHelper;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @return FormWidget
     */
    protected function _prepareForm()
    {
        /** @var $model User */
        $model = $this->_coreRegistry->registry('permissions_user');

        /** @var Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('user_');

        $baseFieldset = $form->addFieldset('base_fieldset', ['legend' => __('Account Information')]);

        if ($model->getUserId()) {
            $baseFieldset->addField('user_id', 'hidden', ['name' => 'user_id']);
        } else {
            if (!$model->hasData('is_active')) {
                $model->setIsActive(1);
            }
        }

        $baseFieldset->addField(
            'username',
            'text',
            [
                'name' => 'username',
                'label' => __('User Name'),
                'id' => 'username',
                'title' => __('User Name'),
                'required' => true
            ]
        );

        $baseFieldset->addField(
            'firstname',
            'text',
            [
                'name' => 'firstname',
                'label' => __('First Name'),
                'id' => 'firstname',
                'title' => __('First Name'),
                'required' => true
            ]
        );

        $baseFieldset->addField(
            'lastname',
            'text',
            [
                'name' => 'lastname',
                'label' => __('Last Name'),
                'id' => 'lastname',
                'title' => __('Last Name'),
                'required' => true
            ]
        );

        $baseFieldset->addField(
            'email',
            'text',
            [
                'name' => 'email',
                'label' => __('Email'),
                'id' => 'customer_email',
                'title' => __('User Email'),
                'class' => 'required-entry validate-email',
                'required' => true
            ]
        );

        if ($this->adminFeaturesHelper->isNativePasswordUsed()) {
            $isNewObject = $model->isObjectNew();
            if ($isNewObject) {
                $passwordLabel = __('Password');
            } else {
                $passwordLabel = __('New Password');
            }
            $confirmationLabel = __('Password Confirmation');
            $this->_addPasswordFields($baseFieldset, $passwordLabel, $confirmationLabel, $isNewObject);
        }

        $baseFieldset->addField(
            'interface_locale',
            'select',
            [
                'name' => 'interface_locale',
                'label' => __('Interface Locale'),
                'title' => __('Interface Locale'),
                'values' => $this->deployedLocales->getOptionLocales(),
                'class' => 'select'
            ]
        );

        if ($this->_authSession->getUser()->getId() != $model->getUserId()) {
            $baseFieldset->addField(
                'is_active',
                'select',
                [
                    'name' => 'is_active',
                    'label' => __('This account is'),
                    'id' => 'is_active',
                    'title' => __('Account Status'),
                    'class' => 'input-select',
                    'options' => ['1' => __('Active'), '0' => __('Inactive')]
                ]
            );
        }

        $baseFieldset->addField('user_roles', 'hidden', ['name' => 'user_roles', 'id' => '_user_roles']);

        $data = $model->getData();
        unset($data['password']);
        $form->setValues($data);

        $this->setForm($form);

        $this->addPgSsoIdField($form);

        return parent::_prepareForm();
    }

    /**
     * Add password input fields
     *
     * @param Fieldset $fieldset
     * @param string $passwordLabel
     * @param string $confirmationLabel
     * @param bool $isRequired
     * @return void
     */
    protected function _addPasswordFields(
        Fieldset $fieldset,
        $passwordLabel,
        $confirmationLabel,
        $isRequired = false
    ) {
        $requiredFieldClass = $isRequired ? ' required-entry' : '';
        $fieldset->addField(
            'password',
            'password',
            [
                'name' => 'password',
                'label' => $passwordLabel,
                'id' => 'customer_pass',
                'title' => $passwordLabel,
                'class' => 'input-text validate-admin-password' . $requiredFieldClass,
                'required' => $isRequired
            ]
        );
        $fieldset->addField(
            'confirmation',
            'password',
            [
                'name' => 'password_confirmation',
                'label' => $confirmationLabel,
                'id' => 'confirmation',
                'title' => $confirmationLabel,
                'class' => 'input-text validate-cpassword' . $requiredFieldClass,
                'required' => $isRequired
            ]
        );
    }

    /**
     * Append a SSO field to form
     * @param Form $form
     */
    protected function addPgSsoIdField(Form $form)
    {
        /**
         * @var Form $form;
         */
        $form = $this->getForm();

        /**
         * @var User $model
         */
        $model = $this->_coreRegistry->registry('permissions_user');

        $baseFieldset = $form->getElement('base_fieldset');

        $baseFieldset->addField(
            'pg_sso_id',
            'text',
            [
                'name' => 'pg_sso_id',
                'label' => __('SSO ID'),
                'id' => 'pg_sso_id',
                'title' => __('SSO ID'),
                'required' => false
            ]
        );

        $form->addValues(["pg_sso_id" => $model->getPgSsoId()]);
    }
}
