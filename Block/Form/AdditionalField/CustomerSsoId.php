<?php

namespace Pg\Sso\Block\Form\AdditionalField;

use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Pg\Customer\Block\Form\Register;
use Pg\Sso\Model\Config as SsoConfig;

/**
 * Class CustomerSsoId
 * @package Pg\Customer\Block\Form\AdditionalField
 * @FIXME: Remove after Magento 2.2
 * @see view/frontend/templates/form/additional_fields/customer_sso_id.phtml
 * @see view/frontend/layout/customer_account_create.xml
 */
class CustomerSsoId extends Register
{

    protected $ssoConfig;

    /**
     * CustomerSsoId constructor.
     *
     * @param Context $context
     * @param \Magento\Directory\Helper\Data $directoryHelper
     * @param EncoderInterface $jsonEncoder
     * @param \Magento\Framework\App\Cache\Type\Config $configCacheType
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param \Pg\Content\Model\Config $contentConfig
     * @param SsoConfig $ssoConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Directory\Helper\Data $directoryHelper,
        EncoderInterface $jsonEncoder,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Url $customerUrl,
        \Pg\Content\Model\Config $contentConfig,
        SsoConfig $ssoConfig,

        array $data = []
    ) {
        parent::__construct(
            $context,
            $directoryHelper,
            $jsonEncoder,
            $configCacheType,
            $regionCollectionFactory,
            $countryCollectionFactory,
            $moduleManager,
            $customerSession,
            $customerUrl,
            $contentConfig,
            $data
        );

        $this->ssoConfig = $ssoConfig;
    }

    /**
     * @return bool
     */
    public function getIsShowSSOIdField()
    {
        return ($this->ssoConfig->isSsoFrontendEnabled()
            && !empty($this->ssoConfig->getDomainSso()));
    }
}