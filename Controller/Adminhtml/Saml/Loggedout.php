<?php

namespace Pg\Sso\Controller\Adminhtml\Saml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Pg\Sso\Model\Saml\AuthFactory;
use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Logger\Logger;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Loggedout
 * @package Pg\Sso\Controller\Adminhtml\Saml
 */
class Loggedout extends SamlAbstract
{
    /**
     * @var Raw;
     */
    protected $rawResult;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var Redirect
     */
    protected $redirect;

    /**
     * Loggedout constructor.
     * @param Context $context
     * @param Provider $provider
     * @param Logger $logger
     * @param Raw $rawResult
     * @param AuthFactory $authFactory
     * @param Redirect $redirect
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        Raw $rawResult,
        AuthFactory $authFactory,
        Redirect $redirect,
        PageFactory $pageFactory
    ) {
        parent::__construct($context, $provider, $logger);
        $this->rawResult = $rawResult;
        $this->authFactory = $authFactory;
        $this->redirect = $redirect;
        $this->pageFactory = $pageFactory;
    }


    /** This action is PUBLIC, keys NOT need to be valid
     * @return $this|ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        // Logged in user - redirect to dashboard
        if ($this->_auth->isLoggedIn()) {
            return $this->redirect->setUrl($this->getStartupPageUrl());
        }

        // Not logged in - just render a page;
        return $this->pageFactory->create();
    }

    /**
     * Not need to validate
     * @return bool
     */
    public function _processUrlKeys()
    {
        return true;
    }

    /**
     * Action is open
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
