<?php

namespace Pg\Sso\Controller\Adminhtml\Saml;

use Magento\Backend\App\Action\Context;
use Pg\Sso\Controller\Adminhtml\AbstractAction;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Saml\Properties\Provider as PropertiesProvider;

abstract class SamlAbstract extends AbstractAction
{
    /** @var PropertiesProvider */
    protected $propertiesProvider;

    /** @var array */
    protected $invalidRelays = [
        "pg_sso/saml/acs"
    ];

    /** @var Logger */
    protected $logger;

    /**
     * SamlAbstract constructor.
     * @param Context $context
     * @param Provider $provider
     */
    public function __construct(
        Context $context,
        PropertiesProvider $provider,
        Logger $logger
    ) {
        parent::__construct($context);

        $this->propertiesProvider = $provider;
        $this->logger = $logger;
    }
}
