<?php

namespace Pg\Sso\Controller\Adminhtml\Saml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Pg\Sso\Exception as SsoException;
use Pg\Sso\Exception\SamlException;
use Pg\Sso\Model\Admin\AuthenticatorFactory;
use Pg\Sso\Model\Admin\UserResolver;
use Pg\Sso\Model\Saml\Auth;
use Pg\Sso\Model\Saml\AuthFactory;
use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Logger\Logger;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Event\Manager as EventManager;
use Pg\Utils\Exception\ReasonableInterface;

/**
 * Class Index
 *
 * Access to this class (action) is public - user is always allowed only in case of not logged
 *
 * @package Pg\Sso\Controller\Adminhtml\Saml
 */
class Acs extends SamlAbstract
{
    /**
     * @var \Magento\Framework\Controller\Result\Raw;
     */
    protected $rawResult;

    /**
     * @var Provider
     */
    protected $propertiesProvider;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var Redirect
     */
    protected $redirect;

    /**
     * @var \Pg\Sso\Model\Admin\AuthenticatorFactory
     */
    protected $backendAuthenticatorFactory;

    /**
     * @var UserResolver
     */
    protected $backendUserResolver;

    /** @var EventManager */
    protected $eventManager;


    /**
     * Acs constructor.
     * @param Context $context
     * @param Provider $provider
     * @param Logger $logger
     * @param AuthFactory $authFactory
     * @param Redirect $redirect
     * @param AuthenticatorFactory $authenticatorFactory
     * @param UserResolver $backendUserResolver
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        AuthFactory $authFactory,
        Redirect $redirect,
        AuthenticatorFactory $authenticatorFactory,
        UserResolver $backendUserResolver,
        EventManager $eventManager
    ) {
        parent::__construct($context, $provider, $logger);
        $this->authFactory = $authFactory;
        $this->redirect = $redirect;
        $this->backendAuthenticatorFactory = $authenticatorFactory;
        $this->backendUserResolver = $backendUserResolver;
        $this->eventManager = $eventManager;
    }


    /** This action is public, keys need to be valid
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        /*
         * Some inputs
         */
        $settings = $this->propertiesProvider->get();
        $session = $this->_auth->getAuthStorage();
        $requestId = $session->getAuthNRequestID();

        try {
            $authObject = $this->authFactory->create($settings);
            $authObject->processResponse($requestId);

            $this->validateAuth($authObject);

            /**
             * Get user by NameID attribute in local system
             */
            $nameId = $authObject->getNameId();
            $sessionId = $authObject->getSessionIndex();

            $user = $this->backendUserResolver->getUser($nameId);

            /**
             * User Exists?
             */
            if (!$user->getId()) {
                throw new SsoException(__("Cannot find user by NameID %1", $nameId));
            }

            $authenticator = $this->backendAuthenticatorFactory->create([
                "user" => $user,
                "storage" => $session
            ]);

            /**
             * Do auth in local storage finally
             */
            $authenticator->authenticate();

            /**
             * Additional info (valid ony for SAML protocl)
             */
            $this->setExtraSamlParams($session, $nameId, $sessionId);

            /**
             * @todo Record login here
             */

        } catch (\Exception $exception) {
            $this->eventManager->dispatch(
                'backend_auth_user_login_failed',
                ['user_name' => $nameId ?? '', 'exception' => $exception]
            );
            $message = $exception->getMessage();
            if ($exception instanceof ReasonableInterface) {
                $message .= ', Reason: ' . $exception->getReason();
            }
            $this->logger->addError($message);
            $this->messageManager->addErrorMessage(__("Authentication Error"));
            return $this->redirectError($this->redirect);
        }


        return $this->redirectSuccess($this->redirect);
    }

    /**
     * @param Auth $authObject
     * @throws SsoException
     */
    protected function validateAuth(Auth $authObject)
    {
        $errors = $authObject->getErrors();
        /**
         * Any errors on SSO SAML?
         */
        if (count($errors)) {
            throw (new SamlException(__(join("; ", $errors))))->setReason($authObject->getLastErrorReason());
        }

        /**
         * Is really authenticated
         */
        if (!$authObject->isAuthenticated()) {
            throw new SsoException(__("Not Authenticated"));
        }
    }

    /**
     * @param Redirect $redirect
     * @return Redirect
     */
    protected function redirectSuccess(Redirect $redirect)
    {
        $relayState = $this->getRequest()->getParam('RelayState');
        if ($this->isRelayStateValid($relayState)) {
            $redirect->setUrl($relayState);
        } else {
            $redirect->setPath($this->_backendUrl->getStartupPageUrl());
        }
        return $redirect;
    }

    /**
     * @param Redirect $redirect
     * @return Redirect
     */
    protected function redirectError(Redirect $redirect)
    {
        return $redirect->setPath("*/*/loggedout");
    }

    /**
     * @param Session $session
     * @param $nameId
     * @param $sessionId
     */
    protected function setExtraSamlParams(Session $session, $nameId, $sessionId)
    {
        $session->setSamlSessionIndex($sessionId);
        $session->setSamlNameId($nameId);
    }

    /**
     * Action is open
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
