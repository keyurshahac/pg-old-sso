<?php

namespace Pg\Sso\Controller\Adminhtml\Saml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw;
use Pg\Sso\Exception as SsoException;
use Pg\Sso\Exception\SamlException;
use Pg\Sso\Model\Admin\AuthenticatorFactory;
use Pg\Sso\Model\Saml\Auth;
use Pg\Sso\Model\Saml\AuthFactory;
use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Logger\Logger;
use Magento\Backend\Model\Auth\Session;
use Magento\User\Model\User;

/**
 * @package Pg\Sso\Controller\Adminhtml\Saml
 */
class Slo extends SamlAbstract
{


    /**
     * @var Raw;
     */
    protected $rawResult;

    /**
     * @var Provider
     */
    protected $propertiesProvider;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var Redirect
     */
    protected $redirect;

    /**
     * @var AuthenticatorFactory
     */
    protected $backendAuthenticatorFactory;

    /**
     * @param Context $context
     * @param Provider $provider
     * @param Logger $logger
     * @param AuthFactory $authFactory
     * @param Redirect $redirect
     * @param AuthenticatorFactory $authenticatorFactory
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        AuthFactory $authFactory,
        Redirect $redirect,
        AuthenticatorFactory $authenticatorFactory
    ) {
        parent::__construct($context, $provider, $logger);
        $this->authFactory = $authFactory;
        $this->redirect = $redirect;
        $this->backendAuthenticatorFactory = $authenticatorFactory;
    }

    /**
     * This action is public, keys need to be valid
     * @return ResponseInterface
     */
    public function execute()
    {
        /*
         * Some inputs
         */
        $settings = $this->propertiesProvider->get();
        $session = $this->_auth->getAuthStorage();

        try {
            $authObject = $this->authFactory->create($settings);
            $authObject->processSLO(false, $session->getLogoutRequestID());
            $this->validateAuth($authObject);
            $this->logout($session, $session->getUser());
            $this->messageManager->addSuccessMessage(__('You have logged out.'));
            /**
             * @todo Record logout here
             */
        } catch (\Exception $exception) {
            $this->logger->addError($exception->getMessage());
            $this->messageManager->addErrorMessage("Authentication Error");
            return $this->redirectError($this->redirect);
        }

        return $this->redirectSuccess($this->redirect);
    }

    /**
     * @param Redirect $redirect
     * @return mixed
     */
    protected function redirectSuccess(Redirect $redirect)
    {
        return $redirect->setPath("*/*/loggedout");
    }


    /**
     * @param Redirect $redirect
     * @return mixed
     */
    protected function redirectError(Redirect $redirect)
    {
        return $redirect->setPath('index');
    }

    /**
     * @param Auth $authObject
     * @throws SsoException
     */
    protected function validateAuth(Auth $authObject)
    {
        $errors = $authObject->getErrors();
        /**
         * Any errors on SSO SAML?
         */
        if (count($errors)) {
            throw (new SamlException(__(join("; ", $errors))))->setReason($authObject->getLastErrorReason());
        }
    }

    /** Do logout
     * @param Session $session
     * @param User $user
     */
    protected function logout(Session $session, User $user)
    {
        $authenticator = $this->backendAuthenticatorFactory->create([
            "user" => $user,
            "storage" => $session
        ]);
        $authenticator->logout();
        $session->setSamlSessionIndex(null);
        $session->setSamlNameId(null);
    }

    /**
     * Keys are no available here
     * But SLO validation prevent ot CSRF
     * @return bool
     */
    protected function _validateSecretKey()
    {
        return true;
    }
}
