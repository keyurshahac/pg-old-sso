<?php

namespace Pg\Sso\Controller\Adminhtml\Saml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw as RawResult;
use Magento\Framework\Controller\ResultInterface;
use Pg\Sso\Model\Saml\SettingsFactory;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Config as SsoConfig;

/**
 * Class Index
 *
 * Access to this class (aciton) is public - user is always allowed only in case of not logged
 *
 * @package Pg\Sso\Controller\Adminhtml\Saml
 */
class Metadata extends SamlAbstract
{
    /**
     * @var RawResult
     */
    private $rawResult;

    /**
     * @var SettingsFactory
     */
    private $settingsFactory;

    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * Metadata constructor.
     * @param Context $context
     * @param Provider $provider
     * @param Logger $logger
     * @param RawResult $rawResult
     * @param SettingsFactory $settingsFactory
     * @param SsoConfig $ssoConfig
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        RawResult $rawResult,
        SettingsFactory $settingsFactory,
        SsoConfig $ssoConfig
    ) {
        parent::__construct($context, $provider, $logger);

        $this->rawResult = $rawResult;
        $this->settingsFactory = $settingsFactory;
        $this->ssoConfig = $ssoConfig;
    }


    /** This action is PUBLIC, keys NOT need to be valid
     * @return $this|ResponseInterface|ResultInterface
     * @throws \Exception
     * @throws \OneLogin_Saml2_Error
     */
    public function execute()
    {
        // SSO Disabled - Do not show meta (in open mode also)
        if (!$this->ssoConfig->isSsoAdminSamlEnabled()) {
            return $this->rawResult->setHttpResponseCode(404);
        }
        $props = $this->propertiesProvider->get();
        $settings = $this->settingsFactory->create($props);
        $this->rawResult->setHeader('content-type', 'application/xml');

        return $this->rawResult->setContents($settings->getSPMetadata());
    }

    /**
     * Not need to validate
     * @return bool
     */
    public function _processUrlKeys()
    {
        return true;
    }

    /**
     * Action is open
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
