<?php

namespace Pg\Sso\Controller\Adminhtml\Saml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultInterface;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Logger\Logger;

/**
 * Class Index
 *
 * Access to this class (aciton) is public - user is always allowed only in case of not logged
 *
 * @package Pg\Sso\Controller\Adminhtml\Saml
 */
class Ssologout extends SamlAbstract
{
    /**
     * @var Raw;
     */
    protected $rawResult;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var SsoConfig
     */
    protected $ssoConfig;

    /**
     * @var Redirect
     */
    protected $redirect;


    /**
     * Ssologout constructor.
     * @param Context $context
     * @param Provider $provider
     * @param Logger $logger
     * @param Raw $rawResult
     * @param AuthFactory $authFactory
     * @param SsoConfig $ssoConfig
     * @param Redirect $redirect
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        Raw $rawResult,
        AuthFactory $authFactory,
        SsoConfig $ssoConfig,
        Redirect $redirect
    ) {
        parent::__construct($context, $provider, $logger);
        $this->rawResult = $rawResult;
        $this->authFactory = $authFactory;
        $this->ssoConfig = $ssoConfig;
        $this->redirect = $redirect;
    }


    /** This action is PRIVATE, keys need to be valid
     * @return $this|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        if (!$this->canProcess()) {
            return $this->redirect->setPath("admin/index/index");
        }

        $session = $this->_auth->getAuthStorage();
        $props = $this->propertiesProvider->get();

        try {
            $auth = $this->authFactory->create($props);
            // This method will end script
            $auth->logout(null, [], $session->getSamlNameId(), $session->getSamlSessionIndex());
        } catch (\Exception $ex) {
            $this->logger->addError($ex->getMessage());
            $this->messageManager->addErrorMessage(__("Some SSO error occurred."));
        }

        return $this->redirect->setPath("*");
    }

    /**
     * @return bool
     */
    protected function canProcess()
    {
        if (!$this->_auth->isLoggedIn()) {
            return false;
        }
        // SSO Disabled - Redirect to login page
        if (!$this->ssoConfig->isSloAdminSamlEnabled()) {
            return false;
        }

        return true;
    }
}
