<?php

namespace Pg\Sso\Controller\Adminhtml\Saml;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Helper\Data as BackendHelper;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw as RawResult;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Properties\Provider;

/**
 * Class Index
 *
 * Access to this class (aciton) is public - user is always allowed only in case of not logged
 *
 * @package Pg\Sso\Controller\Adminhtml\Saml
 */
class Ssologin extends SamlAbstract
{
    /** @var RawResult */
    protected $rawResult;

    /** @var AuthFactory */
    protected $authFactory;

    /** @var SsoConfig */
    protected $ssoConfig;

    /** @var Redirect */
    protected $redirect;

    /** @var Context */
    private $context;

    /**
     * Ssologin constructor.
     * @param Context $context
     * @param Provider $provider
     * @param Logger $logger
     * @param RawResult $rawResult
     * @param AuthFactory $authFactory
     * @param SsoConfig $ssoConfig
     * @param Redirect $redirect
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        RawResult $rawResult,
        AuthFactory $authFactory,
        SsoConfig $ssoConfig,
        Redirect $redirect
    ) {
        parent::__construct($context, $provider, $logger);

        $this->rawResult = $rawResult;
        $this->authFactory = $authFactory;
        $this->ssoConfig = $ssoConfig;
        $this->redirect = $redirect;
        $this->context = $context;
    }

    /**
     * This action is open, keys need to be valid
     * @return $this|ResponseInterface|RawResult|ResultInterface
     */
    public function execute()
    {
        // Currenly logged - go to dashboard
        if ($this->_auth->isLoggedIn()) {
            return $this->redirect->setUrl($this->getStartupPageUrl());
        }
        // SSO Disabled - Redirect to login page
        if (!$this->ssoConfig->isSsoAdminSamlEnabled()) {
            return $this->redirect->setPath("*");
        }
        $props = $this->propertiesProvider->get();

        try {
            $auth = $this->authFactory->create($props);
            /** @var BackendHelper $backendHelper */
            $backendHelper = $this->context->getHelper();
            $auth->login($backendHelper->getHomePageUrl());
        } catch (\Exception $ex) {
            $this->logger->addError($ex->getMessage());
            $this->messageManager->addErrorMessage(__("Some SSO error occurred."));

            return $this->redirect->setPath("*");
        }

        return $this->rawResult;
    }

    /**
     * Action is open
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
