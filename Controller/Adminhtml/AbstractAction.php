<?php

namespace Pg\Sso\Controller\Adminhtml;

use Magento\Backend\App\Action;

abstract class AbstractAction extends Action
{

    /**
     * @var array
     */
    protected $invalidRelays = [];

    /**
     * @param $url
     * @return bool
     */
    protected function isRelayStateValid($url){

        if(is_null($url)){
            return false;
        }

        if(0 !== strpos($url, $this->_backendUrl->getBaseUrl())){
            return false;
        }

        foreach($this->invalidRelays as $path){
            if(0 !== strpos($this->_backendUrl->getUrl($path), $url)){
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    protected function getStartupPageUrl(){
        return $this->_backendUrl->getStartupPageUrl();
    }

}
