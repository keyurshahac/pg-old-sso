<?php

namespace Pg\Sso\Controller\Saml;

use Pg\Sso\Exception as SsoException;
use Pg\Sso\Exception\SamlException;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Auth;
use Magento\Framework\App\Action\Context;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Customer\Authenticator;
use Pg\Sso\Helper\Auth as SsoHelper;

class Slo extends SamlAbstractAction
{
    /** @var AuthFactory */
    protected $authFactory;
    /** @var Authenticator */
    protected $authenticator;
    /** @var SsoHelper */
    protected $ssoHelper;

    /**
     * @param Context $context
     * @param Provider $provider
     * @param Logger $logger
     * @param AuthFactory $authFactory
     * @param Authenticator $authenticator
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        AuthFactory $authFactory,
        Authenticator $authenticator,
        SsoHelper $ssoHelper
    ) {
        parent::__construct($context, $provider, $logger);
        $this->authFactory = $authFactory;
        $this->authenticator = $authenticator;
        $this->ssoHelper = $ssoHelper;
    }

    public function execute()
    {
        try {
            $settings = $this->propertiesProvider->get();
            $authObject = $this->authFactory->create($settings);
            $authObject->processSLO();
            $this->validateAuth($authObject);
            $this->authenticator->logout();
            $this->ssoHelper->setSsoFlag(false);
        } catch (\Exception $e) {
            $this->logger->addError('logout Error');
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_redirect('/');
        }

        $this->_redirect('*/*/loggedout');
    }


    /**
     * @param Auth $authObject
     *
     * @throws SsoException
     */
    protected function validateAuth(Auth $authObject){
        $errors = $authObject->getErrors();
        /**
         * Any errors on SSO SAML?
         */
        if(count($errors)){
            throw (new SamlException(__(join("; ", $errors))))->setReason($authObject->getLastErrorReason());
        }
    }
}