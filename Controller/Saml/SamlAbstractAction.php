<?php

namespace Pg\Sso\Controller\Saml;

use Magento\Framework\App\Action\Context;
use Pg\Sso\Model\Saml\Properties\Provider as FrontendProvider;
use Pg\Sso\Logger\Logger;
use Magento\Framework\App\Action\Action;

abstract class SamlAbstractAction extends Action
{
    /**
     * @var FrontendProvider
     */
    protected $propertiesProvider;

    /**
     * @var Logger
     */
    protected $logger;


    /**
     * SamlAbstract constructor.
     * @param Context $context
     * @param FrontendProvider $provider
     */
    public function __construct(
        Context $context,
        FrontendProvider $provider,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->propertiesProvider = $provider;
        $this->logger = $logger;
    }
}
