<?php

namespace Pg\Sso\Controller\Saml;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url;
use Magento\Framework\Event\Manager as EventManager;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Exception as SsoException;
use Pg\Sso\Exception\SamlException;
use Pg\Sso\Model\Saml\Auth;
use Magento\Framework\App\Action\Context;
use Pg\Sso\Model\Saml\Properties\Provider as FrontendhtmlProvider;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Customer\Authenticator;
use Pg\Sso\Model\Customer\CustomerResolver;
use Pg\Utils\Exception\ReasonableInterface;
use Pg\Sso\Helper\Auth as SsoHelper;
use Pg\Customer\Exception\NoAccessToSAPException;

/**
 * Class Acs
 * @package   Pg\Sso\Controller\Saml
 * @copyright Copyright (c) 2018 Orba Sp. z o.o.  (http://orba.co)
 */
class Acs extends SamlAbstractAction
{
    const SSO_LOGIN_ERROR = 'Your account does not have a T-number assigned. Please contact your administrator.';

    /** @var Session */
    protected $session;

    /** @var AuthFactory */
    protected $authFactory;

    /** @var Authenticator */
    protected $authenticator;

    /** @var CustomerResolver */
    protected $customerResolver;

    /** @var SsoHelper */
    protected $ssoHelper;

    /** @var EventManager */
    protected $eventManager;

    /**
     * Acs constructor.
     *
     * @param Context $context
     * @param FrontendhtmlProvider $provider
     * @param Logger $logger
     * @param Session $session
     * @param AuthFactory $authFactory
     * @param Authenticator $authenticator
     * @param CustomerResolver $customerResolver
     * @param EventManager $eventManager
     * @param SsoHelper $ssoHelper
     */
    public function __construct(
        Context $context,
        FrontendhtmlProvider $provider,
        Logger $logger,
        Session $session,
        AuthFactory $authFactory,
        Authenticator $authenticator,
        CustomerResolver $customerResolver,
        EventManager $eventManager,
        SsoHelper $ssoHelper
    ) {
        parent::__construct($context, $provider, $logger);
        $this->session = $session;
        $this->authFactory = $authFactory;
        $this->authenticator = $authenticator;
        $this->customerResolver = $customerResolver;
        $this->eventManager = $eventManager;
        $this->ssoHelper = $ssoHelper;
    }


    public function execute()
    {
        $settings = $this->propertiesProvider->get();

        try {
            $authObject = $this->authFactory->create($settings);
            $authObject->processResponse();

            $this->validateAuth($authObject);

            $nameId = $authObject->getNameId();
            $customer = $this->customerResolver->getCustomer($nameId);

            $this->authenticator->authenticate($customer);

            $this->ssoHelper->setSsoFlag();

            $this->eventManager->dispatch('customer_login_success', [
                'login' => $nameId,
                'message'=>'Customer logged in via SSO'
            ]);

        } catch (NoAccessToSAPException $e) {
            $this->logger->addError($e->getMessage());
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->_redirect(Url::ROUTE_ACCOUNT_LOGIN);
        } catch (\Exception $e) {
            $this->eventManager->dispatch(
                'customer_login_failed',
                ['login' => $nameId ?? '', 'message' => "Authentication Error", 'exception' => $e]
            );

            $message = $e->getMessage();
            if ($e instanceof ReasonableInterface) {
                $message .= ', Reason: ' . $e->getReason();
            }
            $this->logger->addError($message);
            $this->messageManager->addErrorMessage(__(self::SSO_LOGIN_ERROR));
            return $this->_redirect('/');
        }

        return $this->_redirect('/');
    }

    /**
     * @param Auth $authObject
     * @throws SsoException
     */
    protected function validateAuth(Auth $authObject){
        $errors = $authObject->getErrors();
        /**
         * Any errors on SSO SAML?
         */
        if(count($errors)){
            throw (new SamlException(__(join("; ", $errors))))->setReason($authObject->getLastErrorReason());
        }

        /**
         * Is really authenticated
         */
        if(!$authObject->isAuthenticated()){
            throw new SsoException(__("Not Authenticated"));
        }
    }

}