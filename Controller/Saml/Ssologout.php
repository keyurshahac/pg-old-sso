<?php

namespace Pg\Sso\Controller\Saml;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Helper\Auth as SsoHelper;

/**
 * Class Index
 *
 * Access to this class (aciton) is public - user is always allowed only in case of not logged
 *
 * @package Pg\Sso\Controller\Saml
 */
class Ssologout extends SamlAbstractAction
{
    /**
     * @var Raw
     */
    private $rawResult;

    /**
     * @var AuthFactory
     */
    private $authFactory;

    /**
     * @var SsoConfig
     */
    private $ssoConfig;
    /** @var SsoHelper */
    protected $ssoHelper;

    /**
     * Ssologin constructor.
     * @param Context $context
     * @param Provider $provider
     * @param Raw $rawResult
     * @param AuthFactory $authFactory
     * @param SsoConfig $ssoConfig
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        Raw $rawResult,
        AuthFactory $authFactory,
        SsoConfig $ssoConfig,
        SsoHelper $ssoHelper
    ) {
        parent::__construct($context, $provider, $logger);
        $this->rawResult = $rawResult;
        $this->authFactory = $authFactory;
        $this->ssoConfig = $ssoConfig;
        $this->ssoHelper = $ssoHelper;
    }


    /**
     * This action is PRIVATE, keys need to be valid
     * @return ResponseInterface
     */
    public function execute()
    {
        if (!$this->ssoConfig->isSsoFrontendEnabled()) {
            return $this->_redirect("/");
        }

        $props = $this->propertiesProvider->get();

        try {
            $this->ssoHelper->setSsoFlag(false);
            $authObject = $this->authFactory->create($props);
            // This method will end script
            $authObject->logout(null, [], $authObject->getNameId(), $authObject->getSessionIndex());
        } catch (\Exception $e) {
            $this->logger->addError($e->getMessage());
            $this->messageManager->addErrorMessage(__("Some SSO error occurred."));
        }

        return $this->_redirect('*/*/loggedout');
    }
}
