<?php

namespace Pg\Sso\Controller\Saml;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Pg\Sso\Model\Saml\SettingsFactory;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Logger\Logger;
use Magento\Framework\Controller\Result\Raw;

/**
 * Class Index
 *
 * Access to this class (aciton) is public - user is always allowed only in case of not logged
 *
 * @package Pg\Sso\Controller\Saml
 */
class Metadata extends SamlAbstractAction
{
    /**
     * @var Raw
     */
    protected $rawResult;

    /**
     * @var SettingsFactory
     */
    protected $settingsFactory;

    /**
     * @var SsoConfig
     */
    protected $ssoConfig;


    /**
     * Metadata constructor.
     * @param Context $context
     * @param Provider $provider
     * @param Logger $logger
     * @param Raw $rawResult
     * @param SettingsFactory $settingsFactory
     * @param SsoConfig $ssoConfig
     */
    public function __construct(
        Context $context,
        Provider $provider,
        Logger $logger,
        Raw $rawResult,
        SettingsFactory $settingsFactory,
        SsoConfig $ssoConfig
    ) {
        parent::__construct($context, $provider, $logger);
        $this->rawResult = $rawResult;
        $this->settingsFactory = $settingsFactory;
        $this->ssoConfig = $ssoConfig;
    }


    /** This action is PUBLIC, keys NOT need to be valid
     * @return $this|ResponseInterface|ResultInterface
     * @throws \Exception
     * @throws \OneLogin_Saml2_Error
     */
    public function execute()
    {
        // SSO Disabled - Do not show meta (in open mode also)
        if (!$this->ssoConfig->isSsoFrontendEnabled()) {
            return $this->rawResult->setHttpResponseCode(404);
        }
        $props = $this->propertiesProvider->get();
        $settings = $this->settingsFactory->create($props);
        $this->rawResult->setHeader('content-type', 'application/xml');

        return $this->rawResult->setContents($settings->getSPMetadata());
    }
}
