<?php

namespace Pg\Sso\Controller\Saml;

use Magento\Customer\Model\Session as UserSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw as RawResult;
use Magento\Framework\Controller\ResultInterface;
use Magento\Store\Model\StoreManagerInterface;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Properties\Provider as PropertiesProvider;

class Ssologin extends SamlAbstractAction
{
    /** @var SsoConfig */
    private $ssoConfig;

    /** @var UserSession */
    private $userSession;

    /** @var AuthFactory */
    private $authFactory;

    /** @var RawResult */
    private $rawResult;

    /** @var PropertiesProvider */
    protected $propertiesProvider;

    /** @var StoreManagerInterface */
    private $storeManager;

    /**
     * Ssologin constructor.
     * @param Context $context
     * @param Logger $logger
     * @param SsoConfig $ssoConfig
     * @param UserSession $userSession
     * @param PropertiesProvider $propertiesProvider
     * @param AuthFactory $authFactory
     * @param RawResult $rawResult
     */
    public function __construct(
        Context $context,
        Logger $logger,
        SsoConfig $ssoConfig,
        UserSession $userSession,
        PropertiesProvider $propertiesProvider,
        AuthFactory $authFactory,
        RawResult $rawResult,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context, $propertiesProvider, $logger);

        $this->ssoConfig = $ssoConfig;
        $this->userSession = $userSession;
        $this->authFactory = $authFactory;
        $this->rawResult = $rawResult;
        $this->propertiesProvider = $propertiesProvider;
        $this->storeManager = $storeManager;
    }

    /**
     * @return ResponseInterface|RawResult|ResultInterface
     */
    public function execute()
    {
        if ($this->userSession->isLoggedIn()) {
            return $this->_redirect('/');
        }
        if (false === $this->ssoConfig->isSsoFrontendEnabled()) {
            return $this->_redirect('customer/account/login/');
        }
        $props = $this->propertiesProvider->get();
        try {
            $auth = $this->authFactory->create($props);
            $store = $this->storeManager->getStore();
            $auth->login($store->getBaseUrl());
        } catch (\Exception $ex) {
            $this->logger->addError($ex->getMessage());
            $this->messageManager->addErrorMessage(__("Some SSO error occurred."));
            return $this->_redirect('customer/account/login/');
        }

        return $this->rawResult;
    }
}
