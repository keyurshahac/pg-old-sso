<?php
/**
 * Created by PhpStorm.
 * User: besch
 * Date: 21.06.17
 * Time: 14:34
 */

namespace Pg\Sso\Controller\Saml;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Pg\Sso\Model\Saml\Properties\Provider as FrontendProvider;
use Pg\Sso\Logger\Logger;
use Magento\Customer\Model\Session as UserSession;

class Loggedout extends SamlAbstractAction
{
    /** @var PageFactory */
    protected $pageFactory;

    /** @var UserSession */
    protected $userSession;

    public function __construct(
        Context $context,
        FrontendProvider $provider,
        Logger $logger,
        PageFactory $pageFactory,
        UserSession $userSession
    ) {
        parent::__construct($context, $provider, $logger);
        $this->pageFactory = $pageFactory;

        $this->userSession = $userSession;
    }


    public function execute()
    {
        if($this->userSession->isLoggedIn()){
            return $this->_redirect('/');
        }

        // Not logged in - just render a page;
        return $this->pageFactory->create();

    }
}