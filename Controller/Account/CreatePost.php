<?php

namespace Pg\Sso\Controller\Account;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Customer\Model\Metadata\FormFactory;
use Magento\Customer\Model\Registration;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlFactory;
use Magento\Store\Model\StoreManagerInterface;
use Pg\Customer\Api\HierarchyManagementInterface;
use Pg\Customer\Api\RoleManagementInterface;
use Pg\Sso\Helper\ControllerHelper\CheckCustomerSso;
use Pg\Customer\Helper\Sap\Structure;
use Pg\Customer\Model\Config;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Customer\Model\SapManagement;
use Psr\Log\LoggerInterface;

/**
 * Created by PhpStorm.
 * User: ksulek
 * Date: 22.10.18
 * Time: 16:10
 */
class CreatePost extends \Pg\Customer\Controller\Account\CreatePost
{

    /** @var CheckCustomerSso */
    protected $checkCustomerSso;

    /** @var SsoConfig */
    protected $ssoConfig;

    public function __construct(
        Context $context,
        Session $customerSession,
        StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManagement,
        UrlFactory $urlFactory,
        FormFactory $formFactory,
        CustomerUrl $customerUrl,
        Registration $registration,
        CustomerExtractor $customerExtractor,
        JsonFactory $jsonFactory,
        Structure $structure,
        Config $config,
        SapManagement $sapManagement,
        RoleManagementInterface $roleManagement,
        HierarchyManagementInterface $hierarchyManagement,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        LoggerInterface $logger,
        SsoConfig $ssoConfig,
        CheckCustomerSso $checkCustomerSso
    ) {
        $this->checkCustomerSso = $checkCustomerSso;
        $this->ssoConfig = $ssoConfig;

        parent::__construct($context, $customerSession, $storeManager, $accountManagement, $urlFactory, $formFactory,
            $customerUrl, $registration, $customerExtractor, $jsonFactory, $structure, $config, $sapManagement,
            $roleManagement, $hierarchyManagement, $customerRepository, $searchCriteriaBuilder, $logger);
    }

    /**
     * Prepare json response with cart data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    protected function prepareJsonResponse() : \Magento\Framework\Controller\Result\Json
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $resultJson->setData([
            'status' => $this->getStatus(),
            'errorMessages' => $this->getErrorMessages(),
            'errorFields' => $this->errorFields,
            'salesRepConfig' => $this->getSalesRepresentativeConfig(),
            'customerDomainSso' => $this->getCustomerSsoIdConfig(),
        ]);

        return $resultJson;
    }

    protected function getSalesRepresentativeConfig() : array
    {
        $res = $this->getCustomerSsoIdConfig();

        if ($res['showField'] === false) {
            return parent::getSalesRepresentativeConfig();
        }

        return [
            'showField' => false
        ];
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    protected function getCustomerSsoIdConfig() : array
    {
        $website = $this->storeManager->getWebsite();

        $email = $this->getRequest()->getParam('email');

        if (empty($email)) {
            return [
                'showField' => false
            ];
        }

        $domainSso = $this->ssoConfig->getDomainSso($website->getId());

        if (!$domainSso) {
            return [
                'showField' => false
            ];
        }

        return [
            'showField' => $this->checkCustomerSso->checkCustomerSsoId($email, $domainSso)
        ];
    }

}