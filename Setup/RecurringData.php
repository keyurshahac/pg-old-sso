<?php
/**
 * @copyright Copyright (c) 2018 Orba Sp. z o.o. (http://orba.co)
 */

namespace Pg\Sso\Setup;

use Magento\Customer\Api\Data\AttributeMetadataInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Pg\Sso\Model\Config\Source\Attribute;
use Pg\Utils\Helper\Installer;

/**
 * Upgrade registered themes and theme related data
 */
class RecurringData implements InstallDataInterface
{
    /** @var Installer */
    protected $installer;

    /** @var CustomerSetupFactory */
    private $customerSetupFactory;

    /**
     * UpgradeData constructor.
     * @param Installer $installer
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        Installer $installer,
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->installer = $installer;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->ensureSsoIdInCustomerGrid($setup);

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    private function ensureSsoIdInCustomerGrid(ModuleDataSetupInterface $setup)
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $customerSetup->updateAttribute(
            Customer::ENTITY,
            Attribute::SSO_ID,
            [
                AttributeMetadataInterface::IS_USED_IN_GRID => true,
                AttributeMetadataInterface::IS_VISIBLE_IN_GRID => true,
                AttributeMetadataInterface::IS_FILTERABLE_IN_GRID => true,
                AttributeMetadataInterface::IS_SEARCHABLE_IN_GRID => true,
            ]
        );
    }
}
