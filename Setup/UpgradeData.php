<?php

namespace Pg\Sso\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Pg\Sso\Model\Config\Source\Attribute;
use Pg\Utils\Helper\Installer;

class UpgradeData implements UpgradeDataInterface
{
    /** @var Installer */
    protected $installer;

    /**
     * UpgradeData constructor.
     * @param Installer $installer
     */
    public function __construct(
        Installer $installer
    ) {
        $this->installer = $installer;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws LocalizedException
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.8', '<')) {
            $this->addSsoIdAttribute($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @throws LocalizedException
     */
    private function addSsoIdAttribute(ModuleDataSetupInterface $setup)
    {
        $this->installer->addCustomerAttribute(
            $setup,
            Attribute::SSO_ID,
            [
                'type' => Table::TYPE_TEXT,
                'label' => 'SSO ID',
                'length' => 64,
                'input' => Table::TYPE_TEXT,
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'sort_order' => 1000,
                'position' => 1000,
                'system' => 0
            ],
            [
                'adminhtml_customer',
                'customer_account_create',
                'customer_account_edit'
            ]
        );
    }
}
