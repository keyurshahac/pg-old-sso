<?php
namespace Pg\Sso\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Security\Setup\InstallSchema as AdminInstallSchema;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /** @var CustomerSetupFactory */
    private $customerSetupFactory;

    /** @var SetFactory */
    private $attributeSetFactory;

    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param SetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        SetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.3', '<')) {

            $adminTable = AdminInstallSchema::ADMIN_USER_DB_TABLE_NAME;

            //Order table
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($adminTable),
                    'pg_sso_id',
                    [
                        'type' => Table::TYPE_TEXT,
                        'length' => 64,
                        'comment' => 'SSO ID'
                    ]
                );
        }


        $setup->endSetup();
    }
}
