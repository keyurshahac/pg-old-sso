<?php

namespace Pg\Sso\App\Action\Plugin\Adminhtml;

use Magento\Framework\Exception\AuthenticationException;

class Authentication extends \Magento\Backend\App\Action\Plugin\Authentication
{
    public function __construct(
        \Magento\Backend\Model\Auth $auth,
        \Magento\Backend\Model\UrlInterface $url,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Backend\App\BackendAppList $backendAppList,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
    ) {
        parent::__construct(
            $auth,
            $url,
            $response,
            $actionFlag,
            $messageManager,
            $backendUrl,
            $resultRedirectFactory,
            $backendAppList,
            $formKeyValidator
        );

        // Append open SAML actions
        $this->_openActions[] = "metadata";
        $this->_openActions[] = "ssologin";
        $this->_openActions[] = "acs";
        $this->_openActions[] = "loggedout";
        /** @todo append more */
    }
}
