<?php

namespace Pg\Sso\Model\Config\Source\Saml;

use \Magento\Framework\Option\ArrayInterface;
use \Magento\Framework\Phrase;

class NameId implements ArrayInterface
{

    /**
     * Available options
     */
    const NAMEID_UNSPECIFIED = 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified';
    const NAMEID_TRANSIENT   = 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient';
    const NAMEID_PERSISTENT  = 'urn:oasis:names:tc:SAML:2.0:attrname-format:persistent';
    const NAMEID_EMAIL       ='urn:oasis:names:tc:SAML:2.0:attrname-format:email';

    /**
     * Do translate of options
     * @return array
     */
    public function toOptionArray(){
        return [
            static::NAMEID_UNSPECIFIED => new Phrase('unspecified'),
            static::NAMEID_TRANSIENT => new Phrase('transient'),
            static::NAMEID_PERSISTENT => new Phrase('persistent'),
            static::NAMEID_EMAIL => new Phrase('email')
        ];
    }

}