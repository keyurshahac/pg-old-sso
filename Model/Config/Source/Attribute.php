<?php

namespace Pg\Sso\Model\Config\Source;

use \Magento\Framework\Option\ArrayInterface;
use \Magento\Framework\Phrase;

class Attribute implements ArrayInterface
{

    /**
     * Available options
     */
    const EMAIL     = "email";
    const USERNAME  = "username";
    const SSO_ID    = "customer_sso_id";

    /**
     * Do translate of options
     * @return array
     */
    public function toOptionArray(){
        return [
            static::EMAIL => new Phrase("Email"),
            static::USERNAME => new Phrase("Username"),
            static::SSO_ID => new Phrase("SSO ID"),
        ];
    }

}