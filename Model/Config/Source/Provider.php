<?php

namespace Pg\Sso\Model\Config\Source;

use \Magento\Framework\Option\ArrayInterface;

class Provider implements ArrayInterface
{

    /**
     * Available providers
     */
    const NONE = "";
    const SAML = "saml";
    const OAUTH = "oauth";

    /**
     * @var array
     */
    protected $_options = [];
    
    /**
     * @param array $options - array of options
     */
    public function __construct(array $options=array()){
        $this->_options = $options;
    }

    /**
     * Do translate of options
     * @return array
     */
    public function toOptionArray(){
        return array_map(array($this, "translate"), $this->_options);
    }

    /**
     * @param $input
     * @return mixed
     */
    protected function translate($input){
        return new \Magento\Framework\Phrase($input);
    }
}