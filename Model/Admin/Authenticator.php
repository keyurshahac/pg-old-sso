<?php

/**
 * This class saves authentication state of given user in authentication storage
 */
namespace Pg\Sso\Model\Admin;

use Magento\Backend\Model\Auth\Session;
use Magento\User\Model\User;
use Magento\Framework\Event\ManagerInterface;
use Magento\Backend\Model\Auth as BackendAuth;
use Pg\Sso\Exception;

class Authenticator extends BackendAuth
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * Authenticator constructor.
     * @param ManagerInterface $eventManager
     * @param User|StorageInterface $user
     * @param Session $session
     */
    public function __construct(
        ManagerInterface $eventManager,
        User $user,
        Session $session)
    {
        $this->user = $user;
        $this->session = $session;
        $this->eventManager = $eventManager;
    }

    /**
     * Just to be able to call plugins and trigger events
     * @param string $username - not used
     * @param string $password - not used
     * @return void
     */
    public function login($username, $password)
    {
        $this->eventManager->dispatch(
            'backend_auth_user_login_success',
            ['user' => $this->user]
        );
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function authenticate(){

        // Do validation
        if(!$this->isUserValid($this->user)){
            throw new Exception(__("User not valid"));
        }

        // Native auth
        $this->session->setUser($this->user);
        $this->session->processLogin();

        // Authenticated from sso
        $this->session->setSsoAuth(true);

        // Call plugins, events... etc...
        $this->login($this->user->getUserName(), null);

        return true;
    }

    /**
     * @return Session
     */
    public function logout()
    {
        return $this->session->processLogout();
    }

    /**
     * @return bool
     */
    protected function isUserValid(User $user){
        return !!$this->user->getId();
    }

}