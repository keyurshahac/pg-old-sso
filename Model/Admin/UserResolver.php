<?php
/**
 * Created by PhpStorm.
 * User: maciejbabolmac
 * Date: 21.02.2017
 * Time: 15:27
 */

namespace Pg\Sso\Model\Admin;

use Magento\Framework\App\Area;
use Magento\Framework\DataObject;
use Magento\User\Model\ResourceModel\User\CollectionFactory;
use Magento\User\Model\ResourceModel\User\Collection;
use Pg\Sso\Model\Config as SsoConfig;

class UserResolver
{

    /**
     * @var CollectionFactory
     */
    private $userCollectionFactory;

    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * @var string
     */
    private $attributeConfig;


    /**
     * UserResolver constructor.
     * @param CollectionFactory $userCollectionFactory
     * @param SsoConfig $ssoConfig
     * @param null $attributeConfig
     */
    public function __construct(
        CollectionFactory $userCollectionFactory,
        SsoConfig $ssoConfig,
        $attributeConfig = null
    ) {
        $this->userCollectionFactory = $userCollectionFactory;
        $this->ssoConfig = $ssoConfig;
        $this->attributeConfig = $attributeConfig;
    }

    /**
     * @param $id
     * @return DataObject
     */
    public function getUser($id)
    {
        $field = $this->ssoConfig->getConfig(
            Area::AREA_ADMINHTML,
            $this->attributeConfig
        );

        $userCollection = $this->userCollectionFactory->create();
        /**
         * @var $userCollection Collection
         */
        $userCollection->addFieldToFilter($field, $id)->addFieldToFilter('is_active', 1);

        return $userCollection->getFirstItem();
    }
}
