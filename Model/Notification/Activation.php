<?php

namespace Pg\Sso\Model\Notification;

use Pg\Customer\Model\Notification\Activation as CustomerActivation;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Pg\Customer\Api\NotificationInterface;
use Pg\Customer\Api\HierarchyManagementInterface;
use Pg\Customer\Helper\Email as EmailHelper;
use Pg\Customer\Model\Notification\Framework\Mail\Template\TransportBuilder;
use Psr\Log\LoggerInterface;
use Pg\Utils\Helper\DataObject;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Customer\Model\Notification\Config;

/**
 * Class Activation
 * @package   Pg\Sso\Model\Notification
 * @copyright Copyright (c) 2018 Orba Sp. z o.o.  (http://orba.co)
 */
class Activation extends CustomerActivation
{
    const XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE_SSO = 'customer/create_account/employee_registration_confirmed_sso_email_template';

    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    public function __construct(
        EmailHelper $emailHelper,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        DataObject $dataObjectHelper,
        Config $config,
        HierarchyManagementInterface $hierarchyManagement,
        LoggerInterface $logger,
        SsoConfig $ssoConfig
    ) {
        parent::__construct(
            $emailHelper,
            $transportBuilder,
            $storeManager,
            $dataObjectHelper,
            $config,
            $hierarchyManagement,
            $logger
        );
        $this->ssoConfig = $ssoConfig;
    }

    /**
     * Specify template path
     * based on website configuration and SSO ID form field filled.
     *
     * Assumptions:
     *
     * - if frontend has only sso login enabled
     * don't send email with link to new password setting
     *
     * - if frontend has only native login
     * don't send sso templated email
     *
     * - if both versions of login are enabled
     * send sso templated email only if SSO ID provided
     *
     * @return string
     */
    protected function getTemplate()
    {
        $loginType = $this->ssoConfig->getFrontendLoginType($this->customer->getWebsiteId());

        if ($loginType === SsoConfig::LOGIN_TYPE_SSO_ONLY ||
            ($loginType === SsoConfig::LOGIN_TYPE_BOTH && $this->isCustomerSsoId())) {
            return $this->config->getByPath(self::XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE_SSO);
        }

        return $this->config->getByPath(self::XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE);
    }

    /**
     * Returns account type name based on code.
     *
     * @return string
     */
    protected function isCustomerSsoId()
    {
        $customerSsoId = $this->dataObjectHelper->getCustomAttributeValue(
            $this->customer,
            'customer_sso_id'
        );

        return !empty($customerSsoId);
    }

}