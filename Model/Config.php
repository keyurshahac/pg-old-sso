<?php

namespace Pg\Sso\Model;

use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Pg\Sso\Model\Config\Source\Provider;

class Config
{
    const PROVIDER = "provider";
    const PREFIX = "pg_sso";

    const XML_PATH_SAML_KEEP_ORIGIN_LOGIN = "saml_keep_origin";
    const XML_PATH_SAML_ATTRIBUTE = "saml_attribute";
    const XML_PATH_SAML_ENTITY = "saml_entity";
    const XML_PATH_SAML_SSO = "saml_sso";
    const XML_PATH_SAML_SLO = "saml_slo";
    const XML_PATH_SAML_CERT_DATA = "saml_certdata";
    const XML_PATH_SAML_TEXT = "saml_text";
    const XML_PATH_SAML_NAME_ID = "saml_nameid";
    const XML_PATH_SAML_RESET_PASSWORD_ENABLED = "saml_reset_password";
    const XML_PATH_SAML_RESET_PASSWORD_URL = "saml_reset_password_url";
    const XML_CONFIG_PATH_DOMAIN_SSO_ID = 'domain_sso_id';
    const LOGIN_TYPE_NATIVE_ONLY = 'native_only';
    const LOGIN_TYPE_SSO_ONLY = 'sso_only';
    const LOGIN_TYPE_BOTH = 'both';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isSsoAdminEnabled()
    {
        return Provider::NONE !== $this->getSsoAdminProvider();
    }

    /**
     * @return bool
     */
    public function isSsoAdminSamlEnabled()
    {
        return Provider::SAML === $this->getSsoAdminProvider();
    }

    /**
     * @todo check why SSO for frontend is only SAML (unlike in isSsoAdminEnabled() isSsoAdminSamlEnabled())
     * @param null $websiteId
     * @return bool
     */
    public function isSsoFrontendEnabled($websiteId = null)
    {
        return Provider::SAML === $this->getSsoFrontEndProvider($websiteId);
    }

    /**
     * @param null $websiteId
     * @return bool
     */
    public function isSloFrontendEnabled($websiteId = null)
    {
        $sloUrl = $this->getSamlSloFrontend();

        return (bool)$this->isSsoFrontendEnabled($websiteId) && !empty($sloUrl);
    }

    /**
     * @return bool
     */
    public function keepAdminSamlNativeLogin()
    {
        return (bool)$this->getConfig(Area::AREA_ADMINHTML, static::XML_PATH_SAML_KEEP_ORIGIN_LOGIN);
    }

    /**
     * @return bool
     */
    public function isSloAdminSamlEnabled()
    {
        return (bool)$this->getConfig(Area::AREA_ADMINHTML, static::XML_PATH_SAML_SLO);
    }

    /**
     * @param null|int $websiteId
     * @return bool
     */
    public function keepFrontendSamlNativeLogin($websiteId = null)
    {
        return (bool)$this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_KEEP_ORIGIN_LOGIN,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null|int $websiteId
     * @return bool
     */
    public function isPasswordResetEnable($websiteId = null)
    {
        $isResetPasswordEnabled = $this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_RESET_PASSWORD_ENABLED,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
        $passwordResetUrl = $this->getPasswordResetUrl($websiteId);
        $isKeepNativeLoginEnabled = $this->keepFrontendSamlNativeLogin();

        return $isResetPasswordEnabled && !empty($passwordResetUrl) && !$isKeepNativeLoginEnabled;
    }

    /**
     * @param null|int $websiteId
     * @return string
     */
    public function getPasswordResetUrl($websiteId = null)
    {
        return (string)$this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_RESET_PASSWORD_URL,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlAttributeFrontend($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_ATTRIBUTE,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlEntityFrontend($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_ENTITY,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlEntityAdminhtml($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_ADMINHTML,
            static::XML_PATH_SAML_ENTITY,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlNameIdFrontend($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_NAME_ID,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlNameIdAdminhtml($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_ADMINHTML,
            static::XML_PATH_SAML_NAME_ID,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlSsoFrontend($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_SSO,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlSsoAdminhtml($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_ADMINHTML,
            static::XML_PATH_SAML_SSO,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getSamlSloFrontend($storeId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_SLO,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlSloAdminhtml($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_ADMINHTML,
            static::XML_PATH_SAML_SLO,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlCertDataFrontend($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_CERT_DATA,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $websiteId
     * @return mixed
     */
    public function getSamlCertDataAdminhtml($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_ADMINHTML,
            static::XML_PATH_SAML_CERT_DATA,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getSamlTextFrontend($storeId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            static::XML_PATH_SAML_TEXT,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getSamlTextAdminhtml($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_ADMINHTML,
            static::XML_PATH_SAML_TEXT,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * @param $area
     * @param $key
     * @param string $scope
     * @param null $scopeCode
     * @return mixed
     * @deprecated External method usage is discouraged.
     * @todo Visibility should be changed to 'private'
     */
    public function getConfig($area, $key, $scope = ScopeInterface::SCOPE_WEBSITE, $scopeCode = null)
    {
        $key = sprintf("%s/%s/%s", static::PREFIX, $area, $key);

        return $this->scopeConfig->getValue($key, $scope, $scopeCode);
    }


    /**
     * @param null $websiteCode
     * @return null|string
     */
    public function getDomainSso($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            self::XML_CONFIG_PATH_DOMAIN_SSO_ID,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );

    }


    /**
     * @return mixed
     */
    private function getSsoAdminProvider()
    {
        return $this->getConfig(Area::AREA_ADMINHTML, static::PROVIDER);
    }

    /**
     * @param null|string $websiteId
     * @return mixed
     */
    private function getSsoFrontEndProvider($websiteId = null)
    {
        return $this->getConfig(
            Area::AREA_FRONTEND,
            static::PROVIDER,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * Determine which kind of login is set on the frondend
     * now for given website (Native, SSO or both of them)
     *
     * @param int $websiteId
     * @return string
     */
    public function getFrontendLoginType($websiteId)
    {
        if (!$this->isSsoFrontendEnabled($websiteId)) {
            return self::LOGIN_TYPE_NATIVE_ONLY;
        }

        if (!$this->keepFrontendSamlNativeLogin($websiteId)) {
            return self::LOGIN_TYPE_SSO_ONLY;
        }

        return self::LOGIN_TYPE_BOTH;
    }
}
