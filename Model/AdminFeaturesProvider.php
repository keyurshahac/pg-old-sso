<?php

namespace Pg\Sso\Model;

use Pg\Security\Api\Provider\AdminFeaturesInterface;

class AdminFeaturesProvider implements AdminFeaturesInterface
{
    /** @var Config */
    private $config;

    /**
     * AdminFeaturesProvider constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function isNativePasswordUsed()
    {
        return !$this->config->isSsoAdminEnabled() || $this->config->keepAdminSamlNativeLogin();
    }
}
