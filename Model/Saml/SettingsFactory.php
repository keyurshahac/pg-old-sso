<?php
namespace Pg\Sso\Model\Saml;

class SettingsFactory
{

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        $instanceName = 'Pg\\Sso\\Model\\Saml\\Settings')
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }

    /**
     * @param array $settings
     * @return \Pg\Sso\Model\Saml\Settings
     */
    public function create(array $settings = [])
    {
        return $this->_objectManager->create(
            $this->_instanceName,
            ['settings'=>$settings]
        );
    }

}