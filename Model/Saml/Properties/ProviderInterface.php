<?php

namespace Pg\Sso\Model\Saml\Properties;

interface ProviderInterface
{
    /**
     * @return array
     */
    public function get();
}
