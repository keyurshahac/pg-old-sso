<?php

namespace Pg\Sso\Model\Saml\Properties;

use Magento\Framework\App\Area;
use Magento\Framework\UrlInterface;
use Pg\Sso\Model\Config as SsoConfig;

class Provider implements ProviderInterface
{
    const PATH_SAML_METADATA = '*/saml/metadata';
    const PATH_SAML_ACS = '*/saml/acs';
    const PATH_SAML_SLO = '*/saml/slo';
    /**
     * @var string
     */
    private $area;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * Provider constructor.
     * @param $area
     * @param UrlInterface $url
     * @param SsoConfig $ssoConfig
     */
    public function __construct(
        $area,
        UrlInterface $url,
        SsoConfig $ssoConfig
    ) {
        $this->area = $area;
        $this->url = $url;
        $this->ssoConfig = $ssoConfig;
    }

    /**
     * @inheritdoc
     */
    public function get()
    {
        $samlMetadataUrl = $this->url->getUrl(static::PATH_SAML_METADATA, ['key' => false]);
        $samlAcsUrl = $this->url->getUrl(static::PATH_SAML_ACS, ['key' => false]);
        $samlSloUrl = $this->url->getUrl(static::PATH_SAML_SLO, ['key' => false]);

        /**
         * We can process only 2 areas: frontend and adminhtml here
         */
        $samlEntityId = $this->isFront() ?
            $this->ssoConfig->getSamlEntityFrontend() :
            $this->ssoConfig->getSamlEntityAdminhtml();
        $samlNameId = $this->isFront() ?
            $this->ssoConfig->getSamlNameIdFrontend() :
            $this->ssoConfig->getSamlNameIdAdminhtml();
        $samlSso = $this->isFront() ?
            $this->ssoConfig->getSamlSsoFrontend() :
            $this->ssoConfig->getSamlSsoAdminhtml();
        $samsSlo = $this->isFront() ?
            $this->ssoConfig->getSamlSloFrontend() :
            $this->ssoConfig->getSamlSloAdminhtml();
        $samlCertData = $this->isFront() ?
            $this->ssoConfig->getSamlCertDataFrontend() :
            $this->ssoConfig->getSamlCertDataAdminhtml();

        return [
            'sp' => [
                'entityId' => $samlMetadataUrl,
                'assertionConsumerService' => ['url' => $samlAcsUrl],
                'singleLogoutService' => ['url' => $samlSloUrl],
                'NameIDFormat' => $samlNameId,
            ],
            'idp' => [
                'entityId' => $samlEntityId,
                'singleSignOnService' => ['url' => $samlSso],
                'singleLogoutService' => ['url' => $samsSlo],
                'x509cert' => $samlCertData,
            ],
            // This is security setting only for PG IdP
            'security' => [
                'requestedAuthnContext' => false,
            ]
        ];
    }

    /**
     * @return bool
     */
    private function isFront()
    {
        return  Area::AREA_FRONTEND === $this->area;
    }
}
