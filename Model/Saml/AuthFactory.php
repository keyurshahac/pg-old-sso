<?php

namespace Pg\Sso\Model\Saml;


class AuthFactory
{

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_scopeResolver = null;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        $instanceName = Auth::class)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }


    /**
     * @param $settings
     * @return Auth
     */
    public function create(array $settings)
    {
        return $this->_objectManager->create(
            $this->_instanceName,
            ['oldSettings'=>$settings]
        );
    }

}