<?php

namespace Pg\Sso\Model\Installer\CustomerSso;

use Pg\SampleData\Model\InstallerAbstract;
use Magento\Framework\App\ResourceConnection;
use Pg\SampleData\Model\DataGetter;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Pg\Customer\Api\CustomerManagementInterface;
use Magento\Customer\Model\CustomerFactory;

/**
 * Class Data
 * @package Pg\Sso\Model\Installer\CustomerSso
 */
class Data extends InstallerAbstract
{
    /**
     * for identify the customer
     */
    const ROW_ENTITY_ID = 'entity_id';

    /**
     * new SSO ID value
     */
    const ROW_SSO_ID = 'sso_id';

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var DataGetter
     */
    protected $dataGetter;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var CustomerManagementInterface
     */
    protected $customerManagement;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Data constructor.
     * @param DataGetter $dataGetter
     * @param ResourceConnection $resourceConnection
     * @param CustomerFactory $customerFactory
     * @param CustomerManagementInterface $customerManagement
     * @param LoggerInterface $logger
     * @param ConsoleLogger $consoleLogger
     */
    public function __construct(
        DataGetter $dataGetter,
        ResourceConnection $resourceConnection,
        CustomerFactory $customerFactory,
        CustomerManagementInterface $customerManagement,
        LoggerInterface $logger,
        ConsoleLogger $consoleLogger
    ) {
        $this->dataGetter = $dataGetter;
        $this->resourceConnection = $resourceConnection;
        $this->customerFactory = $customerFactory;
        $this->customerManagement = $customerManagement;
        $this->logger = $logger;

        parent::__construct($consoleLogger);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $data = $this->dataGetter->loadFromCSV($this->filePath);

        foreach ($data as $row) {
            $customerId = $row[self::ROW_ENTITY_ID];
            $ssoId = $row[self::ROW_SSO_ID];

            try {
                $customer = $this->customerFactory->create()->load($customerId);

                if ($customer->getCustomerSsoId() == $ssoId) {
                    continue;
                }

                $this->customerManagement->saveCustomerDataById($customerId, [\Pg\Sso\Model\Config\Source\Attribute::SSO_ID => $ssoId]);

            } catch (\Exception $exception) {
                $this->logger->debug($exception);
            }
        }

        return true;
    }
}