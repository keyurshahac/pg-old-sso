<?php

namespace Pg\Sso\Model\Installer\AdminSso;

use Pg\SampleData\Model\InstallerAbstract;
use Magento\Framework\App\ResourceConnection;
use Pg\SampleData\Model\DataGetter;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Magento\User\Model\UserFactory;

/**
 * Class Data
 * @package Pg\Sso\Model\Installer\AdminSso
 */
class Data extends InstallerAbstract
{
    /**
     * for identify the admin user
     */
    const ROW_USER_ID = 'user_id';

    /**
     * new SSO ID value
     */
    const ROW_PG_SSO_ID = 'pg_sso_id';

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var DataGetter
     */
    protected $dataGetter;

    /**
     * @var UserFactory
     */
    protected $userFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Data constructor.
     * @param DataGetter $dataGetter
     * @param ResourceConnection $resourceConnection
     * @param UserFactory $userFactory
     * @param LoggerInterface $logger
     * @param ConsoleLogger $consoleLogger
     */
    public function __construct(
        DataGetter $dataGetter,
        ResourceConnection $resourceConnection,
        UserFactory $userFactory,
        LoggerInterface $logger,
        ConsoleLogger $consoleLogger
    ) {
        $this->dataGetter = $dataGetter;
        $this->resourceConnection = $resourceConnection;
        $this->userFactory = $userFactory;
        $this->logger = $logger;

        parent::__construct($consoleLogger);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $data = $this->dataGetter->loadFromCSV($this->filePath);

        foreach ($data as $row) {
            $userId = $row[self::ROW_USER_ID];
            $ssoId = $row[self::ROW_PG_SSO_ID];

            try {

                $user = $this->userFactory->create()->load($userId);

                if ($user->getPgSsoId() == $ssoId) {
                    continue;
                }

                $user->setPgSsoId($ssoId);
                $user->save();
            } catch (\Exception $exception) {
                $this->logger->debug($exception);
            }
        }

        return true;
    }
}