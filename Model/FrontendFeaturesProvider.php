<?php

namespace Pg\Sso\Model;

use Pg\Security\Api\Provider\FrontendFeaturesInterface;

class FrontendFeaturesProvider implements FrontendFeaturesInterface
{
    /** @var Config */
    private $config;

    /**
     * FrontendFeaturesProvider constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function isNativePasswordUsed()
    {
        return !$this->config->isSsoFrontendEnabled() || $this->config->keepFrontendSamlNativeLogin();
    }
}
