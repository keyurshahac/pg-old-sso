<?php

namespace Pg\Sso\Model\Customer\Indexer;

use Magento\Eav\Model\Config;
use Pg\Sso\Model\Config\Source\Attribute;
use Psr\Log\LoggerInterface;

/**
 * Extending default attribute provider to kick off magento bug use of uninstalled custom attribute in first reindex
 * @see https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/realtime_cardwall?ticket=5045
 *
 * Class AttributeProvider
 * @package Pg\Sso\Model\Customer\Indexer
 */
class AttributeProvider extends \Magento\Customer\Model\Indexer\AttributeProvider
{
    /**
     * Only fields from this list can remove - don't touching magento fields
     */
    const OWN_FIELDS = [Attribute::SSO_ID];

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * AttributeProvider constructor.
     * @param Config $eavConfig
     * @param LoggerInterface $logger
     */
    public function __construct(Config $eavConfig, LoggerInterface $logger)
    {
        $this->logger = $logger;

        parent::__construct($eavConfig);
    }

    /**
     * {@inheritdoc}
     */
    public function addDynamicData(array $data)
    {
        $data = parent::addDynamicData($data);

        $this->filterOutNotExistingFields($data['fields'], $this->existingAttributeNames());

        return $data;
    }

    /**
     * Gets names from collection of readed (existing in DB) attributes
     *
     * @return string[]
     */
    protected function existingAttributeNames()
    {
        $names = [];
        foreach ($this->attributes as $attribute) {
            $names[] = $attribute->getName();
        }

        return $names;
    }

    /**
     * @param array $fields
     * @param array $additionalFields
     */
    protected function filterOutNotExistingFields(array &$fields, array $additionalFields)
    {
        foreach ($fields as $key => $field) {
            if (!in_array($key, $additionalFields) && in_array($key,self::OWN_FIELDS)) {
                unset($fields[$key]);
                $this->logger->warning('Filtered out not existing attribute from list', ['attribute_name' => $key]);
            }
        }
    }
}