<?php

namespace Pg\Sso\Model\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Event\ManagerInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\State\UserLockedException;
use Pg\Sso\Exception\CustomerLogin;
use Pg\Customer\Exception\NoAccessToSAPException;
use \Pg\Customer\Api\SapManagementInterface;
use Magento\Framework\Phrase;

class Authenticator
{
    /** @var AuthenticationInterface */
    private $auth;

    /** @var ManagerInterface */
    private $eventDispatcher;

    /** @var Session */
    private $session;

    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /** @var SapManagementInterface */
    private $sapManagement;

    /**
     * @param AuthenticationInterface $auth
     * @param ManagerInterface $eventDispatcher
     * @param Session $session
     * @param CustomerRepositoryInterface $customerRepository
     * @param SapManagementInterface $sapManagement
     */
    public function __construct(
        AuthenticationInterface $auth,
        ManagerInterface $eventDispatcher,
        Session $session,
        CustomerRepositoryInterface $customerRepository,
        SapManagementInterface $sapManagement
    ) {
        $this->auth = $auth;
        $this->eventDispatcher = $eventDispatcher;
        $this->session = $session;
        $this->customerRepository = $customerRepository;
        $this->sapManagement = $sapManagement;
    }

    /**
     * @param Customer $customer
     *
     * @return Customer
     *
     * @throws CustomerLogin
     * @throws UserLockedException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws NoAccessToSAPException
     */
    public function authenticate(Customer $customer = null)
    {
        if (!$customer->getId()) {
            throw new CustomerLogin(__('Customer did not exist'));
        }

        if ($this->auth->isLocked($customer->getId())) {
            throw new UserLockedException(__('The account is locked.'));
        }
        /** #6807 */
        $sapCustomers = $this->sapManagement->getSapIdsByCustomerId($customer->getId());
        if(empty($sapCustomers)) {
            throw new NoAccessToSAPException(
                new Phrase('This account does not have access to any SAP Customer.')
            );
        }

        $this->eventDispatcher->dispatch('customer_data_object_login', ['customer' => $customer]);
        $customerData = $this->customerRepository->getById($customer->getId());
        $this->session->setCustomerDataAsLoggedIn($customerData);

        return $customer;
    }


    public function logout()
    {
        $this->session->logout();
    }
}