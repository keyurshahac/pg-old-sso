<?php

namespace Pg\Sso\Model\Customer;

use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Pg\Sso\Exception\TooMuchEntities;
use Pg\Sso\Model\Config as SsoConfig;

class CustomerResolver
{
    /** @var CustomerCollectionFactory */
    private $customerCollectionFactory;

    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /** @var StoreManagerInterface */
    private $storeManager;

    /**
     * CustomerResolver constructor.
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param SsoConfig $ssoConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        CustomerCollectionFactory $customerCollectionFactory,
        SsoConfig $ssoConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->ssoConfig = $ssoConfig;
        $this->storeManager = $storeManager;
    }


    /**
     * @param $id
     * @return DataObject
     * @throws LocalizedException
     */
    public function getCustomer($id)
    {
        /* $field expected to be customer column with unique constraint */
        $field = $this->ssoConfig->getSamlAttributeFrontend();


        $collection = $this->customerCollectionFactory->create();
        $collection->addAttributeToSelect($field)->addAttributeToFilter($field, $id);

        $websiteId = $this->getWebsiteId();
        if ($websiteId) {
            $collection
                ->addAttributeToSelect('website_id')
                ->addAttributeToFilter('website_id', $websiteId);
        }

        if ($collection->count() > 1) {
            throw new TooMuchEntities(__('Found more than one customer with given ' . $field));
        } elseif ($collection->count() == 0) {
            throw new NoSuchEntityException(__('No customer found'));
        }

        return $collection->getFirstItem();
    }

    /**
     * @return int|null
     */
    private function getWebsiteId()
    {
        $store = $this->storeManager->getStore();
        if ($store instanceof StoreInterface) {
            return $store->getWebsiteId();
        }

        return null;
    }
}
