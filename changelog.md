# Changelog
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
- All notable changes to this module will be documented in this file.
 

## 2.9.3
- ALL | 2.9 | Merge master to rc ([#8368](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/8368))
- ALL | ELS | Upgrade Magento to 2.2.8 ([#8310](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/8310))
 
## 2.9.2
- 2.9 | ALL | Upgrade magento version to 2.2.8 ([#8330](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/8330))
 
## 2.9.1
- 2.9 | ALL | Upgrade magento version to 2.2.8 ([#8330](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/8330))
 
## 2.9.0
- ALL | 2.9 | [BR] | [DE4180] -  [CR] Add translations for Brazil ([#7007](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/7007))
- ALL | 2.9 | [LA] | [DE4277] - Missing translations for Federation login error ([#7138](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/7138))
- Merge 2.8 -> 2.9 ([#6880](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6880))
- ALL | 2.9 | GLOBAL | [DE4179] - When no SSO ID set up correctly i should get a relevant error message, not a 'session ended' message ([#6997](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6997))
- ALL | 2.9 | [EU/NA] | [DE4082] - When logging in using SSO with no Delivery Points - no error message displayed. ([#6807](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6807))
- ALL | 2.9 | Merge rc to 2.9 ([#6844](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6844))
- ALL 2.9 | [TA2198] Create new language pack ([#6688](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6688))
 
## 2.8.12
- ALL | ELS | Upgrade Magento to 2.2.8 ([#8310](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/8310))
 
## 2.8.10
- ALL | 2.8 | NA | [DE4093] Design gap on registration emails ([#6820](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6820))
 
## 2.8.9
- ALL | 2.8 | [DE4164] [LA] Missing Translation in registration form ([#7018](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/7018))
 
## 2.8.8
- ALL | 2.8 | NA | [DE4093] Design gap on registration emails ([#6820](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6820))
 
## 2.8.7
-  ALL | 2.8 | [DE4008][GLOBAL] | Update Registration Email content in scope of US1415 ([#6395](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6395))
 
## 2.8.6
- ALL | 2.8 | Configure log out endpoints for federation ([#6541](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6541))
 
## 2.8.5
- ALL | 2.8 | Configure log out endpoints for federation ([#6541](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6541))
 
## 2.8.3
- ALL | 2.8 | Merge master to 2.8.x (21.11.2018) ([#6396](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6396))
- ALL | 2.8 | Create forgotten unitTests for module-sso ([#6208](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6208))
- ALL | ELS | Cannot open downloaded invoice if user is an order viewer -- orba task ([#6281](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6281))
 
## 2.8.1
-  ALL | 2.8 | [DE4008][GLOBAL] | Update Registration Email content in scope of US1415 ([#6395](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6395))
 
## 2.8.0
- ALL | 2.8 | [TA1784] Add SSO ID field to the user registration page ([#6067](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6067))
- ALL | 2.8 | [US1415] [Technical] [GDPR Info Sec] Enable PING auth for PG users & external Customer access for Customer Online websites ([#6062](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6062))
- ALL | 2.8 | [TA1788] Do not prompt SSO users for password reset after it expires ([#6073](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6073))
- ALL | 2.8 | [TA1785] Set NULL password and hash for new Federation users ([#6068](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6068))
- ALL | 2.8 | [TA1876] Change columns names and layout ([#6150](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6150))
- ALL | 2.8 | [TA1782] Add new configuration option for SSO ID field ([#6064](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6064))
- ALL | 2.8 | [TA1786] Change invitation e-mail for users registered with SSO ID ([#6069](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6069))
- ALL | 2.8 | Merge 2.7 to 2.8 ([#6111](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6111))
- ALL | 2.8 | [TA1787] Set "Logged in by SSO" flag in user session ([#6072](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6072))
- ALL | 2.8 | SSO change url ([#6127](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6127))
- ALL | 2.8 | [US2117] [Technical] Upgrade PHP to version 7.1 ([#5769](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5769))
 
## 2.7.6
- ALL | ELS | Cannot open downloaded invoice if user is an order viewer -- orba task ([#6281](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/6281))
 
## 2.7.4
- ALL | ELS | Federation login error log generation ([#5592](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5592))
 
## 2.7.3
- ALL | 2.7 | Merge 2.6 and ELS to 2.7 ([#5731](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5731))
- ALL | ELS | Global | Bot accounts created in QA environments ([#5546](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5546))
 
## 2.7.2
- ALL | 2.7 | [EU] Cannot login to backend - authentication error | DE3704 ([#5697](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5697))
 
## 2.7.1
- ALL 2.7 | Merge tasks from 2.6 from 24.08  till 3.09 [batch 3] ([#5497](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5497))
- ALL 2.6 | NA/EU | Merge ELS production deployment - Aug 22, 2018 [US2089] ([#5440](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5440))
- ALL | 2.6 | [DE2928] Upload new missing translations to CA and CAPGP ([#4645](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4645))
- ALL | 2.6 | [GLOBAL] Reindexers doesn't work after command bin/magento index:reindex (+ trash in logs) ([#5404](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5404))
- NA | Production | Create script to change SSO value during deploy ([#5332](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5332))
 
## 2.7.0
- ALL | 2.6 | [GLOBAL] Reindexers doesn't work after command bin/magento index:reindex (+ trash in logs) ([#5404](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5404))
- ALL 2.7 | Merge from 2.6 to 2.7 [batch 1] ([#5317](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5317))
- ALL | 2.7 | [DE3264] Remove backend translations from csv that we send for translations ([#5164](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5164))
- ALL | 2.7 | [TA1598] Add log cleaner method in SSO ([#5158](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5158))
- ALL 2.7 | [DE3260] Check translations en-US module ([#5110](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5110))
 
## 2.6.14
- ALL | ELS | Federation login error log generation ([#5592](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5592))
 
## 2.6.13
- ALL | ELS | Global | Bot accounts created in QA environments ([#5546](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5546))
 
## 2.6.11
- ALL 2.6 | NA/EU | Merge ELS production deployment - Aug 22, 2018 [US2089] ([#5440](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5440))
- NA | Production | Create script to change SSO value during deploy ([#5332](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5332))
 
## 2.6.8
- ALL | 2.6 | [DE2928] Upload new missing translations to CA and CAPGP ([#4645](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4645))
 
## 2.6.7
- ALL | 2.6 | [GLOBAL] Reindexers doesn't work after command bin/magento index:reindex (+ trash in logs) ([#5404](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5404))
 
## 2.6.5
- ALL | 2.6 | [US2077] Merge LA  into 2.6 | CAB | ELS | Production deployment - Aug 14, 2018 ([#5329](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5329))
- LA | ELS | UAT Installation simulation failed ([#5045](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5045))
 
## 2.6.4
- ALL | 2.6 | EU/NA | Merge 2.5 release to 2.6  [US2038] ([#5002](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5002))
 
## 2.6.3
- LA | ELS | UAT Installation simulation failed ([#5045](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5045))
- ALL | ELS | Global | Add SSO ID as optional column in customers grid ([#4728](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4728))
- ALL | ELS | after 2.5 | Odd behavior with federation login redirect ([#4687](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4687))
- ALL | SSO backend success login when user is INACTIVE ([#4344](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4344))
 
## 2.6.1
- ALL | 2.6 | [DE2948] DACH | Upload DACH translations to SIT ([#4671](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4671))
- ALL | 2.6 | [TA1461] Insert French CSVs to the system ([#4608](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4608))
- [TA1104-child] Admin edit page and delete button ([#4572](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4572))
- ALL | 2.6 | [TA1104] Remove Magento password completely from the backend ([#4536](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4536))
- NA/EU | Deploy to UAT  -  June 20, 2018 ([#4636](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4636))
- ALL | 2.6 | [DE2807] Attribute customer_sso_id is invisible on "CUSTOMER INFORMATION" page ([#4345](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4345))
- ALL | 2.6 | [US1840] [Technical GDPR Info Sec] Add logs on Access Changes for B2B Store users ([#3902](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3902))
- ALL | 2.6 | [TA994] Upgrade to Magento 2.2.X (just deploy latest version and make ONE Store usable) - pick the most complex one ([#3901](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3901))
- ALL | 2.6 | [TA997] Upgrade all the Stores to version decided in TA996 ([#3923](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3923))
- ALL | 2.6 | [TA1123] Backend users loggin actions. ([#3905](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3905))
 
## 2.5.5
- NA | Production | Create script to change SSO value during deploy ([#5332](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5332))
 
## 2.5.4
- LA | ELS | UAT Installation simulation failed ([#5045](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5045))
 
## 2.5.3
- LA | ELS | UAT Installation simulation failed ([#5045](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/5045))
 
## 2.5.2
- ALL | ELS | Global | Add SSO ID as optional column in customers grid ([#4728](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4728))
- ALL | ELS | after 2.5 | Odd behavior with federation login redirect ([#4687](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4687))
- ALL | SSO backend success login when user is INACTIVE ([#4344](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/4344))
 
## 2.5.0
- ALL | 2.5 | [GLOBAL] [bug] E-mail with password setup not sent [DE2563] ([#3872](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3872))
- ALL | 2.5 | Add Puerto Rico Spanish language pack [TA1042] ([#3765](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3765))
- ALL | [GRISC 2.2]: User can't logout ([#2108](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/2108))
 
## 2.4.0
- ALL 2.3 [GRISC/IT] Logging in with SSO into imaginary account [DE2332] ([#3480](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3480))
- ALL | [DE2223] [EU\GRISC] SSO doesn't work ([#3365](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3365))
- ALL | [TA845] Update changelog files ([#3229](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3229))
- ALL | [TA846]  Update readme files ([#3230](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3230))
- ALL | [TA874] Italian translation apply based on BOX content. ([#3233](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/3233))
 
## 2.3.0
- PGC | Add reset password link on login screen ([#2201](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/2201))
 
## 2.0.0
- ALL | CI Pg_Customer ([#2057](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/2057))
 
## 1.0.4
- GRISC | My Account page changes ([#2095](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/2095))
 
## 1.0.3
- LA | [GLOBAL] ]Error found in TC362: Elements of page are not translated into Spanish [DE396] ([#522](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/522))
 
## 1.0.2
- PGC | SAML Login issue ([#1287](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/1287))
 
## 1.0.0
- LA | Release 1.0.0 preparation ([#1332](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/1332))
 
## 0.2.5
- Europe | Add the greek translations ([#971](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/971))
 
## 0.2.4
- GRISC | BUG | SSO login issue - Debug ([#990](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/990))
 
## 0.2.3.1
- PGC | Integration with Federation for login to store online ([#767](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/767))
 
## 0.2.3
- Europe/Grisc proper location for plugins in sso module ([#925](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/925))
 
## 0.2.2
- PGC | Integration with Federation for login to store online ([#767](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/767))
 
## 0.2.0
- PGC | Integration with Federation for login to store online ([#767](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/767))
- PGC | Password reset ([#409](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/409))
 
## 0.1.7
- Adding es_MX translations to all modules ([#190](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/190))
 
## 0.1.5
- Translations packages [US321] ([#139](https://app.assembla.com/spaces/orba-pg-m2-internal/tickets/139))
 
