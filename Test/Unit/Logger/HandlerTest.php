<?php
namespace Pg\Sso\Test\Unit\Logger;

use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Logger\Handler;

use Pg\Utils\Symfony\Component\Finder\Finder;
use Pg\Utils\Symfony\Component\Finder\FinderFactory;
use Magento\Framework\Filesystem\Io\File as IoFile;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class HandlerTest
 */
class HandlerTest extends TestCase
{
    /** @var  Handler */
    protected $handler;

    /** @var  FinderFactory|MockObject */
    protected $finderFactoryMock;

    /** @var  DirectoryList|MockObject */
    protected $directoryListMock;

    /** @var  IoFile|MockObject */
    protected $ioFileMock;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $objectManager = new ObjectManager($this);

        $arguments = $objectManager->getConstructArguments(Handler::class);

        $this->finderFactoryMock = $arguments['finderFactory'];
        $this->directoryListMock = $arguments['directoryList'];
        $this->ioFileMock = $arguments['ioFile'];

        $this->handler = $objectManager->getObject(
            Handler::class,
            $arguments
        );
    }

    public function testCleanCallsFileFinderAndIoFileWithCorrectArguments()
    {
        $cleaningIntervalInDays = 2;

        $file1 = 'file1';
        $file2 = 'file2';

        $files = [
            $file1,
            $file2
        ];

        $finderMock = $this->getMockFinder($files);

        $this->ioFileMock
            ->expects($this->once())
            ->method('fileExists')
            ->with('/var/log/sso/', false)
            ->willReturn(true);

        $this->finderFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($finderMock);

        $finderMock
            ->expects($this->once())
            ->method('files')
            ->willReturnSelf();

        $finderMock
            ->expects($this->once())
            ->method('depth')
            ->with(0)
            ->willReturnSelf();

        $finderMock
            ->expects($this->once())
            ->method('in')
            ->with('/var/log/sso/')
            ->willReturnSelf();

        $finderMock
            ->expects($this->once())
            ->method('date')
            ->with('until 2 days ago')
            ->willReturnSelf();

        $this->ioFileMock
            ->expects($this->exactly(count($files)))
            ->method('rm')
            ->withConsecutive(
                [$file1],
                [$file2]
            );

        $result = $this->handler->clean($cleaningIntervalInDays);

        $this->assertTrue($result);
    }

    /**
     * @param array $files
     * @return Finder|MockObject
     */
    private function getMockFinder(array $files)
    {
        $iterator = new \ArrayIterator($files);

        $finderMock = $this
            ->getMockBuilder(Finder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $finderMock
            ->expects($this->any())
            ->method('getIterator')
            ->willReturn($iterator);

        return $finderMock;
    }
}
