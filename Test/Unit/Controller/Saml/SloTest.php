<?php

namespace Pg\Sso\Test\Unit\Controller\Saml;

use Pg\Sso\Controller\Saml\Slo;
use Pg\Sso\Model\Customer\Authenticator;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Auth;

class SloTest extends SamlAbstractTestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $authFactory;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $authenticator;
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $ssoHelperMock;

    /** @var Slo */
    protected $testedController;

    protected function setUp()
    {
        parent::setUp();
        $this->authFactory = $this->getMockBuilder(AuthFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->authenticator = $this->getMockBuilder(Authenticator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->ssoHelperMock = $this->getMockBuilder(\Pg\Sso\Helper\Auth::class)
            ->disableOriginalConstructor()->getMock();

        $this->testedController = new Slo(
            $this->context,
            $this->propertiesProvider,
            $this->logger,
            $this->authFactory,
            $this->authenticator,
            $this->ssoHelperMock
        );
    }

    public function testExecute()
    {
        $this->propertiesProvider->expects($this->once())->method('get')->willReturn(['jakis' => 'array']);
        $auth = $this->getMockBuilder(Auth::class)->disableOriginalConstructor()->getMock();
        $this->authFactory->expects($this->once())->method('create')->willReturn($auth);
        $auth->expects($this->once())->method('getErrors')->willReturn([]);
        $auth->expects($this->once())->method('processSLO')->willReturn([]);

        $this->authenticator->expects($this->once())->method('logout');
        $this->ssoHelperMock->expects($this->once())->method('setSsoFlag')->with(false);
        $this->testedController->execute();
    }
}