<?php

namespace Pg\Sso\Test\Unit\Controller\Saml;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\Event\Manager as EventManager;
use Pg\Sso\Controller\Saml\Acs;
use Pg\Sso\Exception\SamlException;
use Pg\Sso\Helper\Auth as SsoAuthHelper;
use Pg\Sso\Model\Customer\Authenticator;
use Pg\Sso\Model\Customer\CustomerResolver;
use Pg\Sso\Model\Saml\Auth;
use Pg\Sso\Model\Saml\AuthFactory;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

/**
 * Class AcsTest
 * @package   Pg\Sso\Test\Unit\Controller\Saml
 * @copyright Copyright (c) 2018 Orba Sp. z o.o.  (http://orba.co)
 */
class AcsTest extends SamlAbstractTestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $sessionMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $authFactory;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $authenticator;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $customerResolver;

    /** @var EventManager| \PHPUnit_Framework_MockObject_MockObject */
    private $eventManagerMock;

    /** @var SsoAuthHelper | MockObject */
    protected $ssoHelperMock;

    /** @var Acs */
    private $testedController;

    protected function setUp()
    {
        parent::setUp();
        $this->sessionMock = $this->basicMock(Session::class);
        $this->authFactory = $this->basicMock(AuthFactory::class);
        $this->authenticator = $this->basicMock(Authenticator::class);
        $this->customerResolver = $this->basicMock(CustomerResolver::class);
        $this->ssoHelperMock = $this->basicMock(SsoAuthHelper::class, ['setSsoFlag']);
        $this->eventManagerMock = $this->basicMock(EventManager::class);

        $this->testedController = new Acs(
            $this->context,
            $this->propertiesProvider,
            $this->logger,
            $this->sessionMock,
            $this->authFactory,
            $this->authenticator,
            $this->customerResolver,
            $this->eventManagerMock,
            $this->ssoHelperMock
        );
        $this->testedController = $this->objectManager->getObject(Acs::class, [
            'context' => $this->context,
            'provider' => $this->propertiesProvider,
            'logger' => $this->logger,
            'session' => $this->sessionMock,
            'authFactory' => $this->authFactory,
            'authenticator' => $this->authenticator,
            'customerResolver' => $this->customerResolver,
            'eventManager' => $this->eventManagerMock,
            'ssoHelper' => $this->ssoHelperMock
        ]);
    }

    public function testExecute()
    {
        $this->propertiesProvider->expects($this->once())->method('get')->willReturn(['jakis' => 'array']);
        $auth = $this->getMockBuilder(Auth::class)->disableOriginalConstructor()->getMock();
        $this->authFactory->expects($this->once())->method('create')->willReturn($auth);
        $auth->expects($this->once())->method('processResponse');
        $auth->expects($this->once())->method('getErrors')->willReturn([]);
        $auth->expects($this->once())->method('getNameId')->willReturn('someUID');
        $auth->expects($this->once())->method('isAuthenticated')->willReturn(true);
        $customer = $this->getMockBuilder(Customer::class)->disableOriginalConstructor()->getMock();
        $this->customerResolver
            ->expects($this->once())
            ->method('getCustomer')
            ->with('someUID')
            ->willReturn($customer);
        $this->authenticator
            ->expects($this->once())
            ->method('authenticate')
            ->with($customer);
        $this->eventManagerMock->expects($this->exactly(1))
            ->method('dispatch')
            ->with(
                'customer_login_success',
                [
                    'login'   => 'someUID',
                    'message' => 'Customer logged in via SSO'
                ]
            );
        $this->testedController->execute();
    }

    public function testExecuteWithSamlException()
    {
        $this->propertiesProvider->expects($this->once())->method('get')->willReturn(['jakis' => 'array']);
        $auth = $this->getMockBuilder(Auth::class)->disableOriginalConstructor()->getMock();
        $this->authFactory->expects($this->once())->method('create')->willReturn($auth);
        $auth->expects($this->once())->method('processResponse');
        $auth->expects($this->once())->method('getErrors')->willReturn(['zle']);
        $auth->expects($this->once())->method('getLastErrorReason')->willReturn('zle_tory');
        $auth->expects($this->exactly(0))->method('getNameId')->willReturn('someUID');
        $auth->expects($this->exactly(0))->method('isAuthenticated')->willReturn(true);
        $customer = $this->getMockBuilder(Customer::class)->disableOriginalConstructor()->getMock();
        $this->customerResolver
            ->expects($this->exactly(0))
            ->method('getCustomer')
            ->with('someUID')
            ->willReturn($customer);
        $this->authenticator
            ->expects($this->exactly(0))
            ->method('authenticate')
            ->with($customer);
        $this->ssoHelperMock->expects($this->exactly(0))
            ->method('setSsoFlag');
        $this->eventManagerMock->expects($this->exactly(1))
            ->method('dispatch')->with(
                'customer_login_failed',
                [
                    'login'     => '',
                    'message'   => "Authentication Error",
                    'exception' => (new SamlException(__('zle')))->setReason('zle_tory')
                ]

            );

        $this->testedController->execute();
    }
}
