<?php

namespace Pg\Sso\Test\Unit\Controller\Saml;

use Magento\Framework\Controller\Result\Raw;
use Pg\Sso\Controller\Saml\Ssologout;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Customer\Model\Session as UserSession;
use Pg\Sso\Model\Saml\Auth;

class SsologoutTest extends SamlAbstractTestCase
{
    /**
     * @var Raw|\PHPUnit_Framework_MockObject_MockObject
     */
    private $rawResultMock;

    /** @var AuthFactory|\PHPUnit_Framework_MockObject_MockObject */
    private $authFactoryMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var UserSession|\PHPUnit_Framework_MockObject_MockObject
     */
    private $userSessionMock;
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $ssoHelperMock;

    /**
     * @var Ssologout
     */
    private $ssologout;

    protected function setUp()
    {
        parent::setUp();

        $this->rawResultMock = $this->getMockBuilder(Raw::class)->disableOriginalConstructor()->getMock();
        $this->authFactoryMock = $this->getMockBuilder(AuthFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->ssoConfigMock = $this->getMockBuilder(SsoConfig::class)->disableOriginalConstructor()->getMock();
        $this->userSessionMock = $this->getMockBuilder(UserSession::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->ssoHelperMock = $this->getMockBuilder(\Pg\Sso\Helper\Auth::class)
            ->disableOriginalConstructor()->getMock();

        $this->ssologout = new Ssologout(
            $this->context,
            $this->propertiesProvider,
            $this->logger,
            $this->rawResultMock,
            $this->authFactoryMock,
            $this->ssoConfigMock,
            $this->ssoHelperMock
        );
    }

    public function testExecute()
    {
        $this->propertiesProvider->expects($this->once())->method('get')->willReturn(['jakis' => 'array']);
        $this->ssoConfigMock->expects($this->once())->method('isSsoFrontendEnabled')->willReturn(true);

        $auth = $this->getMockBuilder(Auth::class)->disableOriginalConstructor()->getMock();
        $this->authFactoryMock->expects($this->once())->method('create')->willReturn($auth);
        $auth->expects($this->once())->method('getNameId')->willReturn('someUID');
        $auth->expects($this->once())->method('getSessionIndex')->willReturn('index');

        $auth
            ->expects($this->once())
            ->method('logout')
            ->with(null, [], 'someUID', 'index');
        $this->ssoHelperMock->expects($this->once())->method('setSsoFlag')->with(false);
        $this->ssologout->execute();
    }

    public function testWhenSsoDisabled()
    {
        $this->ssoConfigMock->expects($this->once())->method('isSsoFrontendEnabled')->willReturn(false);
        $this->ssologout->execute();
    }
}
