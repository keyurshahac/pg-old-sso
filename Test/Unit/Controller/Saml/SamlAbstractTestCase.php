<?php

namespace Pg\Sso\Test\Unit\Controller\Saml;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\TestFramework\Unit\BaseTestCase;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Saml\Properties\Provider;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\RedirectInterface;

abstract class SamlAbstractTestCase extends BaseTestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $logger;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $propertiesProvider;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $context;

    protected $redirect;
    protected $response;
    protected $messageManager;

    protected function setUp()
    {
        parent::setUp();

        $this->logger = $this->getMockBuilder(Logger::class)->disableOriginalConstructor()->getMock();
        $this->propertiesProvider = $this->getMockBuilder(Provider::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder(Context::class)->disableOriginalConstructor()->getMock();


        $this->redirect = $this->getMockBuilder(RedirectInterface::class)->getMock();
        $this->context->method('getRedirect')->willReturn($this->redirect);

        $this->response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $this->context->method('getResponse')->willReturn($this->response);

        $this->messageManager = $this->getMockBuilder(ManagerInterface::class)->getMock();
        $this->context->method('getMessageManager')->willReturn($this->messageManager);
    }
}