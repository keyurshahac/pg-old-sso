<?php

namespace Pg\Sso\Test\Unit\Controller\Saml;

use Magento\Customer\Model\Session as UserSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Pg\Sso\Controller\Saml\Ssologin;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Model\Saml\Auth;
use Pg\Sso\Model\Saml\AuthFactory;
use PHPUnit_Framework_MockObject_MockObject as Mock;
use Pg\Sso\Model\Saml\Properties\Provider as PropertiesProvider;

class SsologinTest extends \PHPUnit\Framework\TestCase
{
    /** @var SsoConfig|Mock */
    private $ssoConfigMock;

    /** @var UserSession|Mock */
    private $userSessionMock;

    /** @var AuthFactory|Mock */
    private $authFactoryMock;

    /** @var Raw|Mock */
    private $rawResultMock;

    /** @var StoreManagerInterface|Mock */
    private $storeManagerMock;

    /** @var PropertiesProvider|Mock */
    private $propertiesProviderMock;

    /** @var StoreInterface|Mock */
    private $storeMock;

    /** @var Auth|Mock */
    private $authMock;

    /** @var Ssologin */
    private $ssologin;

    protected function setUp()
    {
        parent::setUp();

        $om = new ObjectManager($this);
        $this->prepareMocks();

        $this->ssologin = $om->getObject(Ssologin::class, [
            'ssoConfig' => $this->ssoConfigMock,
            'userSession' => $this->userSessionMock,
            'authFactory' => $this->authFactoryMock,
            'rawResult' => $this->rawResultMock,
            'propertiesProvider' => $this->propertiesProviderMock,
            'storeManager' => $this->storeManagerMock,
        ]);
    }

    public function testExecute()
    {
        $baseUrl = 'some.base.url';
        $this->userSessionMock->expects($this->once())
            ->method('isLoggedIn')
            ->willReturn(false);
        $this->ssoConfigMock->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->willReturn(true);
        $this->propertiesProviderMock->expects($this->once())
            ->method('get')
            ->willReturn(['jakis' => 'array']);
        $this->authFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->authMock);
        $this->authMock->expects($this->once())
            ->method('login');
        $this->storeManagerMock
            ->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);
        $this->storeMock
            ->expects($this->once())
            ->method('getBaseUrl')
            ->willReturn($baseUrl);

        $result = $this->ssologin->execute();

        $this->assertSame($this->rawResultMock, $result);
    }

    private function prepareMocks()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->userSessionMock = $this->getMockBuilder(UserSession::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->authFactoryMock = $this->getMockBuilder(AuthFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->rawResultMock = $this->getMockBuilder(Raw::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->authMock = $this
            ->getMockBuilder(Auth::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->propertiesProviderMock = $this
            ->getMockBuilder(PropertiesProvider::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $this->storeManagerMock = $this
            ->getMockBuilder(StoreManagerInterface::class)
            ->setMethods(['getStore'])
            ->getMockForAbstractClass();
        $this->storeMock = $this
            ->getMockBuilder(StoreInterface::class)
            ->setMethods(['getBaseUrl'])
            ->getMockForAbstractClass();
    }
}
