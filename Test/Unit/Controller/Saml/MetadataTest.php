<?php

namespace Pg\Sso\Test\Unit\Controller\Saml;

use Pg\Sso\Model\Saml\SettingsFactory;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Framework\Controller\Result\Raw;
use Pg\Sso\Controller\Saml\Metadata;
use Pg\Sso\Model\Saml\Settings;

class MetadataTest extends SamlAbstractTestCase
{
    /**
     * @var Raw|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $rawResultMock;

    /**
     * @var SettingsFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $settingsFactoryMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $ssoConfigMock;

    /**
     * @var Metadata
     */
    protected $metadata;

    protected function setUp()
    {
        parent::setUp();

        $this->rawResultMock = $this->getMockBuilder(Raw::class)->disableOriginalConstructor()->getMock();
        $this->ssoConfigMock = $this->getMockBuilder(SsoConfig::class)->disableOriginalConstructor()->getMock();
        $this->settingsFactoryMock = $this->getMockBuilder(SettingsFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->metadata = new Metadata(
            $this->context,
            $this->propertiesProvider,
            $this->logger,
            $this->rawResultMock,
            $this->settingsFactoryMock,
            $this->ssoConfigMock
        );
    }


    /**
     * @throws \Exception
     * @throws \OneLogin_Saml2_Error
     */
    public function testExecute()
    {
        $this->ssoConfigMock->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->willReturn(true);

        $this->propertiesProvider->expects($this->once())->method('get')->willReturn(['jakis' => 'array']);
        $settings = $this->getMockBuilder(Settings::class)->disableOriginalConstructor()->getMock();
        $settings->expects($this->once())->method('getSPMetadata')->willReturn('some Content');
        $this->settingsFactoryMock->expects($this->once())->method('create')->willReturn($settings);

        $this->rawResultMock->expects($this->once())->method('setHeader')->with('content-type', 'application/xml');
        $this->rawResultMock->expects($this->once())->method('setContents')->with('some Content');

        $this->metadata->execute();
    }

    /**
     * @throws \Exception
     * @throws \OneLogin_Saml2_Error
     */
    public function testWhenDisabled()
    {
        $this->ssoConfigMock->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->willReturn(false);
        $this->rawResultMock->expects($this->once())->method('setHttpResponseCode')->with(404);
        $this->metadata->execute();
    }
}
