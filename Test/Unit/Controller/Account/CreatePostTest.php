<?php

namespace Pg\Sso\Test\Unit\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\Store\Model\StoreManagerInterface;
use Pg\Sso\Controller\Account\CreatePost;
use Magento\Framework\TestFramework\Unit\BaseTestCase;
use \PHPUnit_Framework_MockObject_MockObject as MockObject;

/**
 * Class CreatePostTest
 * @package   Pg\Sso\Test\Unit\Controller\Account
 * @copyright Copyright (c) 2018 Orba Sp. z o.o.  (http://orba.co)
 */
class CreatePostTest extends BaseTestCase
{
    const TEST_WEBSITE_ID = 4;

    /** @var CreatePost */
    protected $controller;

    /** @var Context | MockObject */
    protected $contextMock;

    /** @var RequestInterface | MockObject */
    protected $requestMock;

    /** @var StoreManagerInterface | MockObject */
    protected $storeManager;

    /* @var StoreInterface | MockObject */
    protected $storeMock;

    /* @var WebsiteInterface | MockObject */
    protected $websiteMock;

    /** @var SsoConfig | MockObject */
    protected $ssoConfigMock;

    public function setUp()
    {
        $this->contextMock = $this->basicMock(Context::class);

        $this->requestMock = $this->getMockForAbstractClass(RequestInterface::class);
        $this->contextMock->expects($this->once())->method('getRequest')
            ->willReturn($this->requestMock);
        $this->storeMock = $this->getMockForAbstractClass(StoreInterface::class);
        $this->storeManager = $this->getMockForAbstractClass(StoreManagerInterface::class);
        $this->ssoConfigMock = $this->basicMock(SsoConfig::class, ['getDomainSso']);

        $objectManager = new ObjectManager($this);
        $this->controller = $objectManager->getObject(
            CreatePost::class,
            [
                'context' => $this->contextMock,
                'storeManager' => $this->storeManager,
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );

        parent::setUp();
    }

    public function testGetCustomerSsoIdConfigDoNotShowFieldEmptyEmail()
    {
        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->willReturn('email')
            ->willReturn('');

        /** invoke \Pg\Sso\Controller\Account\CreatePost::prepareJsonResponse */
        $actualResult = $this->invokeMethod(
            $this->controller,
            'getCustomerSsoIdConfig'
        );
        $expectedResult = ['showField' => false];
        $this->assertEquals($expectedResult, $actualResult);
    }

    /**
     * @dataProvider getDomainSsoDataProvider
     *
     * @param $ssoDomain
     */
    public function testGetCustomerSsoIdConfigDoNotShowFieldDomainSSONotConfigured($ssoDomain)
    {
        $this->websiteMock = $this->getMockForAbstractClass(WebsiteInterface::class);
        $this->websiteMock->expects($this->once())->method('getId')
            ->willReturn(self::TEST_WEBSITE_ID);

        $this->storeManager->expects($this->any())->method('getWebsite')
            ->willReturn($this->websiteMock);

        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->willReturn('email')
            ->willReturn('example@domain.com');

        $this->ssoConfigMock->expects($this->once())
            ->method('getDomainSso')
            ->willReturn($ssoDomain);

        /** @see \Pg\Sso\Controller\Account\CreatePost::getCustomerSsoIdConfig */
        $actualResult = $this->invokeMethod(
            $this->controller,
            'getCustomerSsoIdConfig'
        );
        $expectedResult = ['showField' => false];
        $this->assertEquals($expectedResult, $actualResult);
    }

    public function getDomainSsoDataProvider()
    {
        return [
            'emptyDomainCase' => ['domain' => ''],
            'nullDomainCase'  => ['domain' => null]
        ];
    }

    /**
     * Invoke any method of an object.
     *
     * @param $object
     * @param $methodName
     * @param array $parameters
     *
     * @return mixed
     * @throws \ReflectionException
     */
    protected function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $parameters);
    }
}
