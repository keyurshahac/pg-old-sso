<?php

namespace Pg\Sso\Test\Unit\Controller\Adminhtml\Saml;

use Magento\Framework\Controller\Result\Redirect;
use Magento\User\Model\User;
use Pg\Sso\Controller\Adminhtml\Saml\Slo;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Raw;
use Pg\Sso\Exception;
use Pg\Sso\Model\Admin\Authenticator;
use Pg\Sso\Model\Admin\AuthenticatorFactory;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Auth;
use Magento\Backend\Model\Auth as AdminAuth;
use Magento\Framework\Message\ManagerInterface;
use Pg\Sso\Logger\Logger;
use Magento\Backend\Model\Auth\Session;

class SloTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Provider
     */
    protected $propertiesProvider;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var Auth
     */
    protected $auth;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var Redirect
     */
    protected $redirect;

    /**
     * @var AdminAuth
     */
    public $adminAuth;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AuthenticatorFactory
     */
    protected $backendAuthenticatorFactory;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Slo
     */
    protected $controller;

    /**
     * @var Authenticator
     */
    protected $authenticator;

    /**
     *
     */
    protected function setUp()
    {
        $this->context = $this->getMockBuilder(Context::class)->
        disableOriginalConstructor()->
        getMock();

        $this->propertiesProvider = $this->getMockBuilder(Provider::class)->
        setMethods(['get'])->
        disableOriginalConstructor()->
        getMock();

        $this->rawResult = $this->getMockBuilder(Raw::class)->
        disableOriginalConstructor()->
        setMethods(['setHttpResponseCode', 'setHeader', 'setContents'])->
        getMock();

        $this->auth = $this->getMockBuilder(Auth::class)->
        disableOriginalConstructor()->
        setMethods(['processSLO', 'getErrors'])->
        getMock();

        $this->authFactory = $this->getMockBuilder(AuthFactory::class)->
        disableOriginalConstructor()->
        setMethods(['create'])->
        getMock();

        $this->redirect = $this->getMockBuilder(Redirect::class)->
        disableOriginalConstructor()->
        setMethods(['setPath'])->
        getMock();

        $this->messageManager = $this->getMockBuilder(ManagerInterface::class)->
        disableOriginalConstructor()->
        setMethods(['addErrorMessage'])->
        getMockForAbstractClass();

        $this->logger = $this->getMockBuilder(Logger::class)->
        disableOriginalConstructor()->
        setMethods(['addError'])->
        getMock();

        $this->session = $this->getMockBuilder(Session::class)->
        disableOriginalConstructor()->
        setMethods(['getLogoutRequestID', 'getUser'])->
        getMock();


        $this->backendAuthenticatorFactory = $this->getMockBuilder('Pg\Sso\Model\Admin\AuthenticatorFactory')->
        disableOriginalConstructor()->
        setMethods(['create'])->
        getMock();

        $this->adminAuth = $this->getMockBuilder(AdminAuth::class)->
        disableOriginalConstructor()->
        setMethods(['getAuthStorage'])->
        getMock();

        $this->user = $this->getMockBuilder(User::class)->
        disableOriginalConstructor()->
        getMock();

        $this->authenticator = $this->getMockBuilder(Authenticator::class)->
        disableOriginalConstructor()->
        setMethods(['logout'])->
        getMock();

        $this->context->method('getRedirect')->willReturn($this->redirect);
        $this->context->method('getAuth')->willReturn($this->adminAuth);
        $this->context->method('getMessageManager')->willReturn($this->messageManager);

        $this->controller = $this->getMockBuilder(Slo::class)->
        setConstructorArgs([
            $this->context,
            $this->propertiesProvider,
            $this->logger,
            $this->authFactory,
            $this->redirect,
            $this->backendAuthenticatorFactory
        ])->
        setMethods([
            "validateAuth",
            "redirectSuccess",
            "redirectError",
            "logout"
        ])->
        getMock();

        parent::setUp();
    }

    /**
     *
     */
    protected function configureBasics()
    {
        $someConfig = ['some' => 'data'];
        $authN = "zaaqwssx";

        $this->adminAuth->
        method('getAuthStorage')->
        willReturn($this->session);

        $this->propertiesProvider->
        expects($this->once())->
        method("get")->
        will($this->returnValue($someConfig));

        $this->authFactory->
        expects($this->once())->
        method("create")->
        with($someConfig)->
        will($this->returnValue($this->auth));

        $this->backendAuthenticatorFactory->
        method('create')->
        will($this->returnValue($this->authenticator));

        $this->session->
        expects($this->once())->
        method("getLogoutRequestID")->
        will($this->returnValue($authN));


        $this->auth->
        expects($this->once())->
        method("processSLO")->
        with(false, $authN);
    }

    /**
     *
     */
    protected function configureForError()
    {
        $this->logger->
        expects($this->once())->
        method("addError");

        $this->messageManager->
        expects($this->once())->
        method("addErrorMessage");

        $this->controller->
        expects($this->once())->
        method("redirectError")->
        with($this->redirect)->
        will($this->returnValue($this->redirect));
    }

    /**
     * Test if authObject is valid
     */
    public function testExecuteAuthException()
    {
        $this->configureBasics();
        $this->configureForError();

        $ex = new Exception(__("Some exception"));

        $this->controller->
        expects($this->once())->
        method("validateAuth")->
        with($this->auth)->
        will($this->throwException($ex));

        $this->controller->execute();
    }


    /**
     * Auth exception
     */
    public function testExecuteAuthenticatorException()
    {
        $this->configureBasics();
        $this->configureForError();

        $ex = new Exception(__("Some exception"));

        $this->session->
        expects($this->once())->
        method('getUser')->
        will($this->returnValue($this->user));

        $this->controller->
        expects($this->once())->
        method('logout')->
        with($this->session, $this->user)->
        will($this->throwException($ex));

        $this->controller->execute();
    }

    /**
     * Success test
     */
    public function testExecute()
    {

        $this->configureBasics();

        $this->session->
        expects($this->once())->
        method('getUser')->
        will($this->returnValue($this->user));

        $this->controller->
        expects($this->once())->
        method('logout')->
        with($this->session, $this->user);

        $this->controller->
        expects($this->once())->
        method('redirectSuccess')->
        with($this->redirect)->
        will($this->returnValue($this->redirect));

        $this->assertEquals($this->redirect, $this->controller->execute());
    }
}
