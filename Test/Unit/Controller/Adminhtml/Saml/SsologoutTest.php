<?php

namespace Pg\Sso\Test\Unit\Controller\Adminhtml\Saml;

use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Controller\Adminhtml\Saml\Ssologout;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Raw;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Auth;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Backend\Model\Auth as AdminAuth;
use Magento\Framework\Message\ManagerInterface;
use Pg\Sso\Logger\Logger;
use Magento\Backend\Model\Auth\Session;

class SsologoutTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Raw|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $rawResult;

    /**
     * @var Provider|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $propertiesProvider;

    /**
     * @var AuthFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $authFactory;

    /**
     * @var Auth|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $auth;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $ssoConfigMock;

    /**
     * @var Redirect|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $redirect;

    /**
     * @var AdminAuth|\PHPUnit_Framework_MockObject_MockObject
     */
    public $adminAuth;

    /**
     * @var ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $messageManager;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $logger;

    /**
     * @var Session|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $session;

    /**
     * @var Ssologout|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $controller;

    /**
     *
     */
    protected function setUp()
    {
        $this->context = $this->getMockBuilder(Context::class)->
        disableOriginalConstructor()->
        getMock();

        $this->propertiesProvider = $this->getMockBuilder(Provider::class)->
        setMethods(['get'])->
        disableOriginalConstructor()->
        getMock();

        $this->rawResult = $this->getMockBuilder(Raw::class)->
        disableOriginalConstructor()->
        setMethods(['setHttpResponseCode', 'setHeader', 'setContents'])->
        getMock();

        $this->auth = $this->getMockBuilder(Auth::class)->
        disableOriginalConstructor()->
        setMethods(['logout'])->
        getMock();

        $this->authFactory = $this->getMockBuilder(AuthFactory::class)->
        disableOriginalConstructor()->
        setMethods(['create'])->
        getMock();

        $this->ssoConfigMock = $this->getMockBuilder(SsoConfig::class)->
        disableOriginalConstructor()->
        getMock();

        $this->redirect = $this->getMockBuilder(Redirect::class)->
        disableOriginalConstructor()->
        setMethods(['setPath', 'setUrl'])->
        getMock();

        $this->adminAuth = $this->getMockBuilder(AdminAuth::class)->
        disableOriginalConstructor()->
        setMethods(['getAuthStorage'])->
        getMock();

        $this->messageManager = $this->getMockBuilder(ManagerInterface::class)->
        disableOriginalConstructor()->
        setMethods(['addErrorMessage'])->
        getMockForAbstractClass();

        $this->logger = $this->getMockBuilder(Logger::class)->
        disableOriginalConstructor()->
        setMethods(['addError'])->
        getMock();

        $this->session = $this->getMockBuilder(Session::class)->
        disableOriginalConstructor()->
        setMethods(['getSamlNameId', 'getSamlSessionIndex'])->
        getMock();

        $this->context->method('getRedirect')->willReturn($this->redirect);
        $this->context->method('getAuth')->willReturn($this->adminAuth);
        $this->context->method('getMessageManager')->willReturn($this->messageManager);

        $this->controller = $this->getMockBuilder(Ssologout::class)->
        setConstructorArgs([
            $this->context,
            $this->propertiesProvider,
            $this->logger,
            $this->rawResult,
            $this->authFactory,
            $this->ssoConfigMock,
            $this->redirect
        ])->
        setMethods([
            "getStartupPageUrl",
            "canProcess"
        ])->
        getMock();

        parent::setUp();
    }

    /**
     * Test if SAML is enabled
     */
    public function testExecuteEnabled()
    {
        $nameId = 123;
        $sessionIndex = 456789;
        $sampleData = array("data" => "someData");

        $this->controller->
        expects($this->once())->
        method("canProcess")->
        willReturn(true);

        $this->adminAuth->
        expects($this->once())->
        method('getAuthStorage')->
        will($this->returnValue($this->session));

        $this->propertiesProvider->
        expects($this->once())->
        method('get')->
        will($this->returnValue($sampleData));

        $this->authFactory->
        expects($this->once())->
        method('create')->
        with($sampleData)->
        will($this->returnValue($this->auth));

        $this->session->
        expects($this->once())->
        method('getSamlNameId')->
        will($this->returnValue($nameId));

        $this->session->
        expects($this->once())->
        method('getSamlSessionIndex')->
        will($this->returnValue($sessionIndex));

        $this->redirect->
        expects($this->once())->
        method('setPath')->
        with("*")->
        will($this->returnSelf());

        $this->auth->
        expects($this->once())->
        method('logout')->
        with(null, [], $nameId, $sessionIndex);

        $this->assertEquals($this->redirect, $this->controller->execute());
    }


    /**
     * Test if SAML is disbaled
     */
    public function testExecuteNotProcess()
    {

        $this->controller->
        expects($this->once())->
        method("canProcess")->
        willReturn(false);

        $this->redirect->
        expects($this->once())->
        method('setPath')->
        with('admin/index/index')->
        will($this->returnSelf());

        $this->assertEquals($this->redirect, $this->controller->execute());
    }


    public function testExecuteException()
    {
        $controller = $this->controller;

        $this->controller->
        expects($this->once())->
        method("canProcess")->
        willReturn(true);

        $this->propertiesProvider->
        expects($this->once())->
        method('get')->
        will($this->returnValue([]));

        $exMsg = "Some exception to be logged";
        $ex = new \Exception($exMsg);

        $this->authFactory->
        expects($this->once())->
        method('create')->
        will($this->throwException($ex));

        $this->messageManager->
        expects($this->once())->
        method('addErrorMessage')->
        with($this->stringContains("Some SSO error occurred."));

        $this->logger->
        expects($this->once())->
        method('addError')->
        with($exMsg);


        $this->redirect->
        expects($this->once())->
        method('setPath')->
        with("*")->
        will($this->returnSelf());

        $this->assertEquals($this->redirect, $controller->execute());
    }
}
