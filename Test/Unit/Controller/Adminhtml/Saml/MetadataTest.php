<?php

namespace Pg\Sso\Test\Unit\Controller\Adminhtml\Saml;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Controller\Adminhtml\Saml\Metadata;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Raw as RawResult;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Model\Saml\SettingsFactory;
use Pg\Sso\Model\Saml\Settings;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Config as SsoConfig;

class MetadataTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var RawResult|\PHPUnit_Framework_MockObject_MockObject
     */
    private $rawResultMock;

    /**
     * @var Provider|\PHPUnit_Framework_MockObject_MockObject
     */
    private $providerMock;

    /**
     * @var SettingsFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $settingsFactoryMock;

    /**
     * @var Settings|\PHPUnit_Framework_MockObject_MockObject
     */
    private $settingsMock;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var Metadata
     */
    private $metadata;
    
    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->metadata = $om->getObject(
            Metadata::class,
            [
                'context' => $this->contextMock,
                'provider' => $this->providerMock,
                'logger' => $this->loggerMock,
                'rawResult' => $this->rawResultMock,
                'settingsFactory' => $this->settingsFactoryMock,
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );

        parent::setUp();
    }

    /**
     * @throws \Exception
     * @throws \OneLogin_Saml2_Error
     */
    public function testExecuteWithEnabledConfigWillReturnExpectedResponse()
    {
        $sampleXml = "<xml/>";
        $sampleData = array("data" => "someData");

        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(true));

        $this->providerMock
            ->expects($this->once())
            ->method('get')
            ->will($this->returnValue($sampleData));

        $this->settingsFactoryMock
            ->expects($this->once())
            ->method('create')
            ->will($this->returnValue($this->settingsMock));

        $this->rawResultMock
            ->expects($this->once())
            ->method('setHeader')
            ->with('content-type', 'application/xml');

        $this->settingsMock
            ->expects($this->once())
            ->method('getSPMetadata')
            ->will($this->returnValue($sampleXml));

        $this->rawResultMock
            ->expects($this->once())
            ->method('setContents')
            ->with($sampleXml)
            ->will($this->returnSelf());

        $result = $this->metadata->execute();
        $this->assertSame($this->rawResultMock, $result);
    }

    /**
     * @throws \Exception
     * @throws \OneLogin_Saml2_Error
     */
    public function testExecuteWithDisabledConfigWillReturnExpectedResponse()
    {
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(false));

        $this->rawResultMock
            ->expects($this->once())
            ->method('setHttpResponseCode')
            ->with(404)
            ->will($this->returnSelf());

        $result = $this->metadata->execute();
        $this->assertSame($this->rawResultMock, $result);
    }

    private function prepareMocks()
    {
        $this->contextMock = $this
            ->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->providerMock = $this
            ->getMockBuilder(Provider::class)
            ->setMethods(['get'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->rawResultMock = $this
            ->getMockBuilder(RawResult::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'setHttpResponseCode',
                'setHeader',
                'setContents',
            ])
            ->getMock();
        $this->settingsMock = $this
            ->getMockBuilder(Settings::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSPMetadata'])
            ->getMock();
        $this->settingsFactoryMock = $this
            ->getMockBuilder(SettingsFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->ssoConfigMock = $this
            ->getMockBuilder(ssoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSsoAdminSamlEnabled'])
            ->getMock();
        $this->loggerMock = $this
            ->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->setMethods(['addError'])
            ->getMock();
    }
}
