<?php

namespace Pg\Sso\Test\Unit\Controller\Adminhtml\Saml;

use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Event\Manager;
use Magento\User\Model\User;
use Pg\Sso\Controller\Adminhtml\Saml\Acs;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Raw;
use Pg\Sso\Exception;
use Pg\Sso\Model\Admin\Authenticator;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Auth;
use Magento\Backend\Model\Auth as AdminAuth;
use Magento\Framework\Message\ManagerInterface;
use Pg\Sso\Logger\Logger;
use Magento\Backend\Model\Auth\Session;
use Pg\Sso\Model\Admin\UserResolver;

class AscTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Provider
     */
    protected $propertiesProvider;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var Auth
     */
    protected $auth;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var Redirect
     */
    protected $redirect;

    /**
     * @var AdminAuth
     */
    public $adminAuth;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var \Pg\Sso\Model\Admin\AuthenticatorFactory
     */
    protected $backendAuthenticatorFactory;

    /**
     * @var UserResolver
     */
    protected $backendUserResolver;


    /**
     * @var User
     */
    protected $user;


    /**
     * @var Acs
     */
    protected $controller;


    /**
     * @var Authenticator
     */
    protected $authenticator;

    /**
     *
     */
    protected function setUp()
    {
        $this->context = $this->getMockBuilder(Context::class)->
        disableOriginalConstructor()->
        getMock();

        $this->propertiesProvider = $this->getMockBuilder(Provider::class)->
        setMethods(['get'])->
        disableOriginalConstructor()->
        getMock();

        $this->rawResult = $this->getMockBuilder(Raw::class)->
        disableOriginalConstructor()->
        setMethods(['setHttpResponseCode', 'setHeader', 'setContents'])->
        getMock();

        $this->auth = $this->getMockBuilder(Auth::class)->
        disableOriginalConstructor()->
        setMethods([
            'isAuthenticated',
            'processResponse',
            'getErrors',
            'getNameId',
            'getAttributes',
            'getSessionIndex',
            'unsAuthNRequestID',
        ])->
        getMock();

        $this->authFactory = $this->getMockBuilder(AuthFactory::class)->
        disableOriginalConstructor()->
        setMethods(['create'])->
        getMock();

        $this->redirect = $this->getMockBuilder(Redirect::class)->
        disableOriginalConstructor()->
        setMethods(['setPath'])->
        getMock();

        $this->adminAuth = $this->getMockBuilder(AdminAuth::class)->
        disableOriginalConstructor()->
        setMethods(['isLoggedIn'])->
        getMock();

        $this->messageManager = $this->getMockBuilder(ManagerInterface::class)->
        disableOriginalConstructor()->
        setMethods(['addErrorMessage'])->
        getMockForAbstractClass();

        $this->logger = $this->getMockBuilder(Logger::class)->
        disableOriginalConstructor()->
        setMethods(['addError'])->
        getMock();

        $this->session = $this->getMockBuilder(Session::class)->
        disableOriginalConstructor()->
        setMethods(['getAuthNRequestID'])->
        getMock();


        $this->backendAuthenticatorFactory = $this->getMockBuilder('Pg\Sso\Model\Admin\AuthenticatorFactory')->
        disableOriginalConstructor()->
        setMethods(['create'])->
        getMock();

        $this->backendUserResolver = $this->getMockBuilder(UserResolver::class)->
        disableOriginalConstructor()->
        setMethods(['getUser'])->
        getMock();

        $this->adminAuth = $this->getMockBuilder(AdminAuth::class)->
        disableOriginalConstructor()->
        setMethods(['getAuthStorage'])->
        getMock();

        $this->user = $this->getMockBuilder(User::class)->
        disableOriginalConstructor()->
        setMethods(['getId'])->
        getMock();

        $this->authenticator = $this->getMockBuilder(Authenticator::class)->
        disableOriginalConstructor()->
        setMethods(['authenticate'])->
        getMock();

        $this->context->method('getRedirect')->willReturn($this->redirect);
        $this->context->method('getAuth')->willReturn($this->adminAuth);
        $this->context->method('getMessageManager')->willReturn($this->messageManager);

        $eventManagerMock = $this->getMockBuilder(Manager::class)->disableOriginalConstructor()
            ->setMethods(['dispatch'])->getMock();
        $eventManagerMock->expects($this->any())->method('dispatch')->willReturn(null);

        $this->controller = $this->getMockBuilder(Acs::class)->
        setConstructorArgs([
            $this->context,
            $this->propertiesProvider,
            $this->logger,
            $this->authFactory,
            $this->redirect,
            $this->backendAuthenticatorFactory,
            $this->backendUserResolver,
            $eventManagerMock
            ])->
        setMethods([
            "validateAuth",
            "redirectSuccess",
            "redirectError",
            "setExtraSamlParams"])->
        getMock();

        parent::setUp();
    }

    /**
     *
     */
    protected function configureBasics()
    {
        $someConfig = ['some' => 'data'];
        $authN = "zaaqwssx";

        $this->adminAuth->
        method('getAuthStorage')->
        willReturn($this->session);

        $this->backendUserResolver->
        method("getUser")->
        will($this->returnValue($this->user));

        $this->backendAuthenticatorFactory->
        method('create')->
        will($this->returnValue($this->authenticator));

        $this->propertiesProvider->
        expects($this->once())->
        method("get")->
        will($this->returnValue($someConfig));

        $this->session->
        expects($this->once())->
        method("getAuthNRequestID")->
        will($this->returnValue($authN));

        $this->authFactory->
        expects($this->once())->
        method("create")->
        with($someConfig)->
        will($this->returnValue($this->auth));

        $this->auth->
        expects($this->once())->
        method("processResponse")->
        with($authN);
    }

    /**
     *
     */
    protected function configureForError()
    {
        $this->logger->
        expects($this->once())->
        method("addError");

        $this->messageManager->
        expects($this->once())->
        method("addErrorMessage");

        $this->controller->
        expects($this->once())->
        method("redirectError")->
        with($this->redirect)->
        will($this->returnValue($this->redirect));
    }

    /**
     * Test if authObject is valid
     */
    public function testExecuteAuthException()
    {
        $this->configureBasics();
        $this->configureForError();

        $ex = new Exception(__("Some exception"));

        $this->controller->
        expects($this->once())->
        method("validateAuth")->
        with($this->auth)->
        will($this->throwException($ex));

        $this->controller->execute();
    }


    /**
     * Unexist user
     */
    public function testExecuteUserException()
    {
        $this->configureBasics();
        $this->configureForError();

        $this->backendUserResolver->
        expects($this->once())->
        method("getUser")->
        will($this->returnValue($this->user));

        $this->user->
        expects($this->once())->
        method("getId")->
        will($this->returnValue(null));

        $this->controller->execute();
    }

    /**
     * Auth exception
     */
    public function testExecuteAuthenticatorException()
    {
        $this->configureBasics();
        $this->configureForError();

        $ex = new Exception(__("Some exception"));

        $this->user->
        method("getId")->
        will($this->returnValue(123));

        $this->authenticator->
        expects($this->once())->
        method('authenticate')->
        will($this->throwException($ex));

        $this->controller->execute();
    }

    /**
     * Success test
     */
    public function testExecute()
    {
        $nameId = "soemId";
        $sessionId = "asdasd";
        $userId = 666;

        $this->configureBasics();

        $this->user->
        method('getId')->
        willReturn($userId);

        $this->auth->
        expects($this->once())->
        method('getSessionIndex')->
        will($this->returnValue($sessionId));

        $this->auth->
        expects($this->once())->
        method('getNameId')->
        will($this->returnValue($nameId));

        $this->backendAuthenticatorFactory->
        expects($this->once())->
        method('create')->
        with([
            "user" => $this->user,
            "storage" => $this->session
        ]);

        $this->authenticator->
        expects($this->once())->
        method('authenticate');

        $this->controller->
        expects($this->once())->
        method('setExtraSamlParams')->
        with($this->session, $nameId, $sessionId);

        $this->controller->
        expects($this->once())->
        method('redirectSuccess')->
        with($this->redirect)->
        will($this->returnValue($this->redirect));

        $this->assertEquals($this->redirect, $this->controller->execute());
    }
}
