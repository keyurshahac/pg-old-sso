<?php

namespace Pg\Sso\Test\Unit\Controller\Adminhtml\Saml;

use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Controller\Adminhtml\Saml\Loggedout;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Raw;
use Pg\Sso\Model\Saml\Properties\Provider;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Auth;
use Magento\Backend\Model\Auth as AdminAuth;
use Magento\Framework\Message\ManagerInterface;
use Pg\Sso\Logger\Logger;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Result\Page;

class LoggedoutTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Raw;
     */
    protected $rawResult;

    /**
     * @var Provider
     */
    protected $propertiesProvider;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var Auth
     */
    protected $auth;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var Redirect
     */
    protected $redirect;

    /**
     * @var AdminAuth
     */
    public $adminAuth;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var Page
     */
    protected $page;

    /**
     * @var Loggedout
     */
    protected $controller;

    /**
     *
     */
    protected function setUp()
    {
        $this->context = $this->getMockBuilder(Context::class)->
        disableOriginalConstructor()->
        getMock();

        $this->propertiesProvider = $this->getMockBuilder(Provider::class)->
        setMethods(['get'])->
        disableOriginalConstructor()->
        getMock();

        $this->rawResult = $this->getMockBuilder(Raw::class)->
        disableOriginalConstructor()->
        setMethods(['setHttpResponseCode', 'setHeader', 'setContents'])->
        getMock();

        $this->auth = $this->getMockBuilder(Auth::class)->
        disableOriginalConstructor()->
        setMethods(['login'])->
        getMock();

        $this->authFactory = $this->getMockBuilder(AuthFactory::class)->
        disableOriginalConstructor()->
        setMethods(['create'])->
        getMock();

        $this->redirect = $this->getMockBuilder(Redirect::class)->
        disableOriginalConstructor()->
        setMethods(['setPath', 'setUrl'])->
        getMock();

        $this->adminAuth = $this->getMockBuilder(AdminAuth::class)->
        disableOriginalConstructor()->
        setMethods(['isLoggedIn'])->
        getMock();

        $this->messageManager = $this->getMockBuilder(ManagerInterface::class)->
        disableOriginalConstructor()->
        setMethods(['addErrorMessage'])->
        getMockForAbstractClass();

        $this->logger = $this->getMockBuilder(Logger::class)->
        disableOriginalConstructor()->
        setMethods(['addError'])->
        getMock();

        $this->pageFactory = $this->getMockBuilder(PageFactory::class)->
        disableOriginalConstructor()->
        setMethods(['create'])->
        getMock();

        $this->page = $this->getMockBuilder(Page::class)->
        disableOriginalConstructor()->
        getMock();

        $this->context->method('getRedirect')->willReturn($this->redirect);
        $this->context->method('getAuth')->willReturn($this->adminAuth);
        $this->context->method('getMessageManager')->willReturn($this->messageManager);

        $this->controller = $this->getMockBuilder(Loggedout::class)->
        setConstructorArgs([
            $this->context,
            $this->propertiesProvider,
            $this->logger,
            $this->rawResult,
            $this->authFactory,
            $this->redirect,
            $this->pageFactory
        ])->
        setMethods([
            "getStartupPageUrl"
        ])->
        getMock();

        parent::setUp();
    }

    public function testExecuteLogged()
    {
        $defaultUrl = "http://some.url";

        $this->controller->
        expects($this->once())->
        method('getStartupPageUrl')->
        will($this->returnValue($defaultUrl));

        $this->adminAuth->
        expects($this->once())->
        method("isLoggedIn")->
        will($this->returnValue(true));

        $this->redirect->
        expects($this->once())->
        method("setUrl")->
        with($defaultUrl)->
        will($this->returnSelf());

        $this->assertEquals($this->redirect, $this->controller->execute());
    }

    public function testExecuteNotLogged()
    {
        $this->adminAuth->
        expects($this->once())->
        method("isLoggedIn")->
        will($this->returnValue(false));

        $this->pageFactory->
        expects($this->once())->
        method("create")->
        will($this->returnValue($this->page));

        $this->assertEquals($this->page, $this->controller->execute());
    }
}
