<?php

namespace Pg\Sso\Test\Unit\Controller\Adminhtml\Saml;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Helper\Data as BackendHelper;
use Magento\Backend\Model\Auth as AdminAuth;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Message\ManagerInterface;
use Pg\Sso\Controller\Adminhtml\Saml\Ssologin;
use Pg\Sso\Logger\Logger;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Model\Saml\Auth;
use Pg\Sso\Model\Saml\AuthFactory;
use Pg\Sso\Model\Saml\Properties\Provider;
use PHPUnit_Framework_MockObject_MockObject as Mock;

class SsologinTest extends \PHPUnit\Framework\TestCase
{
    /** @var Raw|Mock */
    private $rawResultMock;

    /** @var Provider|Mock */
    private $providerMock;

    /** @var AuthFactory|Mock */
    private $authFactoryMock;

    /** @var Auth|Mock */
    private $authMock;

    /** @var Context|Mock */
    private $contextMock;

    /** @var SsoConfig|Mock */
    private $ssoConfigMock;

    /** @var Redirect|Mock */
    private $redirectMock;

    /** @var AdminAuth|Mock */
    private $adminAuthMock;

    /** @var ManagerInterface|Mock */
    private $messageManagerMock;

    /** @var Logger|Mock */
    private $loggerMock;

    /** @var BackendHelper|Mock */
    private $backendHelperMock;

    /** @var Ssologin */
    private $ssoLogin;

    protected function setUp()
    {
        parent::setUp();
        $this->prepareMocks();

        $this->ssoLogin = $this
            ->getMockBuilder(Ssologin::class)
            ->setConstructorArgs([
                $this->contextMock,
                $this->providerMock,
                $this->loggerMock,
                $this->rawResultMock,
                $this->authFactoryMock,
                $this->ssoConfigMock,
                $this->redirectMock
            ])
            ->setMethods(['getStartupPageUrl'])
            ->getMock();
    }

    /**
     * Test if SAML is enabled
     */
    public function testExecuteEnabled()
    {
        $sampleData = array("data" => "someData");
        $howePageUrl = 'some.home.page.url';

        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(true));
        $this->adminAuthMock
            ->expects($this->once())
            ->method('isLoggedIn')
            ->will($this->returnValue(false));
        $this->providerMock
            ->expects($this->once())
            ->method('get')
            ->will($this->returnValue($sampleData));
        $this->authFactoryMock
            ->expects($this->once())
            ->method('create')
            ->will($this->returnValue($this->authMock));
        $this->authMock
            ->expects($this->once())
            ->method('login');
        $this->contextMock
            ->expects($this->once())
            ->method('getHelper')
            ->willReturn($this->backendHelperMock);
        $this->backendHelperMock
            ->expects($this->once())
            ->method('getHomePageUrl')
            ->willReturn($howePageUrl);

        $this->assertEquals($this->rawResultMock, $this->ssoLogin->execute());
    }

    /**
     * Test if SAML is disabled
     */
    public function testExecuteDisabled()
    {
        $this->adminAuthMock
            ->expects($this->once())
            ->method('isLoggedIn')
            ->will($this->returnValue(false));
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(false));
        $this->redirectMock
            ->expects($this->once())
            ->method('setPath')
            ->with("*")
            ->will($this->returnSelf());

        $this->assertEquals($this->redirectMock, $this->ssoLogin->execute());
    }

    /**
     * Test if SAML is disbaled
     */
    public function testExecuteLoggedIn()
    {
        $someUrl = "https://some.addr";

        $this->adminAuthMock
            ->expects($this->once())
            ->method('isLoggedIn')
            ->will($this->returnValue(true));
        $this->ssoLogin
            ->expects($this->once())
            ->method("getStartupPageUrl")
            ->willReturn($someUrl);
        $this->redirectMock
            ->expects($this->once())
            ->method('setUrl')
            ->with($someUrl)
            ->will($this->returnSelf());

        $this->assertEquals($this->redirectMock, $this->ssoLogin->execute());
    }


    public function testExecuteException()
    {
        $exMsg = "Some exception to be logged";
        $ex = new \Exception("Some exception to be logged");

        $this->ssoConfigMock
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(true));
        $this->adminAuthMock
            ->method('isLoggedIn')
            ->will($this->returnValue(false));
        $this->providerMock
            ->expects($this->once())
            ->method('get')
            ->will($this->returnValue([]));
        $this->authFactoryMock
            ->expects($this->once())
            ->method('create')
            ->will($this->throwException($ex));
        $this->messageManagerMock
            ->expects($this->once())
            ->method('addErrorMessage')
            ->with($this->stringContains("SSO error"));
        $this->loggerMock
            ->expects($this->once())
            ->method('addError')
            ->with($exMsg);
        $this->redirectMock
            ->expects($this->once())
            ->method('setPath')
            ->with("*")
            ->will($this->returnSelf());

        $this->assertEquals($this->redirectMock, $this->ssoLogin->execute());
    }

    private function prepareMocks()
    {
        $this->contextMock = $this
            ->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->providerMock = $this
            ->getMockBuilder(Provider::class)
            ->setMethods(['get'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->rawResultMock = $this
            ->getMockBuilder(Raw::class)
            ->disableOriginalConstructor()
            ->setMethods(['setHttpResponseCode', 'setHeader', 'setContents'])
            ->getMock();
        $this->authMock = $this
            ->getMockBuilder(Auth::class)
            ->disableOriginalConstructor()
            ->setMethods(['login'])
            ->getMock();
        $this->authFactoryMock = $this
            ->getMockBuilder(AuthFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSsoAdminSamlEnabled'])
            ->getMock();
        $this->redirectMock = $this
            ->getMockBuilder(Redirect::class)
            ->disableOriginalConstructor()
            ->setMethods(['setPath', 'setUrl'])
            ->getMock();
        $this->adminAuthMock = $this
            ->getMockBuilder(AdminAuth::class)
            ->disableOriginalConstructor()
            ->setMethods(['isLoggedIn'])
            ->getMock();
        $this->messageManagerMock = $this
            ->getMockBuilder(ManagerInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['addErrorMessage'])
            ->getMockForAbstractClass();
        $this->loggerMock = $this
            ->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->setMethods(['addError'])
            ->getMock();
        $this->backendHelperMock = $this
            ->getMockBuilder(BackendHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getHomePageUrl'])
            ->getMock();

        $this->contextMock
            ->method('getRedirect')
            ->willReturn($this->redirectMock);
        $this->contextMock
            ->method('getAuth')
            ->willReturn($this->adminAuthMock);
        $this->contextMock
            ->method('getMessageManager')
            ->willReturn($this->messageManagerMock);
    }
}
