<?php
namespace Pg\Sso\Test\Unit\Plugin\Observer;

use Magento\Framework\Event;
use Magento\Framework\Event\Observer;
use Pg\PasswordManagement\Helper\Password\Validation\Expiration;
use Pg\Sso\Plugin\Observer\CheckCustomerPasswordExpirationPlugin;
use Magento\Framework\TestFramework\Unit\BaseTestCase;
use Magento\Customer\Model\Session as CustomerSession;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use Pg\PasswordManagement\Observer\CheckCustomerPasswordExpiration;
use Magento\Framework\Api\AttributeInterface;
use Magento\Customer\Api\Data\CustomerInterface;

/**
 * Class CheckCustomerPasswordExpirationPluginTest
 * @package   Pg\Sso\Test\Unit\Plugin\Observer
 * @copyright Copyright (c) 2018 Orba Sp. z o.o.  (http://orba.co)
 */
class CheckCustomerPasswordExpirationPluginTest extends BaseTestCase
{

    /** @var CheckCustomerPasswordExpirationPlugin */
    protected $plugin;

    /**
     * @var CustomerSession | MockObject
     */
    protected $customerSessionMock;

    /**
     * @var CheckCustomerPasswordExpiration | MockObject
     */
    protected $subjectMock;

    /** @var Observer | MockObject */
    protected $observerMock;

    /** @var Event | MockObject */
    protected $eventMock;

    /** @var CustomerInterface | MockObject */
    protected $customerMock;

    /** @var Expiration | MockObject */
    protected $passwordExpirationMock;

    protected function setUp()
    {
        parent::setUp();

        $this->customerSessionMock = $this->basicMock(CustomerSession::class);
        $this->subjectMock = $this->basicMock(CheckCustomerPasswordExpiration::class);
        $this->observerMock = $this->basicMock(Observer::class);
        $this->eventMock = $this->getMockBuilder(\Magento\Framework\Event::class)
            ->disableOriginalConstructor()
            ->setMethods(['getModel'])
            ->getMock();
        $this->customerMock = $this->getMockBuilder(CustomerInterface::class)
            ->disableOriginalConstructor()->getMock();
        $this->passwordExpirationMock = $this->getMockBuilder(Expiration::class)
            ->disableOriginalConstructor()->getMock();

        $this->plugin = $this->objectManager->getObject(
            CheckCustomerPasswordExpirationPlugin::class,
            [
                'customerSession' => $this->customerSessionMock,
                'passwordExpiration' => $this->passwordExpirationMock
            ]
        );
    }

    public function testBeforeExecute()
    {
        $customAttributeMock = $this->getMockBuilder(AttributeInterface::class)
            ->disableOriginalConstructor()->getMock();
        $customAttributeMock->expects($this->any())->method('getValue')
            ->willReturn('DD2205');

        $this->customerMock->expects($this->once())
            ->method('getCustomAttribute')
            ->with('customer_sso_id')
            ->willReturn($customAttributeMock);
        $this->observerMock->expects($this->once())
            ->method('getEvent')->willReturn($this->eventMock);
        $this->eventMock->expects($this->once())
            ->method('getModel')->willReturn($this->customerMock);
        $this->passwordExpirationMock->expects($this->once())
            ->method('isExpired')->willReturn(true);
        $this->plugin->beforeExecute($this->subjectMock, $this->observerMock);
    }

}
