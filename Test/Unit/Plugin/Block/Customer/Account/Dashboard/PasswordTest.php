<?php

namespace Pg\Sso\Test\Unit\Plugin\Block\Customer\Account\Dashboard;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Customer\Block\Account\Dashboard\Password as OriginalPassword;
use Pg\Sso\Helper\Auth;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Plugin\Block\Customer\Account\Dashboard\Password;

/**
 * Class PasswordTest
 *
 * @package   Pg\Sso\Test\Unit\Plugin\Block\Customer\Account\Dashboard
 * @author    Viktoriia Sultanovska (viktoriia.sultanovska@orba.co)
 * @copyright Copyright (c) Orba Sp. z o.o.  (http://orba.co)
 */
class PasswordTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var Password
     */
    protected $plugin;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $ssoHelperMock;

    /**
     * @var OriginalPassword |\PHPUnit_Framework_MockObject_MockObject
     */
    protected $subjectMock;

    protected function setUp()
    {
        $objectManagerHelper = new ObjectManager($this);
        $this->ssoHelperMock = $this
            ->getMockBuilder(Auth::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->subjectMock
            = $this->getMockBuilder(OriginalPassword::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->plugin = $objectManagerHelper->getObject(
            Password::class,
            ['ssoHelper' => $this->ssoHelperMock]
        );
    }

    public function testAfterToHtmlNotSSO()
    {
        $expectedResult = "<html>";
        $this->ssoHelperMock->expects($this->once())->method('getSsoFlag')
            ->willReturn(false);
        $actualResult = $this->plugin->afterToHtml(
            $this->subjectMock,
            $expectedResult
        );
        $this->assertEquals($expectedResult, $actualResult);
        $this->assertNotEmpty($actualResult);
    }

    public function testAfterToHtmlSSO()
    {
        $this->ssoHelperMock->expects($this->once())->method('getSsoFlag')
            ->willReturn(true);
        $actualResult = $this->plugin->afterToHtml(
            $this->subjectMock,
            "<html>"
        );
        $this->assertEquals(false, $actualResult);
        $this->assertEmpty($actualResult);
    }
}
