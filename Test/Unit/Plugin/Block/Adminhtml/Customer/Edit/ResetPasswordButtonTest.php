<?php

namespace Pg\Sso\Test\Unit\Plugin\Block\Adminhtml\Customer\Edit;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Plugin\Block\Adminhtml\Customer\Edit\ResetPasswordButton;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;
use Magento\Customer\Block\Adminhtml\Edit\ResetPasswordButton as OldButton;

class ResetPasswordButtonTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var CustomerRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $customerRepositoryMock;

    /**
     * @var Registry|\PHPUnit_Framework_MockObject_MockObject
     */
    private $registryMock;

    /**
     * @var OldButton|\PHPUnit_Framework_MockObject_MockObject
     */
    private $oldObjMock;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var ResetPasswordButton
     */
    private $resetPasswordButton;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->resetPasswordButton = $om->getObject(
            ResetPasswordButton::class,
            [
                'ssoConfig' => $this->ssoConfigMock,
                'customerFactory' => $this->customerRepositoryMock,
                'context' => $this->contextMock,
                'registry' => $this->registryMock,
            ]
        );
    }

    /**
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testWhenDisabled()
    {
        $this->registryMock->expects($this->exactly(2))->method('registry')
            ->with(RegistryConstants::CURRENT_CUSTOMER_ID)
            ->willReturn(5);
        $customer = $this->getMockBuilder(Customer::class)
            ->setMethods(['getWebsiteId'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerRepositoryMock->expects($this->once())->method('getById')
            ->with(5)->willReturn($customer);

        $this->ssoConfigMock->expects($this->once())->method('isPasswordResetEnable')->willReturn(false);
        $this->ssoConfigMock->expects($this->once())->method('isSsoFrontendEnabled')->willReturn(true);

        $customer->method('getWebsiteId')->willReturn(6);

        $this->assertEquals(
            [],
            $this->resetPasswordButton->aroundGetButtonData(
                $this->oldObjMock,
                [$this, 'someFakeCallback'],
                ['nieważne co']
            )
        );
    }

    /**
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testWhenEnable()
    {
        $this->registryMock->expects($this->exactly(2))->method('registry')
            ->with(RegistryConstants::CURRENT_CUSTOMER_ID)
            ->willReturn(5);
        $customer = $this->getMockBuilder(Customer::class)->disableOriginalConstructor()->getMock();
        $this->customerRepositoryMock->expects($this->once())->method('getById')
            ->with(5)->willReturn($customer);

        $this->ssoConfigMock->expects($this->once())->method('isPasswordResetEnable')->willReturn(true);

        $someValue = 'some value that old object will return';
        $this->assertEquals(
            $someValue,
            $this->resetPasswordButton->aroundGetButtonData(
                $this->oldObjMock,
                [$this, 'someFakeCallback'],
                $someValue
            )
        );
    }

    public function someFakeCallback($return)
    {
        return $return;
    }

    private function prepareMocks()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerRepositoryMock = $this
            ->getMockBuilder(CustomerRepositoryInterface::class)
            ->getMock();
        $this->registryMock = $this
            ->getMockBuilder(Registry::class)
            ->getMock();
        $this->contextMock = $this
            ->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->oldObjMock = $this
            ->getMockBuilder(OldButton::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
