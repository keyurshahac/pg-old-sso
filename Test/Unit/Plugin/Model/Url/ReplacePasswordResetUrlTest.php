<?php
namespace Pg\Sso\Test\Unit\Plugin\Model\Url;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Customer\Model\Url;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Plugin\Model\Url\ReplacePasswordResetUrl;

class ReplacePasswordResetUrlTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ReplacePasswordResetUrl
     */
    private $replacePasswordResetUrl;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var Url
     */
    private $urlMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->replacePasswordResetUrl = $om->getObject(
            ReplacePasswordResetUrl::class,
            [
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );
        parent::setUp();
    }

    public function testAfterGetForgotPasswordUrlWithDisabledConfigWillReturnUnmodifiedString()
    {
        $inputString = 'a';
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->willReturn(true);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isPasswordResetEnable')
            ->willReturn(false);

        $result = $this->replacePasswordResetUrl->afterGetForgotPasswordUrl(
            $this->urlMock,
            $inputString
        );
        $this->assertSame($result, $inputString);
    }

    /**
     * After bug repair - when disabled SAML SSO in fronted, link for forgot password still modified
     * @see https://www.assembla.com/spaces/orba-pg-m2-internal/tickets/3872-all-%7C-2-5-%7C--bug--e-mail-with-password-setup-not-sent--de2535-
     */
    public function testAfterGetForgotPasswordUrlWithDisabledSSOConfigWillReturnUnmodifiedString()
    {
        $inputString = 'a';
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->willReturn(false);
        $this->ssoConfigMock
            ->expects($this->any())
            ->method('isPasswordResetEnable')
            ->willReturn(true);

        $result = $this->replacePasswordResetUrl->afterGetForgotPasswordUrl(
            $this->urlMock,
            $inputString
        );
        $this->assertSame($result, $inputString);
    }

    public function testAfterGetForgotPasswordUrlWithEnabledConfigReturnStringFromConfig()
    {
        $inputString = 'a';
        $configValue = 'b';
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->willReturn(true);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isPasswordResetEnable')
            ->willReturn(true);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getPasswordResetUrl')
            ->willReturn($configValue);

        $result = $this->replacePasswordResetUrl->afterGetForgotPasswordUrl(
            $this->urlMock,
            $inputString
        );
        $this->assertSame($result, $configValue);
    }

    private function prepareMocks()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'isSsoFrontendEnabled',
                'isPasswordResetEnable',
                'getPasswordResetUrl',
            ])
            ->getMock();
        $this->urlMock = $this
            ->getMockBuilder(Url::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
