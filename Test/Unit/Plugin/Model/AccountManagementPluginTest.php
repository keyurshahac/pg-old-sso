<?php

namespace Pg\Sso\Test\Unit\Plugin\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Helper\Auth as SsoHelper;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Customer\Api\Data\CustomerInterface;
use Pg\Sso\Plugin\Model\AccountManagementPlugin;
use Pg\PasswordManagement\Model\Customer\AccountManagement;

class AccountManagementPluginTest extends \PHPUnit\Framework\TestCase
{
    /** @var CustomerInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $customerInterfaceMock;
    /** @var AccountManagement|\PHPUnit_Framework_MockObject_MockObject */
    private $accountManagementMock;
    /** @var SsoHelper|\PHPUnit_Framework_MockObject_MockObject */
    private $ssoHelperMock;
    /** @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject */
    private $ssoConfigMock;
    /** @var AccountManagementPlugin */
    private $model;

    protected function setUp()
    {
        $this->prepareMocks();

        $objectManager = new ObjectManager($this);
        $this->model = $objectManager->getObject(
            AccountManagementPlugin::class,
            [
                'ssoHelper' => $this->ssoHelperMock,
                'ssoConfig' => $this->ssoConfigMock
            ]
        );
        parent::setUp();
    }

    public function testAroundChangeResetPasswordLinkTokenWithSsoOnlyCustomer()
    {

        $websiteId = uniqid();
        $loginType = SsoConfig::LOGIN_TYPE_BOTH;

        $this->customerInterfaceMock->expects($this->once())->method('getWebsiteId')
            ->willReturn($websiteId);
        $this->ssoConfigMock->expects($this->once())->method('getFrontendLoginType')
            ->with($websiteId)->willReturn($loginType);

        $this->ssoHelperMock->expects($this->once())->method('isSsoOnlyCustomer')
            ->with($this->customerInterfaceMock)->willReturn(true);

        $result = $this->model->aroundChangeResetPasswordLinkToken(
            $this->accountManagementMock,
            function () {
                $this->fail("Sso-only customer doesn't call this");
            },
            $this->customerInterfaceMock,
            ''
        );

        $this->assertSame(true, $result);
    }

    public function testAroundChangeResetPasswordLinkTokenWithLocalCustomer()
    {
        $rpToken = uniqid();
        $expectedResult = uniqid();
        $websiteId = uniqid();
        $loginType = SsoConfig::LOGIN_TYPE_BOTH;

        $this->customerInterfaceMock->expects($this->once())->method('getWebsiteId')
            ->willReturn($websiteId);
        $this->ssoConfigMock->expects($this->once())->method('getFrontendLoginType')
            ->with($websiteId)->willReturn($loginType);
        $this->ssoHelperMock->expects($this->once())->method('isSsoOnlyCustomer')
            ->with($this->customerInterfaceMock)->willReturn(false);


        $result = $this->model->aroundChangeResetPasswordLinkToken(
            $this->accountManagementMock,
            function ($customer, $passwordLinkToken) use ($rpToken, $expectedResult) {
                $this->assertSame($this->customerInterfaceMock, $customer);
                $this->assertSame($rpToken, $passwordLinkToken);
                return $expectedResult;
            },
            $this->customerInterfaceMock,
            $rpToken
        );

        $this->assertSame($expectedResult, $result);
    }

    private function prepareMocks()
    {
        $this->ssoHelperMock = $this->getMockBuilder(SsoHelper::class)
            ->disableOriginalConstructor()->getMock();
        $this->ssoConfigMock = $this->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()->getMock();
        $this->customerInterfaceMock = $this->getMockBuilder(CustomerInterface::class)
            ->disableOriginalConstructor()->getMockForAbstractClass();
        $this->accountManagementMock = $this->getMockBuilder(AccountManagement::class)
            ->disableOriginalConstructor()->getMock();
    }
}
