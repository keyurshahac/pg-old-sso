<?php
namespace Pg\Sso\Test\Unit\Plugin\Model\Notification;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Customer\Model\Notification\Config as OriginalConfig;
use Pg\Sso\Plugin\Customer\Model\Notification\ConfigPlugin;
use Pg\Sso\Model\Config as SsoConfig;

class ConfigPluginTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ConfigPlugin
     */
    private $configPlugin;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var OriginalConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $originalConfigMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->configPlugin = $om->getObject(
            ConfigPlugin::class,
            [
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );
        parent::setUp();
    }

    public function testAroundIsSendPasswordSetLinkEnabledWithSsoConfigEnabledWillReturnTrue()
    {
        $websiteId = 1;
        $callable = function () {
            return;
        };
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->with($websiteId)
            ->willReturn(false);

        $result = $this->configPlugin->aroundIsSendPasswordSetLinkEnabled(
            $this->originalConfigMock,
            $callable,
            $websiteId
        );
        $this->assertTrue($result);
    }

    public function testAroundIsSendPasswordSetLinkEnabledWithNativeLoginConfigDisabledWillReturnFalse()
    {
        $websiteId = 1;
        $callable = function () {
            return;
        };
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->with($websiteId)
            ->willReturn(true);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('keepFrontendSamlNativeLogin')
            ->with($websiteId)
            ->willReturn(false);

        $result = $this->configPlugin->aroundIsSendPasswordSetLinkEnabled(
            $this->originalConfigMock,
            $callable,
            $websiteId
        );
        $this->assertFalse($result);
    }

    public function testAroundIsSendPasswordSetLinkEnabledWithConfigsDisabledWillReturnCallable()
    {
        $websiteId = 1;
        $callable = function ($websiteId) {
            return $websiteId;
        };
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->with($websiteId)
            ->willReturn(true);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('keepFrontendSamlNativeLogin')
            ->with($websiteId)
            ->willReturn(true);

        $result = $this->configPlugin->aroundIsSendPasswordSetLinkEnabled(
            $this->originalConfigMock,
            $callable,
            $websiteId
        );
        $this->assertSame($result, $websiteId);
    }

    private function prepareMocks()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'isSsoFrontendEnabled',
                'keepFrontendSamlNativeLogin'
            ])
            ->getMock();
        $this->originalConfigMock = $this
            ->getMockBuilder(OriginalConfig::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
