<?php

namespace Pg\Sso\Test\Unit\Plugin\Action;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Plugin\Action\Logout;
use Magento\Customer\Controller\Account\Logout as OldLogout;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\Result\Redirect;

class LogoutTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Logout
     */
    private $logout;

    /**
     * @var OldLogout|\PHPUnit_Framework_MockObject_MockObject
     */
    private $controllerMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var RedirectFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $resultRedirectFactoryMock;

    /**
     * @var Redirect|\PHPUnit_Framework_MockObject_MockObject
     */
    private $redirectMock;

    /**
     * @var Http|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    /**
     *
     */
    protected function setUp()
    {
        $this->prepareMocks();

        $om = new ObjectManager($this);
        $this->logout = $om->getObject(
            Logout::class,
            [
                'ssoConfig' => $this->ssoConfigMock,
                'resultRedirectFactory' => $this->resultRedirectFactoryMock,
            ]
        );
        parent::setUp();
    }

    public function testAroundExecuteWithDisabledConfigWillReturnCallable()
    {
        $expected = "someResult";
        $callableMock = function () use ($expected) {
            return $expected;
        };

        $this->ssoConfigMock
            ->expects($this->once())
            ->method("isSloFrontendEnabled")
            ->will($this->returnValue(false));
        $this->resultRedirectFactoryMock
            ->expects($this->never())
            ->method('create')
            ->will($this->returnValue($this->redirectMock));

        $result = $this->logout->aroundExecute(
            $this->controllerMock,
            $callableMock,
            $this->requestMock
        );
        $this->assertSame($expected, $result);
    }

    public function testAroundExecuteWiEnabledConfigWillReturnRedirect()
    {
        $callableMock = function () {
            return '';
        };
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSloFrontendEnabled')
            ->willReturn(true);
        $this->resultRedirectFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($this->redirectMock);
        $this->redirectMock
            ->expects($this->once())
            ->method('setPath')
            ->with(Logout::PATH_SAML_SSO_LOGOUT);

        $result = $this->logout->aroundExecute(
            $this->controllerMock,
            $callableMock,
            $this->requestMock
        );
        $this->assertsame($result, $this->redirectMock);
    }

    private function prepareMocks()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSloFrontendEnabled'])
            ->getMock();
        $this->resultRedirectFactoryMock = $this
            ->getMockBuilder(RedirectFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->redirectMock = $this
            ->getMockBuilder(Redirect::class)
            ->disableOriginalConstructor()
            ->setMethods(['setPath'])
            ->getMock();
        $this->controllerMock = $this
            ->getMockBuilder(OldLogout::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->requestMock = $this
            ->getMockBuilder(Http::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
