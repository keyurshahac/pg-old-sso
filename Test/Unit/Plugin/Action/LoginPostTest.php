<?php

namespace Pg\Sso\Test\Unit\Plugin\Action;

use Pg\Sso\Plugin\Action\LoginPost;
use Magento\Customer\Controller\Account\LoginPost as OldLogin;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\Result\Redirect;

class LoginPostTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var LoginPost
     */
    protected $loginPost;

    /**
     * @var OldLogin|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $oldLoginMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $ssoConfigMock;

    /**
     * @var RedirectFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $redirectFactoryMock;

    /**
     * @var Redirect|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $redirectMock;

    /**
     * @var Http|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $requestMock;

    /**
     * @var \Closure
     */
    protected $proceed;

    /**
     * @var string
     */
    protected $proceedResult = "someResult";

    protected function setUp()
    {
        $this->ssoConfigMock = $this->getMockBuilder(SsoConfig::class)->
        disableOriginalConstructor()->
        setMethods(["isSsoFrontendEnabled", "keepFrontendSamlNativeLogin"])->
        getMock();

        $this->redirectFactoryMock = $this->getMockBuilder(RedirectFactory::class)->
        disableOriginalConstructor()->
        setMethods(["create"])->
        getMock();

        $this->redirectMock = $this->getMockBuilder(Redirect::class)->
        disableOriginalConstructor()->
        setMethods(['setPath'])->
        getMock();

        $this->redirectFactoryMock->
        expects($this->any())->
        method('create')->
        will($this->returnValue($this->redirectMock));

        $this->oldLoginMock = $this->getMockBuilder(OldLogin::class)->
        disableOriginalConstructor()->
        getMock();


        $this->requestMock = $this->getMockBuilder(Http::class)->
        disableOriginalConstructor()->
        getMock();

        $this->loginPost = new LoginPost($this->ssoConfigMock, $this->redirectFactoryMock);

        $expectedResult = $this->proceedResult;

        $this->proceed = function ($request) use ($expectedResult) {
            return $expectedResult;
        };

        parent::setUp();
    }

    /**
     * Test SSO disbaled - just no actions
     */
    public function testAroundExecuteDisbaled()
    {

        $this->ssoConfigMock->
        expects($this->once())->
        method('isSsoFrontendEnabled')->
        will($this->returnValue(false));

        $this->assertEquals(
            $this->proceedResult,
            $this->loginPost->aroundExecute(
                $this->oldLoginMock,
                $this->proceed,
                $this->requestMock
            )
        );
    }


    /**
     * Test SSO enable no native login
     */
    public function testAroundExecuteSamlEnabledNoNativeLogin()
    {

        $this->ssoConfigMock->
        expects($this->once())->
        method('isSsoFrontendEnabled')->
        will($this->returnValue(true));

        $this->ssoConfigMock->
        expects($this->once())->
        method('keepFrontendSamlNativeLogin')->
        will($this->returnValue(false));

        $this->assertEquals(
            $this->redirectMock,
            $this->loginPost->aroundExecute(
                $this->oldLoginMock,
                $this->proceed,
                $this->requestMock
            )
        );
    }

    /**
     * Test SSO disbaled enabled native login
     */
    public function testAroundExecuteSamlEnabledNativeLogin()
    {

        $this->ssoConfigMock->
        expects($this->once())->
        method('isSsoFrontendEnabled')->
        will($this->returnValue(true));

        $this->ssoConfigMock->
        expects($this->once())->
        method('keepFrontendSamlNativeLogin')->
        will($this->returnValue(true));

        $this->assertEquals(
            $this->proceedResult,
            $this->loginPost->aroundExecute(
                $this->oldLoginMock,
                $this->proceed,
                $this->requestMock
            )
        );
    }
}
