<?php

namespace Pg\Sso\Test\Unit\Plugin\Action\Adminhtml;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Plugin\Action\Adminhtml\Login;
use Magento\Backend\Controller\Adminhtml\Auth\Login as BackendLogin;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Model\Config as SsoConfig;

class LoginTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Login
     */
    private $login;

    /**
     * @var BackendLogin|\PHPUnit_Framework_MockObject_MockObject
     */
    private $controllerMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var RedirectFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $resultRedirectFactoryMock;

    /**
     * @var Redirect|\PHPUnit_Framework_MockObject_MockObject
     */
    private $redirectMock;

    /**
     * @var Http|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    /**
     * @var \Closure
     */
    private $proceedMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->login = $om->getObject(
            Login::class,
            [
                'resultRedirectFactory' => $this->resultRedirectFactoryMock,
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );

        parent::setUp();
    }

    public function testAroundExecuteWithDisabledSamlConfigWillNotCallCreateOnRedirectFactory()
    {
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(false));
        $this->resultRedirectFactoryMock
            ->expects($this->never())
            ->method('create');

        $this->login->aroundExecute(
            $this->controllerMock,
            $this->proceedMock,
            $this->requestMock
        );
    }

    public function testAroundExecuteWithEnabledSamlAndNativeLoginConfigWillNotCallCreateOnRedirectFactory()
    {
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(true));
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('keepAdminSamlNativeLogin')
            ->willReturn(true);
        $this->resultRedirectFactoryMock
            ->expects($this->never())
            ->method('create');

        $this->login->aroundExecute(
            $this->controllerMock,
            $this->proceedMock,
            $this->requestMock
        );
    }

    public function testAroundExecuteWithDisabledConfigWillReturnCallable()
    {
        $expected = "someResult";
        $callableMock = function () use ($expected) {
            return $expected;
        };

        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(false));
        $this->resultRedirectFactoryMock
            ->expects($this->never())
            ->method('create');

        $result = $this->login->aroundExecute(
            $this->controllerMock,
            $callableMock,
            $this->requestMock
        );

        $this->assertsame($result, $expected);
    }

    public function testAroundExecuteWithEnabledConfigWillReturnRedirect()
    {
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->will($this->returnValue(true));
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('keepAdminSamlNativeLogin')
            ->willReturn(false);
        $this->resultRedirectFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($this->redirectMock);
        $this->redirectMock
            ->expects($this->once())
            ->method('setPath')
            ->with(Login::PATH_SAML_SSO_LOGIN);

        $result = $this->login->aroundExecute(
            $this->controllerMock,
            $this->proceedMock,
            $this->requestMock
        );
        $this->assertSame($result, $this->redirectMock);
    }

    private function prepareMocks()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods(["isSsoAdminSamlEnabled", "isSsoAdminEnabled", "keepAdminSamlNativeLogin"])
            ->getMock();
        $this->resultRedirectFactoryMock = $this
            ->getMockBuilder(RedirectFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create"])
            ->getMock();
        $this->redirectMock = $this
            ->getMockBuilder(Redirect::class)
            ->disableOriginalConstructor()
            ->setMethods(['setPath'])
            ->getMock();
        $this->resultRedirectFactoryMock
            ->expects($this->any())
            ->method('create')
            ->will($this->returnValue($this->redirectMock));
        $this->controllerMock = $this
            ->getMockBuilder(BackendLogin::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->requestMock = $this
            ->getMockBuilder(Http::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->proceedMock = function () {
            return;
        };
    }
}
