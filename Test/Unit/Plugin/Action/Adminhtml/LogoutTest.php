<?php

namespace Pg\Sso\Test\Unit\Plugin\Action\Adminhtml;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Plugin\Action\Adminhtml\Logout;
use Magento\Backend\Controller\Adminhtml\Auth\Logout as BackendLogout;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Model\Config as SsoConfig;

class LogoutTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Logout
     */
    private $logout;

    /**
     * @var BackendLogout|\PHPUnit_Framework_MockObject_MockObject
     */
    private $controllerMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var RedirectFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $resultRedirectFactoryMock;

    /**
     * @var Redirect|\PHPUnit_Framework_MockObject_MockObject
     */
    private $redirectMock;

    /**
     * @var Http|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    /**
     * @var \Closure
     */
    private $proceedMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->logout = $om->getObject(
            Logout::class,
            [
                'resultRedirectFactory' => $this->resultRedirectFactoryMock,
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );

        parent::setUp();
    }

    public function testAroundExecuteWithSsoAdminDisabledWillReturnCallableWithoutCreatingRedirect()
    {
        $expected = "someResult";
        $callableMock = function () use ($expected) {
            return $expected;
        };

        $this->ssoConfigMock
            ->expects($this->once())
            ->method("isSsoAdminEnabled")
            ->will($this->returnValue(false));
        $this->resultRedirectFactoryMock
            ->expects($this->never())
            ->method('create');


        $result = $this->logout->aroundExecute(
            $this->controllerMock,
            $callableMock,
            $this->requestMock
        );
        $this->assertSame($result, $expected);
    }

    public function testAroundExecuteWithAdminSamlConfigEnabledWillReturnRedirect()
    {
        $this->ssoConfigMock
            ->expects($this->once())
            ->method("isSsoAdminEnabled")
            ->will($this->returnValue(true));
        $this->ssoConfigMock
            ->expects($this->once())
            ->method("isSloAdminSamlEnabled")
            ->will($this->returnValue(true));
        $this->resultRedirectFactoryMock
            ->expects($this->any())
            ->method('create')
            ->will($this->returnValue($this->redirectMock));
        $this->redirectMock
            ->expects($this->once())
            ->method('setPath')
            ->with(Logout::PATH_SSO_SAML_LOGOUT)
            ->will($this->returnSelf());

        $result = $this->logout->aroundExecute(
            $this->controllerMock,
            $this->proceedMock,
            $this->requestMock
        );
        $this->assertSame($result, $this->redirectMock);
    }

    public function testAroundExecuteWithAdminSamlConfigDisabledWillCallCallableAndReturnRedirect()
    {
        $callableWasCalled = false;
        $callableMock = function () use (&$callableWasCalled) {
            $callableWasCalled = true;
        };

        $this->ssoConfigMock
            ->expects($this->once())
            ->method("isSsoAdminEnabled")
            ->will($this->returnValue(true));
        $this->ssoConfigMock
            ->expects($this->once())
            ->method("isSloAdminSamlEnabled")
            ->will($this->returnValue(false));
        $this->resultRedirectFactoryMock
            ->expects($this->any())
            ->method('create')
            ->will($this->returnValue($this->redirectMock));
        $this->redirectMock
            ->expects($this->once())
            ->method('setPath')
            ->with(Logout::PATH_SSO_SAML_LOGGED_OUT)
            ->will($this->returnSelf());

        $result = $this->logout->aroundExecute(
            $this->controllerMock,
            $callableMock,
            $this->requestMock
        );
        $this->assertSame($result, $this->redirectMock);
        $this->assertTrue($callableWasCalled);
    }

    private function prepareMocks()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'isSsoAdminSamlEnabled',
                'isSsoAdminEnabled',
                'keepAdminSamlNativeLogin',
                'isSloAdminSamlEnabled'
            ])
            ->getMock();
        $this->resultRedirectFactoryMock = $this
            ->getMockBuilder(RedirectFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create"])
            ->getMock();

        $this->redirectMock = $this
            ->getMockBuilder(Redirect::class)
            ->disableOriginalConstructor()
            ->setMethods(['setPath'])
            ->getMock();
        $this->controllerMock = $this
            ->getMockBuilder(BackendLogout::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->requestMock = $this
            ->getMockBuilder(Http::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->proceedMock = function () {
            return;
        };
    }
}
