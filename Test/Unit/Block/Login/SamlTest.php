<?php

namespace Pg\Sso\Test\Unit\Block\Login;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Block\Login\Saml;
use Pg\Sso\Model\Config as SsoConfig;

final class SamlTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var Saml
     */
    private $saml;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->saml = $om->getObject(
            Saml::class,
            [
                'context' => $this->contextMock,
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );
    }

    public function testIsEnabledWillReturnConfigValue()
    {
        $expected = 1;
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->willReturn($expected);

        $result = $this->saml->isEnabled();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlTextWillReturnConfigValue()
    {
        $expected = 1;
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlTextFrontend')
            ->willReturn($expected);

        $result = $this->saml->getSamlText();
        $this->assertSame($result, $expected);
    }

    public function testIsPasswordResetEnabledWillReturnConfigValue()
    {
        $expected = 1;
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isPasswordResetEnable')
            ->willReturn($expected);

        $result = $this->saml->isPasswordResetEnabled();
        $this->assertSame($result, $expected);
    }

    public function testGetPasswordResetUrlWillReturnConfigValue()
    {
        $expected = 1;
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getPasswordResetUrl')
            ->willReturn($expected);

        $result = $this->saml->getPasswordResetUrl();
        $this->assertSame($result, $expected);
    }

    private function prepareMocks()
    {
        $this->contextMock = $this
            ->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'isSsoFrontendEnabled',
                'getSamlTextFrontend',
                'isPasswordResetEnable',
                'getPasswordResetUrl',
            ])
            ->getMock();
    }
}
