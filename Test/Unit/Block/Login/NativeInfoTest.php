<?php

namespace Pg\Sso\Test\Unit\Block\Login;

use Pg\Sso\Block\Login\NativeInfo;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Registration;
use Magento\Customer\Model\Url;
use Magento\Checkout\Helper\Data as CheckoutData;
use Magento\Framework\Url\Helper\Data as CoreData;

class NativeInfoTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var NativeInfo
     */
    private $nativeInfo;

    protected function setUp()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->getMock();
        $context = $this
            ->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $registration = $this
            ->getMockBuilder(Registration::class)
            ->disableOriginalConstructor()
            ->getMock();
        $url = $this
            ->getMockBuilder(Url::class)
            ->disableOriginalConstructor()
            ->getMock();
        $chekcoutData = $this
            ->getMockBuilder(CheckoutData::class)
            ->disableOriginalConstructor()
            ->getMock();
        $coreData = $this
            ->getMockBuilder(CoreData::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->nativeInfo = new NativeInfo(
            $this->ssoConfigMock,
            $context,
            $registration,
            $url,
            $chekcoutData,
            $coreData
        );
    }

    public function testWhenNoNativeLogin()
    {
        $this->ssoConfigMock->expects($this->once())->method('isSsoFrontendEnabled')->willReturn(true);
        $this->ssoConfigMock->expects($this->once())->method('keepFrontendSamlNativeLogin')->willReturn(false);

        $this->assertEquals('', $this->nativeInfo->toHtml());
    }
}
