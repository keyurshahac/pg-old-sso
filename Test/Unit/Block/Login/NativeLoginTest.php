<?php

namespace Pg\Sso\Test\Unit\Block\Login;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url;
use Pg\Sso\Block\Login\NativeLogin;
use Pg\Sso\Model\Config as SsoConfig;

class NativeLoginTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var NativeLogin|\PHPUnit_Framework_MockObject_MockObject
     */
    private $nativeLogin;

    /**
     * @var Url|\PHPUnit_Framework_MockObject_MockObject
     */
    private $urlMock;

    /**
     * @var Session|\PHPUnit_Framework_MockObject_MockObject
     */
    private $sessionMock;

    /**
     * @var Context
     */
    private $contextMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->nativeLogin = $om->getObject(
            NativeLogin::class,
            [
                'ssoConfig' => $this->ssoConfigMock,
                'context' => $this->contextMock,
                'customerSession' => $this->sessionMock,
                'customerUrl' => $this->urlMock,
            ]
        );
    }

    public function testWhenNoNativeLogin()
    {
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoFrontendEnabled')
            ->willReturn(true);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('keepFrontendSamlNativeLogin')
            ->willReturn(false);

        $this->assertEquals('', $this->nativeLogin->toHtml());
    }

    private function prepareMocks()
    {
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->contextMock = $this
            ->getMockBuilder(Context::class)
            ->disableOriginalConstructor()->getMock();
        $this->sessionMock = $this
            ->getMockBuilder(Session::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->urlMock = $this
            ->getMockBuilder(Url::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
