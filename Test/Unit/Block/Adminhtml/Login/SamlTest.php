<?php

namespace Pg\Sso\Test\Unit\Block\Adminhtml\Login;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Block\Adminhtml\Login\Saml;
use Pg\Sso\Model\Config as SsoConfig;

class SamlTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Saml|\PHPUnit_Framework_MockObject_MockObject
     */
    private $saml;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->saml = $om->getObject(
            Saml::class,
            [
                'context' => $this->contextMock,
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );
    }

    public function testIsEnabledWillReturnConfigValue()
    {
        $expected = 1;
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('isSsoAdminSamlEnabled')
            ->willReturn($expected);

        $result = $this->saml->isEnbaled();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlTextWillReturnConfigValue()
    {
        $expected = 1;
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlTextAdminhtml')
            ->willReturn($expected);

        $result = $this->saml->getSamlText();
        $this->assertSame($result, $expected);
    }

    private function prepareMocks()
    {
        $this->contextMock = $this
            ->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSsoAdminSamlEnabled', 'getSamlTextAdminhtml'])
            ->getMock();
    }
}
