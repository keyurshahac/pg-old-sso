<?php

namespace Pg\Sso\Test\Unit\App\Action\Plugin\Adminhtml;

use Pg\Sso\App\Action\Plugin\Adminhtml\Authentication;

class AuthenticationTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Test constructor overload
     */
    public function testConstructor(){
        $instance = new Authentication(
            $this->getMockBuilder(\Magento\Backend\Model\Auth::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(\Magento\Backend\Model\UrlInterface::class)->disableOriginalConstructor()->getMockForAbstractClass(),
            $this->getMockBuilder(\Magento\Framework\App\ResponseInterface::class)->disableOriginalConstructor()->getMockForAbstractClass(),
            $this->getMockBuilder(\Magento\Framework\App\ActionFlag::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(\Magento\Framework\Message\ManagerInterface::class)->disableOriginalConstructor()->getMockForAbstractClass(),
            $this->getMockBuilder(\Magento\Backend\Model\UrlInterface::class)->disableOriginalConstructor()->getMockForAbstractClass(),
            $this->getMockBuilder(\Magento\Framework\Controller\Result\RedirectFactory::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(\Magento\Backend\App\BackendAppList::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(\Magento\Framework\Data\Form\FormKey\Validator::class)->disableOriginalConstructor()->getMock()
        );

        $reflection = new \ReflectionProperty(Authentication::class, '_openActions');
        $reflection->setAccessible(true);

        // @todo add more here
        $this->assertContains("metadata", $reflection->getValue($instance));
    }
}
