<?php
/**
 * Created by PhpStorm.
 * User: orba
 * Date: 16.10.2018
 * Time: 13:07
 */

namespace Pg\Sso\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Api\AttributeInterface;
use Pg\Sso\Helper\Auth;
use Pg\Sso\Model\Config\Source\Attribute;
use Magento\Framework\Session\SessionManagerInterface;
use PHPUnit\Framework\TestCase;

class AuthTest extends TestCase
{
    /** @var SessionManagerInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $sessionMock;
    /** @var CustomerInterface|\PHPUnit_Framework_MockObject_MockObject $customerInterfaceMock */
    private $customerInterfaceMock;
    /** @var AttributeInterface|\PHPUnit_Framework_MockObject_MockObject $customerInterfaceMock */
    private $attributeInterfaceMock;
    /** @var Auth */
    private $model;

    public function setUp()
    {
        $this->sessionMock = $this->getMockBuilder(SessionManagerInterface::class)
            ->setMethods(['setSsoFlag', 'getSsoFlag'])->getMockForAbstractClass();
        $this->customerInterfaceMock = $this->getMockBuilder(CustomerInterface::class)
            ->setMethods(['getCustomAttribute'])->getMockForAbstractClass();
        $this->attributeInterfaceMock = $this->getMockBuilder(AttributeInterface::class)
            ->setMethods(['getValue'])->getMockForAbstractClass();

        $objectManager = new ObjectManager($this);
        $this->model = $objectManager->getObject(Auth::class, ['session' => $this->sessionMock]);
    }

    /**
     * @testWith [ false ]
     *           [ true ]
     */
    public function testSetSsoFlag($value)
    {
        $this->sessionMock->expects($this->once())->method('setSsoFlag')->with($value);

        $this->model->setSsoFlag($value);
    }

    public function testSetSsoFlagDefault()
    {
        $this->sessionMock->expects($this->once())->method('setSsoFlag')->with(true);

        $this->model->setSsoFlag();
    }

    /**
     * @testWith [ false, false ]
     *           [ true, true ]
     *           [ null, false ]
     */
    public function testGetSsoFlag($actual, $expected)
    {
        $this->sessionMock->expects($this->once())->method('getSsoFlag')->willReturn($actual);

        $this->assertEquals($expected, $this->model->getSsoFlag());
    }

    public function testIsSsoOnlyCustomerWithNull()
    {
        $this->customerInterfaceMock->expects($this->once())->method('getCustomAttribute')
            ->with(Attribute::SSO_ID)->willReturn(null);

        $result = $this->model->isSsoOnlyCustomer($this->customerInterfaceMock);
        $this->assertSame(false, $result);
    }

    /**
     * @testWith [ "", false ]
     *           [ false, false ]
     *           [ null, false ]
     *           [ "abc", true ]
     *           [ "123", true ]
     *           [ "0", true ]
     *           [ 0, true ]
     *           [ 123, true ]
     */
    public function testIsSsoOnlyCustomerWithAttribute($attributeValue, $expectedResult)
    {
        $this->attributeInterfaceMock->expects($this->once())->method('getValue')
            ->willReturn($attributeValue);
        $this->customerInterfaceMock->expects($this->once())->method('getCustomAttribute')
            ->with(Attribute::SSO_ID)->willReturn($this->attributeInterfaceMock);

        $result = $this->model->isSsoOnlyCustomer($this->customerInterfaceMock);
        $this->assertSame($expectedResult, $result);
    }
}
