<?php
namespace Pg\Sso\Test\Unit\Model\Notification;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Model\Notification\Activation as ActivationModel;
use \PHPUnit_Framework_MockObject_MockObject as MockObject;
use Magento\Store\Model\StoreManagerInterface;
use Pg\Customer\Model\Notification\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Api\Data\StoreInterface;
use Pg\Customer\Model\Notification\Config;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Customer\Helper\Email as EmailHelper;
use PHPUnit\Framework\TestCase;

/**
 * Class ActivationTest
 * @package   Pg\Sso\Test\Unit\Model\Notification
 * @copyright Copyright (c) 2018 Orba Sp. z o.o.  (http://orba.co)
 */
class ActivationTest extends \Pg\Customer\Test\Unit\Model\Notification\NotificationAbstract
{
    /**
     * @var ActivationModel
     */
    protected $model;

    /**
     * @var CustomerInterface | MockObject
     */
    protected $customerMock;

    /**
     * @var \Pg\Utils\Helper\DataObject | MockObject
     */
    protected $dataObjectHelperMock;

    /**
     * @var Config | MockObject
     */
    protected $configMock;

    /**
     * @var SsoConfig | MockObject
     */
    protected $ssoConfigMock;

    /**
     * @var StoreManagerInterface | MockObject
     */
    protected $storeManagerMock;

    /**
     * @var StoreInterface | MockObject
     */
    protected $storeMock;

    /**
     * @var MockObject
     */
    protected $emailHelperMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $transportBuilderMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $hierarchyManagementMock;

    public function setUp()
    {
        parent::setUp();


        $this->ssoConfigMock = $this->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new ObjectManager($this);
        $this->model = $objectManager->getObject(ActivationModel::class, [
            'emailHelper' => $this->emailHelperMock,
            'hierarchyManagement' => $this->hierarchyManagementMock,
            'transportBuilder' => $this->transportBuilderMock,
            'storeManager' => $this->storeManagerMock,
            'config' => $this->configMock,
            'dataObjectHelper' => $this->dataObjectHelperMock,
            'ssoConfig' => $this->ssoConfigMock
        ]);
    }

    /**
     * @dataProvider providerAllScenarious
     **/
    public function testNotifyEmail(
        $isSsoFrontendEnabled,
        $keepFrontendSamlNativeLogin,
        $ssoIdValue,
        $expectedEmailTemplate,
        $loginType
    )
    {
        $customer = 123;
        $websiteId = 8;

        $this->customerMock
            ->method('getId')
            ->willReturn($customer);

        $this->customerMock->expects($this->once())->method('getWebsiteId')
            ->willReturn($websiteId);
        $this->ssoConfigMock->expects($this->once())->method('getFrontendLoginType')
            ->with($websiteId)->willReturn($loginType);




        $this->hierarchyManagementMock
            ->expects($this->atLeastOnce())
            ->method('getIdsByCustomerId')
            ->with($customer)
            ->willReturn([55]);

        $this->emailHelperMock->expects($this->atLeastOnce())
            ->method('getEmailRecipient')
            ->with($this->customerMock)
            ->willReturn([
                'email' => 'test@orba.co',
                'name' => 'Orba Test'
            ]);

        $this->ssoConfigMock->expects($this->any())
            ->method('isSsoFrontendEnabled')
            ->willReturn($isSsoFrontendEnabled);

        $this->ssoConfigMock->expects($this->any())
            ->method('keepFrontendSamlNativeLogin')
            ->willReturn($keepFrontendSamlNativeLogin);

        $this->transportBuilderMock->expects($this->once())
            ->method('addTo')
            ->with('test@orba.co', 'Orba Test');

        $this->dataObjectHelperMock->expects($this->atLeastOnce())
            ->method('getCustomAttributeValue')
            ->willReturn($ssoIdValue);

        $this->configMock->expects($this->atLeastOnce())
            ->method('getByPath')
            ->with($expectedEmailTemplate);
        $this->model->notify($this->customerMock);
    }

    /**
     * Parameters description
     * 1: ssoConfig->isSsoFrontendEnabled() return value
     * 2: ssoConfig->keepFrontendSamlNativeLogin() return value
     * 3: SSO id form field value
     * 4: expected email template
     * @return array
     */
    public function providerAllScenarious() {
        return [
          // only native login enabled, sso id field doesn't matter
          [false, true, 'DD2255', ActivationModel::XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE, SsoConfig::LOGIN_TYPE_NATIVE_ONLY],

          // only native login enabled, sso id field doesn't matter
          [false, true, '', ActivationModel::XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE, SsoConfig::LOGIN_TYPE_NATIVE_ONLY],

          // only sso
          [true, false, 'DD2255', ActivationModel::XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE_SSO, SsoConfig::LOGIN_TYPE_SSO_ONLY],
          [true, false, '', ActivationModel::XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE_SSO, SsoConfig::LOGIN_TYPE_SSO_ONLY],

          // both login method enabled, sso id provided
          [true, true, 'DD2255', ActivationModel::XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE_SSO, SsoConfig::LOGIN_TYPE_BOTH],

          // both login method enabled, sso id missing
          [true, true, '', ActivationModel::XML_PATH_CONFIRMED_EMPLOYEE_EMAIL_TEMPLATE, SsoConfig::LOGIN_TYPE_BOTH]
        ];
    }
}
