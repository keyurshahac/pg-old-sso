<?php

namespace Pg\Sso\Test\Unit\Model\Saml;

use Pg\Sso\Model\Saml\SettingsFactory;
use Magento\Framework\ObjectManagerInterface;
use Pg\Sso\Model\Saml\Settings;

class SettingsFactoryTest extends \PHPUnit\Framework\TestCase{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var string
     */
    protected $instanceName;

    protected function setUp()
    {
        $this->objectManager = $this->getMockBuilder(ObjectManagerInterface::class)->
            disableOriginalConstructor()->
            setMethods(['create'])->
            getMockForAbstractClass();

        $this->instanceName = "Pg\\Sso\\Model\\Saml\\Settings";

        parent::setUp();
    }

    /**
     * Test create
     */
    public function testCreate(){
        $sampleSettings = ['some'=>'data'];

        $settingsMock = $this->getMockBuilder(Settings::class)->
            disableOriginalConstructor()->
            getMock();

        $this->objectManager->
            expects($this->once())->
            method('create')->
            with($this->instanceName, ['settings'=>$sampleSettings])->
            willReturn($settingsMock);


        $factory = new SettingsFactory(
            $this->objectManager,
            $this->instanceName
        );

        $this->assertEquals($settingsMock, $factory->create($sampleSettings));
    }
}