<?php

namespace Pg\Sso\Test\Unit\Model\Saml;


use Pg\Sso\Model\Saml\Auth;
use Pg\Sso\Model\Saml\AuthFactory;
use Magento\Framework\ObjectManagerInterface;

class AuthFactoryTest extends \PHPUnit\Framework\TestCase{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var string
     */
    protected $instanceName;

    protected function setUp()
    {
        $this->objectManager = $this->getMockBuilder(ObjectManagerInterface::class)->
            disableOriginalConstructor()->
            setMethods(['create'])->
            getMockForAbstractClass();

        $this->instanceName = "Pg\\Sso\\Model\\Saml\\Auth";

        parent::setUp();
    }

    /**
     * Test create
     */
    public function testCreate(){
        $sampleSettings = ['some'=>'data'];

        $settingsMock = $this->getMockBuilder(Auth::class)->
            disableOriginalConstructor()->
            getMock();

        $this->objectManager->
            expects($this->once())->
            method('create')->
            with($this->instanceName, ['oldSettings'=>$sampleSettings])->
            willReturn($settingsMock);


        $factory = new AuthFactory(
            $this->objectManager,
            $this->instanceName
        );

        $this->assertEquals($settingsMock, $factory->create($sampleSettings));
    }
}