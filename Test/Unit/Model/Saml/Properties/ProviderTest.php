<?php

namespace Pg\Sso\Test\Unit\Model\Saml\Properties;

use Magento\Framework\App\Area;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Model\Saml\Properties\Provider;
use Magento\Framework\UrlInterface;
use Pg\Sso\Model\Config as SsoConfig;

class ProviderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var UrlInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $urlMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /**
     * @var Provider
     */
    private $providerFront;

    /**
     * @var Provider
     */
    private $providerAdmin;

    /**
     *
     */
    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->providerFront = $om->getObject(
            Provider::class,
            [
                'area' => Area::AREA_FRONTEND,
                'url' => $this->urlMock,
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );
        $this->providerAdmin = $om->getObject(
            Provider::class,
            [
                'area' => Area::AREA_ADMINHTML,
                'url' => $this->urlMock,
                'ssoConfig' => $this->ssoConfigMock,
            ]
        );
    }

    public function testGetWithFrontendWillCallCorrespondingConfigMethods()
    {
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlEntityFrontend');
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlNameIdFrontend');
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlSsoFrontend');
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlSloFrontend');
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlCertDataFrontend');

        $this->providerFront->get();
    }

    public function testGetWithFrontendWillReturnExpectedArray()
    {
        $samlEntityId = 1;
        $samlNameId = 2;
        $samlSso = 3;
        $samsSlo = 4;
        $samlCertData = 5;
        $samlMetadataUrl = 6;
        $samlAcsUrl = 7;
        $samlSloUrl = 8;

        $expected = [
            'sp' => [
                'entityId' => $samlMetadataUrl,
                'assertionConsumerService' => ['url' => $samlAcsUrl],
                'singleLogoutService' => ['url' => $samlSloUrl],
                'NameIDFormat' => $samlNameId,
            ],
            'idp' => [
                'entityId' => $samlEntityId,
                'singleSignOnService' => ['url' => $samlSso],
                'singleLogoutService' => ['url' => $samsSlo],
                'x509cert' => $samlCertData,
            ],
            'security' => [
                'requestedAuthnContext' => false,
            ],
        ];

        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlEntityFrontend')
            ->willReturn($samlEntityId);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlNameIdFrontend')
            ->willReturn($samlNameId);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlSsoFrontend')
            ->willReturn($samlSso);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlSloFrontend')
            ->willReturn($samsSlo);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlCertDataFrontend')
            ->willReturn($samlCertData);

        $this->urlMock
            ->expects($this->exactly(3))
            ->method('getUrl')
            ->will(
                $this->returnValueMap(
                    [
                        [Provider::PATH_SAML_METADATA, ['key' => false], $samlMetadataUrl],
                        [Provider::PATH_SAML_ACS, ['key' => false], $samlAcsUrl],
                        [Provider::PATH_SAML_SLO, ['key' => false], $samlSloUrl]
                    ]
                )
            );

        $result = $this->providerFront->get();

        $this->assertSame($result, $expected);
    }

    public function testGetWithAdminhtmlWillCallCorrespondingConfigMethods()
    {
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlEntityAdminhtml');
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlNameIdAdminhtml');
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlSsoAdminhtml');
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlSloAdminhtml');
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlCertDataAdminhtml');

        $this->providerAdmin->get();
    }

    public function testGetWithAdminhtmlWillReturnExpectedArray()
    {
        $samlEntityId = 1;
        $samlNameId = 2;
        $samlSso = 3;
        $samsSlo = 4;
        $samlCertData = 5;
        $samlMetadataUrl = 6;
        $samlAcsUrl = 7;
        $samlSloUrl = 8;

        $expected = [
            'sp' => [
                'entityId' => $samlMetadataUrl,
                'assertionConsumerService' => ['url' => $samlAcsUrl],
                'singleLogoutService' => ['url' => $samlSloUrl],
                'NameIDFormat' => $samlNameId,
            ],
            'idp' => [
                'entityId' => $samlEntityId,
                'singleSignOnService' => ['url' => $samlSso],
                'singleLogoutService' => ['url' => $samsSlo],
                'x509cert' => $samlCertData,
            ],
            'security' => [
                'requestedAuthnContext' => false,
            ],
        ];

        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlEntityAdminhtml')
            ->willReturn($samlEntityId);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlNameIdAdminhtml')
            ->willReturn($samlNameId);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlSsoAdminhtml')
            ->willReturn($samlSso);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlSloAdminhtml')
            ->willReturn($samsSlo);
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlCertDataAdminhtml')
            ->willReturn($samlCertData);

        $this->urlMock
            ->expects($this->exactly(3))
            ->method('getUrl')
            ->will(
                $this->returnValueMap(
                    [
                        [Provider::PATH_SAML_METADATA, ['key' => false], $samlMetadataUrl],
                        [Provider::PATH_SAML_ACS, ['key' => false], $samlAcsUrl],
                        [Provider::PATH_SAML_SLO, ['key' => false], $samlSloUrl]
                    ]
                )
            );

        $result = $this->providerAdmin->get();

        $this->assertSame($result, $expected);
    }

    private function prepareMocks()
    {
        $this->urlMock = $this
            ->getMockBuilder(UrlInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['getUrl'])
            ->getMockForAbstractClass();

        $this->ssoConfigMock = $this
            ->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getSamlEntityFrontend',
                'getSamlNameIdFrontend',
                'getSamlSsoFrontend',
                'getSamlSloFrontend',
                'getSamlCertDataFrontend',
                'getSamlEntityAdminhtml',
                'getSamlNameIdAdminhtml',
                'getSamlSsoAdminhtml',
                'getSamlSloAdminhtml',
                'getSamlCertDataAdminhtml',
            ])
            ->getMock();
    }
}
