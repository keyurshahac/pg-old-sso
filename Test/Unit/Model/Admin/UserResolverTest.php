<?php
/**
 * Created by PhpStorm.
 * User: maciejbabolmac
 * Date: 21.02.2017
 * Time: 15:27
 */

namespace Pg\Sso\Test\Unit\Model\Admin;

use Pg\Sso\Model\Admin\UserResolver;
use Magento\Framework\App\Area;
use Magento\User\Model\ResourceModel\User\CollectionFactory;
use Magento\User\Model\ResourceModel\User\Collection;
use Magento\User\Model\User;
use Pg\Sso\Model\Config as SsoConfig;

class UserResolverTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var CollectionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $userCollectionFactoryMock;

    /**
     * @var Collection|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $userCollectionMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $ssoConfigMock;

    /**
     * @var UserResolver
     */
    protected $userResolver;

    /**
     * @var string
     */
    protected $testScope = "sso_attribute";


    /**
     * @var User
     */
    protected $user;

    protected function setUp()
    {
        $this->ssoConfigMock = $this->getMockBuilder(SsoConfig::class)->
        setMethods(['getConfig'])->
        disableOriginalConstructor()->
        getMock();

        $this->userCollectionFactoryMock = $this->getMockBuilder(CollectionFactory::class)->
        setMethods(['create'])->
        disableOriginalConstructor()->
        getMock();

        $this->userCollectionMock = $this->getMockBuilder(Collection::class)->
        setMethods(['addFieldToFilter', 'getFirstItem'])->
        disableOriginalConstructor()->
        getMock();

        $this->user = $this->getMockBuilder(User::class)->
        disableOriginalConstructor()->
        getMock();

        $this->userResolver = new UserResolver(
            $this->userCollectionFactoryMock,
            $this->ssoConfigMock,
            $this->testScope
        );

        parent::setUp();
    }


    public function testGetUser()
    {
        $testId = 666;
        $attributeName = "some_attribute";

        $this->ssoConfigMock->
        expects($this->once())->
        method("getConfig")->
        with(Area::AREA_ADMINHTML, $this->testScope)->
        will($this->returnValue($attributeName));

        $this->userCollectionFactoryMock->
        expects($this->once())->
        method("create")->
        will($this->returnValue($this->userCollectionMock));

        $this->userCollectionMock->
        expects($this->exactly(2))->
        method("addFieldToFilter")
            ->withConsecutive(
                [$attributeName, $testId],
                ['is_active', 1]
            )->will($this->returnSelf());

        $this->userCollectionMock->
        expects($this->once())->
        method("getFirstItem")->
        will($this->returnValue($this->user));

        $this->assertEquals($this->user, $this->userResolver->getUser($testId));
    }
}
