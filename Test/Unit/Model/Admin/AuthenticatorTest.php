<?php

namespace Pg\Sso\Unit\Test\Model\Admin;

use Pg\Sso\Model\Admin\Authenticator;
use Magento\Backend\Model\Auth\Session;
use Magento\User\Model\User;
use Magento\Framework\Event\ManagerInterface;
use Pg\Sso\Exception;

class AuthenticatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Authenticator
     */
    protected $authenticator;

    /**
     * @var ManagerInterface
     */
    protected $eventManager;


    protected function setUp()
    {
        $this->eventManager = $this->getMockBuilder(ManagerInterface::class)->
            setMethods(['dispatch'])->
            disableOriginalConstructor()->
            getMockForAbstractClass();

        $this->user = $this->getMockBuilder(User::class)->
            setMethods(['getId'])->
            disableOriginalConstructor()->
            getMock();

        $this->session = $this->getMockBuilder(Session::class)->
            setMethods(['setUser', 'processLogin', 'setSsoAuth'])->
            disableOriginalConstructor()->
            getMock();



        $this->authenticator = new Authenticator(
            $this->eventManager,
            $this->user,
            $this->session
        );

        parent::setUp();
    }

    /**
     *
     */
    public function testLogin(){
        $this->eventManager->
            expects($this->once())->
            method('dispatch')->
            with('backend_auth_user_login_success', [
                "user" => $this->user
            ]);

        $this->authenticator->login("username", null);
    }

    /**
     * @expectedException Exception
     */
    public function testAuthenticateException(){
        $this->user->expects($this->once())->method("getId")->will($this->returnValue(null));
        $this->authenticator->authenticate();
    }

    /**
     *
     */
    public function testAuthenticate(){
        $this->user->expects($this->once())->method("getId")->will($this->returnValue(123));

        $this->session->expects($this->once())->method("setUser")->with($this->user)->will($this->returnSelf());
        $this->session->expects($this->once())->method("processLogin")->will($this->returnSelf());
        $this->session->expects($this->once())->method("setSsoAuth")->with(true)->will($this->returnSelf());

        $this->assertTrue($this->authenticator->authenticate());
    }

}