<?php

namespace Pg\Sso\Test\Unit\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Store\Model\ScopeInterface;
use Pg\Sso\Model\Config;
use Pg\Sso\Model\Config\Source\Provider;

final class ConfigTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $scopeConfigMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->config = $om->getObject(
            Config::class,
            [
                'scopeConfig' => $this->scopeConfigMock,
            ]
        );

        parent::setUp();
    }

    public function testGetConfig()
    {
        $testKey = "adminhtml";
        $testField = "some_field";
        $expected = "saml";

        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->with("pg_sso/$testKey/$testField")
            ->will($this->returnValue($expected));

        $this->assertEquals($expected, $this->config->getConfig($testKey, $testField));
    }

    public function testIsSsoAdminEnabled()
    {
        $this->scopeConfigMock
            ->method('getValue')
            ->with("pg_sso/adminhtml/provider")
            ->will($this->onConsecutiveCalls("saml", "other", ""));
        /**
         * Test: saml value, other value, <empty> value,
         */
        $this->assertTrue($this->config->isSsoAdminEnabled());
        $this->assertTrue($this->config->isSsoAdminEnabled());
        $this->assertFalse($this->config->isSsoAdminEnabled());
    }

    public function testIsSsoAdminSamlEnabled()
    {
        $this->scopeConfigMock
            ->method('getValue')
            ->with("pg_sso/adminhtml/provider")
            ->will($this->onConsecutiveCalls("saml", "other", ""));
        /**
         * Test: saml value, other value, <empty> value,
         */
        $this->assertTrue($this->config->isSsoAdminSamlEnabled());
        $this->assertFalse($this->config->isSsoAdminSamlEnabled());
        $this->assertFalse($this->config->isSsoAdminSamlEnabled());
    }

    public function testKeepAdminSamlNativeLogin()
    {
        $this->scopeConfigMock
            ->expects($this->once())
            ->method("getValue")
            ->with("pg_sso/adminhtml/saml_keep_origin")
            ->will($this->returnValue(1));

        $this->assertTrue($this->config->keepAdminSamlNativeLogin());
    }

    public function testIsSloAdminSamlEnabled()
    {
        $this->scopeConfigMock
            ->expects($this->once())
            ->method("getValue")
            ->with("pg_sso/adminhtml/saml_slo")
            ->will($this->returnValue(1));

        $this->assertTrue($this->config->isSloAdminSamlEnabled());
    }

    /**
     * @todo refactor it to make simpler
     */
    public function testResetPasswordEnabledReturnsFalseWhenConfigurationReturnsZero()
    {
        $this->scopeConfigMock
            ->method('getValue')
            ->withConsecutive(
                [sprintf(
                    "%s/%s/%s",
                    Config::PREFIX,
                    Area::AREA_FRONTEND,
                    Config::XML_PATH_SAML_RESET_PASSWORD_ENABLED
                )],
                [sprintf(
                    "%s/%s/%s",
                    Config::PREFIX,
                    Area::AREA_FRONTEND,
                    Config::XML_PATH_SAML_RESET_PASSWORD_URL
                )],
                [sprintf(
                    "%s/%s/%s",
                    Config::PREFIX,
                    Area::AREA_FRONTEND,
                    Config::XML_PATH_SAML_KEEP_ORIGIN_LOGIN
                )]
            )
            ->willReturn('0');

        $this->assertFalse($this->config->isPasswordResetEnable());
    }

    public function testGetPasswordResetUrlReturnsConfigurationValue()
    {
        $passwordReset = '#PasswordReset#';
        $this->scopeConfigMock
            ->method('getValue')
            ->with('pg_sso/frontend/saml_reset_password_url')
            ->willReturn($passwordReset);

        $this->assertEquals($passwordReset, $this->config->getPasswordResetUrl());
    }

    public function testResetPasswordEnabledReturnsFalseWhenNoUrlConfigured()
    {
        $this->scopeConfigMock
            ->method('getValue')
            ->withConsecutive(
                ['pg_sso/frontend/saml_reset_password'],
                ['pg_sso/frontend/saml_reset_password_url']
            )
            ->willReturnOnConsecutiveCalls(['1', '']);

        $this->assertFalse($this->config->isPasswordResetEnable());
    }

    public function testResetPasswordEnabledReturnsTrueWhenConfigurationEnabledAndUrlDefined()
    {
        $this->scopeConfigMock->method('getValue')
            ->withConsecutive(
                ['pg_sso/frontend/saml_reset_password'],
                ['pg_sso/frontend/saml_reset_password_url']
            )
            ->willReturnOnConsecutiveCalls('1', '#PasswordReset#');

        $this->assertTrue($this->config->isPasswordResetEnable());
    }

    public function testResetPasswordEnabledReturnsFalseWhenKeepNativeLoginEnabled()
    {
        $this->scopeConfigMock
            ->method('getValue')
            ->withConsecutive(
                ['pg_sso/frontend/saml_reset_password'],
                ['pg_sso/frontend/saml_reset_password_url'],
                ['pg_sso/frontend/saml_keep_origin']
            )
            ->willReturnOnConsecutiveCalls('1', '#PasswordReset#', '1');

        $this->assertFalse($this->config->isPasswordResetEnable());
    }

    public function testIsSloFrontendEnabledWithDisabledSsoFrontendWillReturnFalse()
    {
        $this->scopeConfigMock
            ->expects($this->exactly(2))
            ->method('getValue')
            ->will($this->onConsecutiveCalls('', false));

        $result = $this->config->isSloFrontendEnabled();
        $this->assertFalse($result);
    }

    public function testIsSloFrontendEnabledWithEnabledSsoFrontendAndEmptyUrlWillReturnFalse()
    {
        $this->scopeConfigMock
            ->expects($this->exactly(2))
            ->method('getValue')
            ->will($this->onConsecutiveCalls('', Provider::SAML));

        $result = $this->config->isSloFrontendEnabled();
        $this->assertFalse($result);
    }

    public function testIsSloFrontendEnabledWithEnabledSsoFrontendAndUrlWillReturnTrue()
    {
        $this->scopeConfigMock
            ->expects($this->exactly(2))
            ->method('getValue')
            ->will($this->onConsecutiveCalls('some string', Provider::SAML));

        $result = $this->config->isSloFrontendEnabled();
        $this->assertTrue($result);
    }

    public function testGetSamlAttributeFrontendWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlAttributeFrontend();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlEntityFrontendWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlEntityFrontend();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlEntityAdminhtmlWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlEntityAdminhtml();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlNameIdFrontendWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlNameIdFrontend();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlNameIdAdminhtmlWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlNameIdAdminhtml();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlSsoFrontendWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlSsoFrontend();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlSsoAdminhtmlWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlSsoAdminhtml();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlSloFrontendWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlSloFrontend();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlSloAdminhtmlWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlSloAdminhtml();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlCertDataFrontendWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlCertDataFrontend();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlCertDataAdminhtmlWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlCertDataAdminhtml();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlTextFrontendWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlTextFrontend();
        $this->assertSame($result, $expected);
    }

    public function testGetSamlTextAdminhtmlWillReturnScopeConfigValue()
    {
        $expected = 1;
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expected);

        $result = $this->config->getSamlTextAdminhtml();
        $this->assertSame($result, $expected);
    }

    private function prepareMocks()
    {
        $this->scopeConfigMock = $this
            ->getMockBuilder(ScopeConfigInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['getValue'])
            ->getMockForAbstractClass();
    }

    public function testSSOEmailDomain()
    {
        $expectedValue = '@domain.com';
        $websiteId = 5;

        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedValue);

        $result = $this->config->getDomainSso($websiteId);

        $this->assertSame($expectedValue, $result);
    }
}
