<?php

namespace Pg\Sso\Test\Unit\Model\Config\Source;

use Pg\Sso\Model\Config\Source\Provider;

class ProviderTest extends \PHPUnit\Framework\TestCase
{
    public function testToOptionArray(){
        $testArray = [
            "value1"=>"label1",
            "value2"=>"label2"
        ];
        $model = new Provider($testArray);

        $this->assertEquals(
            $testArray,
            array_map(function($val){return (string)$val;}, $model->toOptionArray())
        );

    }
}