<?php

namespace Pg\Sso\Test\Unit\Model\Config\Source\Adminhtml;

use Pg\Sso\Model\Config\Source\Adminhtml\Attribute;

class AttributeTest extends \PHPUnit\Framework\TestCase
{
    public function testToOptionArray(){
        $model = new Attribute();
        $result = $model->toOptionArray();
        $this->assertArrayHasKey(Attribute::EMAIL, $result);
        $this->assertArrayHasKey(Attribute::USERNAME, $result);
        $this->assertArrayHasKey(Attribute::SSO_ID, $result);
    }
}