<?php

namespace Pg\Sso\Test\Unit\Model\Config\Source\Saml;

use Pg\Sso\Model\Config\Source\Saml\NameId;

class NameIdTest extends \PHPUnit\Framework\TestCase
{
    public function testToOptionArray(){
        $model = new NameId();
        $result = $model->toOptionArray();
        $this->assertArrayHasKey(NameId::NAMEID_UNSPECIFIED, $result);
        $this->assertArrayHasKey(NameId::NAMEID_TRANSIENT, $result);
        $this->assertArrayHasKey(NameId::NAMEID_PERSISTENT, $result);
        $this->assertArrayHasKey(NameId::NAMEID_EMAIL, $result);
    }
}