<?php

namespace Pg\Sso\Test\Unit\Model\Installer\AdminSso;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Model\Installer\AdminSso\Data;
use Magento\Framework\App\ResourceConnection;
use Pg\SampleData\Model\DataGetter;
use Psr\Log\LoggerInterface;
use Magento\User\Model\UserFactory;
use Magento\User\Model\User;

class DataTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Data
     */
    protected $testedObject;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnectionMock;

    /**
     * @var DataGetter
     */
    protected $dataGetterMock;

    /**
     * @var UserFactory
     */
    protected $userFactoryMock;

    /**
     * @var LoggerInterface
     */
    protected $loggerMock;

    /**
     * @var User
     */
    protected $userMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->testedObject = $om->getObject(
            Data::class,
            [
                'dataGetter' => $this->dataGetterMock,
                'resourceConnection' => $this->resourceConnectionMock,
                'userFactory' => $this->userFactoryMock,
                'logger' => $this->loggerMock,
            ]
        );
    }

    public function testRunWillCallLoadFromCsv()
    {
        $this->dataGetterMock
            ->expects($this->once())
            ->method('loadFromCSV')
            ->willReturn([]);
        $this->testedObject->run();
    }

    public function testRunWillCallUserGetAndSet()
    {
        $sapCode = 1;
        $ssoId = 123123;

        $this->dataGetterMock
            ->expects($this->once())
            ->method('loadFromCSV')
            ->willReturn([[Data::ROW_USER_ID => $sapCode, Data::ROW_PG_SSO_ID => $ssoId]]);

        $this->userFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($this->userMock);

        $this->userMock
            ->expects($this->once())
            ->method('load')
            ->with($sapCode)
            ->willReturn($this->userMock);

        $this->userMock
            ->expects($this->once())
            ->method('getPgSsoId')
            ->willReturn('345212');
//
//        $this->userMock
//            ->expects($this->once())
//            ->method('setPgSsoId')
//            ->with($ssoId);

//        $this->userMock
//            ->expects($this->once())
//            ->method('save');

        $this->testedObject->run();
    }

    public function testRunWithExceptionWillCallLogger()
    {
        $sapCode = 1;
        $ssoId = 123123;
        $this->dataGetterMock
            ->expects($this->once())
            ->method('loadFromCSV')
            ->willReturn([[Data::ROW_USER_ID => $sapCode, Data::ROW_PG_SSO_ID => $ssoId]]);
        $this->userFactoryMock
            ->expects($this->once())
            ->method('create')
            ->will($this->throwException(new \Exception('Error!')));
        $this->loggerMock
            ->expects($this->once())
            ->method('debug');
        $this->testedObject->run();
    }


    protected function prepareMocks()
    {
        $this->dataGetterMock = $this
            ->getMockBuilder(DataGetter::class)
            ->disableOriginalConstructor()
            ->setMethods(['loadFromCSV'])
            ->getMock();
        $this->resourceConnectionMock = $this
            ->getMockBuilder(ResourceConnection::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $this->userFactoryMock = $this
            ->getMockBuilder(UserFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->userMock = $this
            ->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->setMethods(['getPgSsoId', 'setPgSsoId', 'load'])
            ->getMock();
        $this->loggerMock = $this
            ->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
    }
}
