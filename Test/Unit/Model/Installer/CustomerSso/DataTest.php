<?php

namespace Pg\Sso\Test\Unit\Model\Installer\CustomerSso;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Model\Installer\CustomerSso\Data;
use Magento\Framework\App\ResourceConnection;
use Pg\SampleData\Model\DataGetter;
use Psr\Log\LoggerInterface;
use Magento\Customer\Model\Customer;
use Pg\Customer\Api\CustomerManagementInterface;
use Magento\Customer\Model\CustomerFactory;

class DataTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Data
     */
    protected $testedObject;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnectionMock;

    /**
     * @var DataGetter
     */
    protected $dataGetterMock;

    /**
     * @var CustomerManagementInterface
     */
    protected $customerManagementMock;

    /**
     * @var Customer
     */
    protected $customerMock;

    /**
     * @var LoggerInterface
     */
    protected $loggerMock;

    /**
     * @var CustomerFactory
     */
    protected $customerFactoryMock;

    protected function setUp()
    {
        $this->prepareMocks();
        $om = new ObjectManager($this);
        $this->testedObject = $om->getObject(
            Data::class,
            [
                'dataGetter' => $this->dataGetterMock,
                'resourceConnection' => $this->resourceConnectionMock,
                'customerManagement' => $this->customerManagementMock,
                'customerFactory' => $this->customerFactoryMock,
                'logger' => $this->loggerMock,
            ]
        );
    }

    public function testRunWillCallLoadFromCsv()
    {
        $this->dataGetterMock
            ->expects($this->once())
            ->method('loadFromCSV')
            ->willReturn([]);
        $this->testedObject->run();
    }

    public function testRunWillCallUserGetAndSet()
    {
        $sapCode = 1;
        $ssoId = 123123;
        $this->dataGetterMock
            ->expects($this->once())
            ->method('loadFromCSV')
            ->willReturn([[Data::ROW_ENTITY_ID => $sapCode, Data::ROW_SSO_ID => $ssoId]]);

        $this->customerFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($this->customerMock);

        $this->customerMock
            ->expects($this->once())
            ->method('load')
            ->willReturn($this->customerMock);

        $this->customerManagementMock
            ->expects($this->once())
            ->method('saveCustomerDataById');

        $this->testedObject->run();
    }

    public function testRunWillCallUserGet()
    {
        $sapCode = 1;
        $ssoId = 123123;
        $this->dataGetterMock
            ->expects($this->once())
            ->method('loadFromCSV')
            ->willReturn([[Data::ROW_ENTITY_ID => $sapCode, Data::ROW_SSO_ID => $ssoId]]);

        $this->customerFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($this->customerMock);

        $this->customerMock
            ->expects($this->once())
            ->method('load')
            ->willReturn($this->customerMock);


        $this->customerMock
            ->expects($this->once())
            ->method('getCustomerSsoId')
            ->willReturn('123123');

        $this->customerManagementMock
            ->expects($this->never())
            ->method('saveCustomerDataById');

        $this->testedObject->run();
    }

    public function testRunWithExceptionWillCallLogger()
    {
        $sapCode = 1;
        $ssoId = 123123;
        $this->dataGetterMock
            ->expects($this->once())
            ->method('loadFromCSV')
            ->willReturn([[Data::ROW_ENTITY_ID=> $sapCode, Data::ROW_SSO_ID => $ssoId]]);
        $this->customerFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($this->customerMock);
        $this->customerMock
            ->expects($this->once())
            ->method('load')
            ->will($this->throwException(new \Exception('Error!')));
        $this->loggerMock
            ->expects($this->once())
            ->method('debug');
        $this->testedObject->run();
    }


    protected function prepareMocks()
    {
        $this->dataGetterMock = $this
            ->getMockBuilder(DataGetter::class)
            ->disableOriginalConstructor()
            ->setMethods(['loadFromCSV'])
            ->getMock();
        $this->resourceConnectionMock = $this
            ->getMockBuilder(ResourceConnection::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $this->customerManagementMock = $this
            ->getMockBuilder(CustomerManagementInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $this->customerFactoryMock = $this
            ->getMockBuilder(CustomerFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMockForAbstractClass();
        $this->customerMock = $this
            ->getMockBuilder(Customer::class)
            ->disableOriginalConstructor()
            ->setMethods(['load', 'getCustomerSsoId'])
            ->getMockForAbstractClass();
        $this->loggerMock = $this
            ->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
    }
}
