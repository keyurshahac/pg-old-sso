<?php

namespace Pg\Sso\Unit\Test\Model\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Event\ManagerInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\State\UserLockedException;
use Pg\Sso\Exception\CustomerLogin;
use Pg\Sso\Model\Customer\Authenticator;

class AuthenticatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var AuthenticationInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $authMock;

    /**
     * @var ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $managerMock;

    /**
     * @var Session|\PHPUnit_Framework_MockObject_MockObject
     */
    private $sessionMock;

    /**
     * @var CustomerRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $customerRepositoryMock;


    private $sapManagementMock;
    /**
     * @var Customer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $customerMock;

    /**
     * @var Authenticator
     */
    private $authenticator;

    protected function setUp()
    {
        $this->authMock = $this->getMockBuilder(AuthenticationInterface::class)->getMock();
        $this->managerMock = $this->getMockBuilder(ManagerInterface::class)->getMock();
        $this->sessionMock = $this->getMockBuilder(Session::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)->getMock();
        $this->sapManagementMock = $this->getMockBuilder(\Pg\Customer\Api\SapManagementInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerMock = $this->getMockBuilder(Customer::class)->disableOriginalConstructor()->getMock();
        $this->authenticator = new Authenticator(
            $this->authMock,
            $this->managerMock,
            $this->sessionMock,
            $this->customerRepositoryMock,
            $this->sapManagementMock
        );
    }

    /**
     * @throws CustomerLogin
     * @throws UserLockedException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Pg\Customer\Exception\NoAccessToSAPException
     */
    public function testAuthenticateNoSapCustomer()
    {
        $customerId = 5;
        $this->customerMock->expects($this->exactly(3))->method('getId')->willReturn($customerId);
        $this->authMock->expects($this->once())->method('isLocked')->willReturn(false);
        $this->managerMock
            ->expects($this->never())
            ->method('dispatch')
            ->with('customer_data_object_login', ['customer' => $this->customerMock]);
        $customerData = new \stdClass();
        $this->customerRepositoryMock
            ->expects($this->never())
            ->method('getById')
            ->with($customerId)
            ->willReturn($customerData);

        $this->sapManagementMock
            ->expects($this->once())
            ->method('getSapIdsByCustomerId')
            ->with($customerId)
            ->willReturn(null);

        $this->sessionMock
            ->expects($this->never())
            ->method('setCustomerDataAsLoggedIn')
            ->with($customerData);

        $this->expectException(\Pg\Customer\Exception\NoAccessToSAPException::class);
        $result = $this->authenticator->authenticate($this->customerMock);
    }

    /**
     * @throws CustomerLogin
     * @throws UserLockedException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Pg\Customer\Exception\NoAccessToSAPException
     */
    public function testAuthenticateSapCustomer()
    {
        $customerId = 5;
        $this->customerMock->expects($this->exactly(4))->method('getId')->willReturn($customerId);
        $this->authMock->expects($this->once())->method('isLocked')->willReturn(false);
        $this->managerMock
            ->expects($this->once())
            ->method('dispatch')
            ->with('customer_data_object_login', ['customer' => $this->customerMock]);
        $customerData = new \stdClass();
        $this->customerRepositoryMock
            ->expects($this->once())
            ->method('getById')
            ->with($customerId)
            ->willReturn($customerData);

        $this->sessionMock
            ->expects($this->once())
            ->method('setCustomerDataAsLoggedIn')
            ->with($customerData);

        $this->sapManagementMock
            ->expects($this->once())
            ->method('getSapIdsByCustomerId')
            ->with($customerId)
            ->willReturn([1]);

        $result = $this->authenticator->authenticate($this->customerMock);
        $this->assertSame($this->customerMock, $result);
    }

    /**
     * @throws CustomerLogin
     * @throws UserLockedException
     */
    public function testNoCustomer()
    {
        $this->customerMock->expects($this->once())->method('getId')->willReturn(null);
        $this->expectException(CustomerLogin::class);
        $this->authenticator->authenticate($this->customerMock);
    }

    /**
     * @throws CustomerLogin
     * @throws UserLockedException
     */
    public function testLockedCustomer()
    {
        $this->customerMock->expects($this->exactly(2))->method('getId')->willReturn(5);
        $this->authMock->expects($this->once())->method('isLocked')->willReturn(true);
        $this->expectException(UserLockedException::class);
        $this->authenticator->authenticate($this->customerMock);
    }

    public function testLogout()
    {
        $this->sessionMock->expects($this->once())->method('logout');
        $this->authenticator->logout();
    }
}
