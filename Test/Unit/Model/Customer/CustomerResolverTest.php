<?php

namespace Pg\Sso\Unit\Test\Model\Customer;

use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Pg\Sso\Exception\TooMuchEntities;
use Pg\Sso\Model\Config as SsoConfig;
use Pg\Sso\Model\Customer\CustomerResolver;

class CustomerResolverTest extends \PHPUnit\Framework\TestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $customerCollectionFactoryMock;

    /**
     * @var SsoConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $ssoConfigMock;

    /** @var StoreManagerInterface | \PHPUnit_Framework_MockObject_MockObject */
    private $storeManager;

    /** @var CustomerResolver */
    private $testedObject;

    protected function setUp()
    {
        $om = new ObjectManager($this);
        $this->customerCollectionFactoryMock = $this->getMockBuilder(CustomerCollectionFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->ssoConfigMock = $this->getMockBuilder(SsoConfig::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->storeManager = $this->getMockBuilder(StoreManagerInterface::class)->getMock();

        $this->testedObject = $om->getObject(
            CustomerResolver::class,
            [
                'customerCollectionFactory' => $this->customerCollectionFactoryMock,
                'ssoConfig' => $this->ssoConfigMock,
                'storeManager' => $this->storeManager
            ]
        );
    }

    /**
     * @throws LocalizedException
     */
    public function testGetById()
    {
        $field = 'someFieldName';
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlAttributeFrontend')
            ->willReturn($field);
        $collection = $this->getMockBuilder(AbstractCollection::class)
            ->setMethods(['addAttributeToSelect', 'addAttributeToFilter', 'load', 'getFirstItem', 'count'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->customerCollectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collection);

        $collection->expects($this->once())->method('addAttributeToSelect')->willReturnSelf();
        $collection
            ->expects($this->once())
            ->method('addAttributeToFilter')
            ->with($field, 5)
            ->willReturnSelf();

        $collection->expects($this->atLeastOnce())->method('count')->willReturn(1);
        $collection->expects($this->once())->method('getFirstItem');

        $this->testedObject->getCustomer(5);
    }

    /**
     * @throws LocalizedException
     */
    public function testFoundMore()
    {
        $field = 'someFieldName';
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlAttributeFrontend')
            ->willReturn($field);
        $collection = $this->getMockBuilder(AbstractCollection::class)
            ->setMethods(['addAttributeToSelect', 'addAttributeToFilter', 'load', 'getFirstItem', 'count'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->customerCollectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collection);

        $collection->expects($this->once())->method('addAttributeToSelect')->willReturnSelf();
        $collection
            ->expects($this->once())
            ->method('addAttributeToFilter')
            ->with($field, 5)
            ->willReturnSelf();

        $collection->expects($this->atLeastOnce())->method('count')->willReturn(2);

        $this->expectException(TooMuchEntities::class);
        $this->testedObject->getCustomer(5);
    }

    /**
     * @throws LocalizedException
     */
    public function testFoundNone()
    {
        $field = 'someFieldName';
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlAttributeFrontend')
            ->willReturn($field);
        $collection = $this->getMockBuilder(AbstractCollection::class)
            ->setMethods(['addAttributeToSelect', 'addAttributeToFilter', 'load', 'getFirstItem', 'count'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->customerCollectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collection);

        $collection->expects($this->once())->method('addAttributeToSelect')->willReturnSelf();
        $collection
            ->expects($this->once())
            ->method('addAttributeToFilter')
            ->with($field, 5)
            ->willReturnSelf();

        $collection->expects($this->atLeastOnce())->method('count')->willReturn(0);

        $this->expectException(NoSuchEntityException::class);
        $this->testedObject->getCustomer(5);
    }

    /**
     * @throws LocalizedException
     */
    public function testCustomerFilteredByWebsite()
    {
        $field = 'someFieldName';
        $this->ssoConfigMock
            ->expects($this->once())
            ->method('getSamlAttributeFrontend')
            ->willReturn($field);
        $collection = $this->getMockBuilder(AbstractCollection::class)
            ->setMethods(['addAttributeToSelect', 'addAttributeToFilter', 'load', 'getFirstItem', 'count'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->customerCollectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collection);

        $collection->expects($this->exactly(2))->method('addAttributeToSelect')
            ->withConsecutive([$field], ['website_id'])->willReturnSelf();
        $collection
            ->expects($this->exactly(2))
            ->method('addAttributeToFilter')
            ->withConsecutive([$field, 5], ['website_id', 7])
            ->willReturnSelf();

        $collection->expects($this->atLeastOnce())->method('count')->willReturn(1);
        $collection->expects($this->once())->method('getFirstItem');
        $store = $this->getMockBuilder(StoreInterface::class)
            ->setMethods(['getWebsiteId'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->storeManager->expects($this->once())->method('getStore')->willReturn($store);
        $store->expects($this->once())->method('getWebsiteId')->willReturn(7);
        $this->testedObject->getCustomer(5);
    }
}
