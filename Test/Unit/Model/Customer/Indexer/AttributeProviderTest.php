<?php

namespace Pg\Sso\Test\Unit\Model\Customer\Indexer;

use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Pg\Sso\Model\Config\Source\Attribute;
use Pg\Sso\Model\Customer\Indexer\AttributeProvider;
use \PHPUnit\Framework\TestCase;
use \PHPUnit\Framework\MockObject\MockObject as MockObject;
use Psr\Log\LoggerInterface;

/**
 * Class AttributeProviderTest
 * @package Pg\Sso\Test\Unit\Model\Customer\Indexer
 * @covers \Pg\Sso\Model\Customer\Indexer\AttributeProvider
 */
class AttributeProviderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var AttributeProvider | MockObject
     */
    protected $attributeProvider;

    /**
     * @var EavConfig | MockObject
     */
    protected $eavConfigMock;

    /**
     * @var LoggerInterface | MockObject
     */
    protected $loggerMock;

    /**
     * Setup tests
     */
    protected function setUp()
    {
        parent::setUp();

        $this->setUpMocks();

        $om = new ObjectManager($this);
        $this->attributeProvider = $om->getObject(AttributeProvider::class, [
            'eavConfig' => $this->eavConfigMock,
            'logger' => $this->loggerMock
        ]);
    }

    /**
     * @covers \Pg\Sso\Model\Customer\Indexer\AttributeProvider::addDynamicData()
     * @covers \Pg\Sso\Model\Customer\Indexer\AttributeProvider::existingAttributeNames()
     * @covers \Pg\Sso\Model\Customer\Indexer\AttributeProvider::filterOutNotExistingFields()
     */
    public function testAddDynamicDataOK()
    {
        $expectedFields = [Attribute::SSO_ID => [], 'another' => []];
        $data = ['fields' => [Attribute::SSO_ID => [], 'another' => []]];

        $attributeMock = $this->getMockBuilder(\Magento\Customer\Model\Attribute::class)
            ->disableOriginalConstructor()
            ->getMock();
        $attributeMock->expects($this->once())
            ->method('getName')
            ->willReturn(Attribute::SSO_ID);

        $attributeCollectionMock = $this->getMockBuilder(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection::class)
            ->disableOriginalConstructor()
            ->setMethods(['getItems'])
            ->getMockForAbstractClass();
        $attributeCollectionMock->expects($this->once())
            ->method('getItems')
            ->willReturn([$attributeMock]);

        $entityTypeMock = $this->getMockBuilder(\Magento\Eav\Model\Entity\Type::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entityTypeMock->expects($this->once())
            ->method('getAttributeCollection')
            ->willReturn($attributeCollectionMock);


        $this->eavConfigMock->expects($this->once())
            ->method('getEntityType')
            ->willReturn($entityTypeMock);

        $actual = $this->attributeProvider->addDynamicData($data);
        $this->assertSame($expectedFields, $actual['fields']);
    }

    /**
     * @covers \Pg\Sso\Model\Customer\Indexer\AttributeProvider::addDynamicData()
     * @covers \Pg\Sso\Model\Customer\Indexer\AttributeProvider::existingAttributeNames()
     * @covers \Pg\Sso\Model\Customer\Indexer\AttributeProvider::filterOutNotExistingFields()
     */
    public function testAddDynamicDataNOSSOID()
    {
        $expectedFields = ['another' => []];
        $data = ['fields' => [Attribute::SSO_ID => [], 'another' => []]];

        $attributeMock = $this->getMockBuilder(\Magento\Customer\Model\Attribute::class)
            ->disableOriginalConstructor()
            ->getMock();
        $attributeMock->expects($this->once())
            ->method('getName')
            ->willReturn('another');

        $attributeCollectionMock = $this->getMockBuilder(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection::class)
            ->disableOriginalConstructor()
            ->setMethods(['getItems'])
            ->getMockForAbstractClass();
        $attributeCollectionMock->expects($this->once())
            ->method('getItems')
            ->willReturn([$attributeMock]);

        $entityTypeMock = $this->getMockBuilder(\Magento\Eav\Model\Entity\Type::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entityTypeMock->expects($this->once())
            ->method('getAttributeCollection')
            ->willReturn($attributeCollectionMock);

        $this->eavConfigMock->expects($this->once())
            ->method('getEntityType')
            ->willReturn($entityTypeMock);

        $this->loggerMock->expects($this->once())
            ->method('warning')
            ->with('Filtered out not existing attribute from list', ['attribute_name' => Attribute::SSO_ID]);

        $actual = $this->attributeProvider->addDynamicData($data);
        $this->assertSame($expectedFields, $actual['fields']);
    }

    /**
     * Set up mocks :)
     */
    protected function setUpMocks()
    {
        $this->eavConfigMock = $this->getMockBuilder(EavConfig::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->setMethods(['warning'])
            ->getMockForAbstractClass();
    }
}