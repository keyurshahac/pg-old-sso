<?php
namespace Pg\Sso\Logger;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Pg\Logging\Api\LogCleanerInterface;
use Pg\Utils\Symfony\Component\Finder\Finder;
use Pg\Utils\Symfony\Component\Finder\FinderFactory;
use Magento\Framework\Filesystem\Io\File as IoFile;
use Magento\Framework\App\Filesystem\DirectoryList;

class Handler extends StreamHandler implements LogCleanerInterface
{
    /** Log directory */
    const DIRECTORY = '/var/log/sso/';

    /** Log file extension */
    const FILE_EXTENSION = '.log';

    /** Date pattern used to name files */
    const FILE_NAME_DATE_PATTERN = 'Y_MM_dd';

    /** @var TimezoneInterface */
    private $timezone;

    /** @var int Logging level */
    private $loggerType = Logger::WARNING;

    /** @var FinderFactory */
    private $finderFactory;

    /** @var IoFile */
    private $ioFile;

    /** @var DirectoryList  */
    private $directoryList;

    /**
     * Handler constructor
     * @param TimezoneInterface $timezone
     * @param FinderFactory $finderFactory
     * @param IoFile $ioFile
     * @param DirectoryList $directoryList
     * @param null $filePath
     */
    public function __construct(
        TimezoneInterface $timezone,
        FinderFactory $finderFactory,
        IoFile $ioFile,
        DirectoryList $directoryList,
        $filePath = null
    ) {
        $this->timezone = $timezone;
        $this->finderFactory = $finderFactory;
        $this->ioFile = $ioFile;
        $this->directoryList = $directoryList;

        $filename = $this->getFileName();

        parent::__construct(
            $filePath ? $filePath . $filename : BP . $filename,
            $this->loggerType
        );
        $this->setFormatter(new LineFormatter(null, null, true));
    }

    /**
     * @return string
     */
    private function getFileName() : string
    {
        $dateString = $this->timezone->formatDateTime(
            $this->timezone->date(),
            null,
            null,
            null,
            null,
            static::FILE_NAME_DATE_PATTERN
        );

        return sprintf(
            '%s%s%s',
            static::DIRECTORY,
            $dateString,
            static::FILE_EXTENSION
        );
    }

    /**
     * @inheritdoc
     */
    public function clean(int $days)
    {
        $path = $this->directoryList->getPath(DirectoryList::ROOT) . static::DIRECTORY;

        if (!$this->ioFile->fileExists($path, false)) {
            return true;
        }

        /** @var Finder $finder */
        $finder = $this->finderFactory->create();
        $finder
            ->files()
            ->depth(0)
            ->date(sprintf('until %d days ago', $days))
            ->in($path);

        foreach ($finder as $file) {
            $this->ioFile->rm($file);
        }

        return true;
    }
}