<?php

namespace Pg\Sso\Plugin\Action\Adminhtml;

use Magento\Backend\Controller\Adminhtml\Auth\Login as BackendLogin;
use Magento\Framework\Controller\Result\RedirectFactory;
use Pg\Sso\Model\Config as SsoConfig;

class Login
{
    const PATH_SAML_SSO_LOGIN = 'pg_sso/saml/ssologin';
    /**
     * @var $resultRedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * Login constructor.
     * @param RedirectFactory $resultRedirectFactory
     * @param SsoConfig $ssoConfg
     */
    public function __construct(
        RedirectFactory $resultRedirectFactory,
        SsoConfig $ssoConfg
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->ssoConfig = $ssoConfg;
    }

    /**
     * @param BackendLogin $subject
     * @param callable $proceed
     * @param array ...$args
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function aroundExecute(BackendLogin $subject, callable $proceed, ...$args)
    {
        $doSamlRedirect = $this->ssoConfig->isSsoAdminSamlEnabled() && !$this->ssoConfig->keepAdminSamlNativeLogin();
        if ($doSamlRedirect) {
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath(static::PATH_SAML_SSO_LOGIN);
            return $redirect;
        }

        return $proceed(...$args);
    }
}
