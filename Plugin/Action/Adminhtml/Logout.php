<?php

namespace Pg\Sso\Plugin\Action\Adminhtml;

use Magento\Backend\Controller\Adminhtml\Auth\Logout as BackendLogout;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Pg\Sso\Model\Config as SsoConfig;

class Logout
{
    const PATH_SSO_SAML_LOGOUT = 'pg_sso/saml/ssologout';
    const PATH_SSO_SAML_LOGGED_OUT = 'pg_sso/saml/loggedout';
    /**
     * @var $resultRedirectFactory
     */
    private $resultRedirectFactory;

    /**
     * @var SsoConfig;
     */
    private $ssoConfig;

    /**
     * Logout constructor.
     * @param RedirectFactory $resultRedirectFactory
     * @param SsoConfig $ssoConfig
     */
    public function __construct(
        RedirectFactory $resultRedirectFactory,
        SsoConfig $ssoConfig
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->ssoConfig = $ssoConfig;
    }

    /**
     * @param BackendLogout $subject
     * @param callable $proceed
     * @param array ...$args
     * @return callable|Redirect
     */
    public function aroundExecute(BackendLogout $subject, callable $proceed, ...$args)
    {
        if (!$this->ssoConfig->isSsoAdminEnabled()) {
            return $proceed(...$args);
        }
        /**
         * @var Redirect
         */
        $redirect = $this->resultRedirectFactory->create();
        // In case of single logout redirect to slo mechanism
        if ($this->ssoConfig->isSloAdminSamlEnabled()) {
            return $redirect->setPath(static::PATH_SSO_SAML_LOGOUT);
        }
        // Other way just logout and redirect to loggedout page
        $proceed(...$args);

        return $redirect->setPath(static::PATH_SSO_SAML_LOGGED_OUT);
    }
}
