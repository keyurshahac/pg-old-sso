<?php

namespace Pg\Sso\Plugin\Action;

use Magento\Customer\Controller\Account\Logout as OldLogout;
use Magento\Framework\Controller\Result\Redirect;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Framework\Controller\Result\RedirectFactory;

class Logout
{
    const PATH_SAML_SSO_LOGOUT = 'pg_sso/saml/ssologout';
    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * @var $resultRedirectFactory
     */
    private $resultRedirectFactory;

    /**
     * Logout constructor.
     * @param SsoConfig $ssoConfig
     * @param RedirectFactory $resultRedirectFactory
     */
    public function __construct(
        SsoConfig $ssoConfig,
        RedirectFactory $resultRedirectFactory
    ) {
        $this->ssoConfig = $ssoConfig;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    /**
     * @param OldLogout $subject
     * @param callable $proceed
     * @param array ...$args
     * @return Redirect|callable
     */
    public function aroundExecute(OldLogout $subject, callable $proceed, ...$args)
    {
        if ($this->ssoConfig->isSloFrontendEnabled()) {
            /**
             * @var Redirect $redirect
             */
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath(static::PATH_SAML_SSO_LOGOUT);

            return $redirect;
        }

        return $proceed(...$args);
    }
}
