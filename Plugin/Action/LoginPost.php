<?php

namespace Pg\Sso\Plugin\Action;

use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Customer\Controller\Account\LoginPost as OldLogin;
use Pg\Sso\Model\Config as SsoConfig;

class LoginPost
{
    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * @var RedirectFactory
     */
    private $resultRedirectFactory;

    /** LoginPost constructor.
     * @param SsoConfig $ssoConfig
     * @param RedirectFactory $resultRedirectFactory
     */
    public function __construct(
        SsoConfig $ssoConfig,
        RedirectFactory $resultRedirectFactory
    ) {
        $this->ssoConfig = $ssoConfig;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    public function aroundExecute(OldLogin $subject, callable $proceed, ...$args)
    {
        if ($this->ssoConfig->isSsoFrontendEnabled() &&
            false === $this->ssoConfig->keepFrontendSamlNativeLogin()) {
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('pg_sso/saml/ssologin');

            return $redirect;
        }

        return $proceed(...$args);
    }
}
