<?php

namespace Pg\Sso\Plugin\Model\Url;

use Magento\Customer\Model\Url;
use Pg\Sso\Model\Config as SsoConfig;

class ReplacePasswordResetUrl
{
    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * ReplacePasswordResetUrl constructor.
     * @param SsoConfig $ssoConfig
     */
    public function __construct(SsoConfig $ssoConfig)
    {
        $this->ssoConfig = $ssoConfig;
    }

    /**
     * @param Url $context
     * @param $forgotPasswordUrl
     * @return string
     */
    public function afterGetForgotPasswordUrl(Url $context, $forgotPasswordUrl)
    {
        return ($this->ssoConfig->isSsoFrontendEnabled() && $this->ssoConfig->isPasswordResetEnable()) ?
            $this->ssoConfig->getPasswordResetUrl() : $forgotPasswordUrl;
    }
}
