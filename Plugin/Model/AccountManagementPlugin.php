<?php

namespace Pg\Sso\Plugin\Model;

use Pg\Sso\Helper\Auth as SsoHelper;
use Magento\Customer\Api\Data\CustomerInterface;
use Pg\PasswordManagement\Model\Customer\AccountManagement;
use Pg\Sso\Model\Config as SsoConfig;

class AccountManagementPlugin
{
    /**
     * @var SsoHelper
     */
    private $ssoHelper;

    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    /**
     * AccountManagementPlugin constructor.
     * @param SsoHelper $ssoHelper
     * @param SsoConfig $ssoConfig
     */
    public function __construct(
        SsoHelper $ssoHelper,
        SsoConfig $ssoConfig
    ) {
        $this->ssoHelper = $ssoHelper;
        $this->ssoConfig = $ssoConfig;
    }

    /**
     * Prevent adding customer_entity.rp_token to database if we're dealing with SSO-only customer.
     *
     * @param AccountManagement $subject
     * @param callable $proceed
     * @param CustomerInterface $customer
     * @param $passwordLinkToken
     * @return bool
     */
    public function aroundChangeResetPasswordLinkToken(
        AccountManagement $subject,
        callable $proceed,
        CustomerInterface $customer,
        $passwordLinkToken
    ) {

        $loginType = $this->ssoConfig->getFrontendLoginType($customer->getWebsiteId());

        if ($loginType === SsoConfig::LOGIN_TYPE_NATIVE_ONLY ||
            ($loginType === SsoConfig::LOGIN_TYPE_BOTH && !$this->ssoHelper->isSsoOnlyCustomer($customer))) {
            return $proceed($customer, $passwordLinkToken);
        }

        return true;
    }
}
