<?php

namespace Pg\Sso\Plugin\Observer;

use Magento\Customer\Model\Session as CustomerSession;
use Pg\PasswordManagement\Observer\CheckCustomerPasswordExpiration;

/**
 * Class CheckCustomerPasswordExpirationPlugin
 * @package   Pg\Sso\Plugin\Observer
 * @copyright Copyright (c) 2018 Orba Sp. z o.o.  (http://orba.co)
 */
class CheckCustomerPasswordExpirationPlugin
{
    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var \Pg\PasswordManagement\Helper\Password\Validation\Expiration
     */
    protected $passwordExpiration;

    /**
     * CheckCustomerPasswordExpirationPlugin constructor.
     *
     * @param CustomerSession $customerSession
     * @param \Pg\PasswordManagement\Helper\Password\Validation\Expiration $passwordExpiration
     */
    public function __construct(
        CustomerSession $customerSession,
        \Pg\PasswordManagement\Helper\Password\Validation\Expiration $passwordExpiration
    ) {
        $this->customerSession = $customerSession;
        $this->passwordExpiration = $passwordExpiration;
    }

    /**
     * @see \Pg\PasswordManagement\Observer\CheckCustomerPasswordExpiration::execute()
     *
     * @param CheckCustomerPasswordExpiration $subject
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function beforeExecute(
        CheckCustomerPasswordExpiration $subject,
        \Magento\Framework\Event\Observer $observer
    ) {
        /** @var \Magento\Customer\Model\Customer $customer */
        $customer = $observer->getEvent()->getModel();
        if ($this->passwordExpiration->isExpired($customer) && !$this->isCustomerLoggedBySso($customer)) {
            // Set password expired flag
            $this->customerSession->setData(CheckCustomerPasswordExpiration::PASSWORD_IS_EXPIRED_FLAG, true);
        }
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return bool
     */
    private function isCustomerLoggedBySso($customer)
    {
        $attributeValue = $customer->getCustomAttribute('customer_sso_id');
        return ($attributeValue && !empty($attributeValue->getValue()));
    }
}
