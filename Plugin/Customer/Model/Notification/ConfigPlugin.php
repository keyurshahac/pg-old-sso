<?php

namespace Pg\Sso\Plugin\Customer\Model\Notification;

use Pg\Customer\Model\Notification\Config;
use Pg\Sso\Model\Config as SsoConfig;

class ConfigPlugin
{
    /**
     * @var SsoConfig
     */
    private $ssoConfig;

    public function __construct(SsoConfig $ssoConfig)
    {
        $this->ssoConfig = $ssoConfig;
    }

    /**
     * Overrides logic responsible for sending "Set Password" link to new customer
     * @param Config $context
     * @param callable $proceed
     * @param $websiteId
     * @return bool
     */
    public function aroundIsSendPasswordSetLinkEnabled(Config $context, callable $proceed, $websiteId)
    {
        if (!$this->ssoConfig->isSsoFrontendEnabled($websiteId)) {
            return true;
        }
        if (!$this->ssoConfig->keepFrontendSamlNativeLogin($websiteId)) {
            return false;
        }

        return $proceed($websiteId);
    }
}