<?php

namespace Pg\Sso\Plugin\Block\Adminhtml\Customer\Edit;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Pg\Sso\Model\Config as SsoConfig;
use Magento\Customer\Block\Adminhtml\Edit\ResetPasswordButton as OldButton;
use Magento\Customer\Block\Adminhtml\Edit\GenericButton;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class ResetPasswordButton extends GenericButton
{
    /** @var SsoConfig */
    private $ssoConfig;

    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /**
     * ResetPasswordButton constructor.
     * @param SsoConfig $ssoConfig
     * @param CustomerRepositoryInterface $customerFactory
     * @param Context $context
     * @param Registry $registry
     */
    public function __construct(
        SsoConfig $ssoConfig,
        CustomerRepositoryInterface $customerFactory,
        Context $context,
        Registry $registry
    ) {
        parent::__construct($context, $registry);
        $this->ssoConfig = $ssoConfig;
        $this->customerRepository = $customerFactory;
    }

    /**
     * @param OldButton $subject
     * @param callable $proceed
     * @param array ...$args
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function aroundGetButtonData(OldButton $subject, callable $proceed, ...$args)
    {
        if (!$this->getCustomerId()) {
            return [];
        }

        $customer = $this->customerRepository->getById($this->getCustomerId());

        if (false === $this->ssoConfig->isPasswordResetEnable($customer->getWebsiteId()) &&
            $this->ssoConfig->isSsoFrontendEnabled()
        ) {
            return [];
        }

        return $proceed(...$args);
    }
}
