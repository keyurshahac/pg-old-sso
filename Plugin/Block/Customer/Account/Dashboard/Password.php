<?php

namespace Pg\Sso\Plugin\Block\Customer\Account\Dashboard;

use Pg\Customer\Block\Account\Dashboard\Password as OriginalPassword;
use Pg\Sso\Helper\Auth as SsoHelper;

/**
 * Class Password
 *
 * @package   Pg\Sso\Plugin\Block\Adminhtml\Customer\Edit
 * @author    Viktoriia Sultanovska (viktoriia.sultanovska@orba.co)
 * @copyright Copyright (c) Orba Sp. z o.o.  (http://orba.co)
 */
class Password
{
    /** @var SsoHelper */
    protected $ssoHelper;

    /**
     * Password constructor.
     * @param SsoHelper $ssoHelper
     */
    public function __construct(
        SsoHelper $ssoHelper
    ) {
        $this->ssoHelper = $ssoHelper;
    }

    /**
     * @param OriginalPassword $subject
     * @param $result
     *
     * @see \Pg\Customer\Block\Account\Dashboard\Password::toHtml()
     *
     * @return bool
     */
    public function afterToHtml(OriginalPassword $subject, $result)
    {
        return $this->ssoHelper->getSsoFlag() ?
            false : $result;
    }
}
