<?php
namespace Pg\Sso\Exception;

use Pg\Sso\Exception;
use Pg\Utils\Exception\ReasonableInterface;
use Pg\Utils\Exception\ReasonableTrait;

/**
 * Class SamlException
 * @package Pg\Sso\Exception
 */
class SamlException extends Exception implements ReasonableInterface
{
    use ReasonableTrait;
}
