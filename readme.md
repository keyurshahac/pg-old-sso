# Magento 2 - PG Sso Module
Allow use SSO concept to authenticate a user in Magento system. Many providers should be supported (saml, openid, etc...) on multiple areas (front/admin/scope)

`Magento version 2.1.2 EE`

## 1. Module status
### 1.1. Backend part
 
 
__COMPLETED:__
 
 - None

__TODO:__

 - Module config:
   - SSO For backend dropdown with: none, saml options for now
   - SAML details
   - Global scope
 - Add saml_sso_id to admin users entity
 - Add saml_sso_id to admin form
 - Add saml_sso_id to admin grid
 - Add SAML link to admin login form
 - Override admin login action
 - Add admin metadata action
 - Allow SAML actions for not logged in user in admin scope
 - Add needed source models for SAML form
 - Add 3rd part SAML library
 - Add SAML SLO and SSO support
 - Add failed login logs
 - Handling requests using SAML protocol
 - Provide mocked IdP to test SSO/SLO
   
### 1.2. Frontend part

__TODO: Nothing for now__


## 2. Adding new admin user based on SAML login

TODO


### Translations
 
Available translations: en_US, es_MX, it_IT, el_GR.