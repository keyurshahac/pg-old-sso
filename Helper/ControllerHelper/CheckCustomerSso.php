<?php
/**
 * Created by PhpStorm.
 * User: ksulek
 * Date: 16.10.18
 * Time: 12:15
 */

namespace Pg\Sso\Helper\ControllerHelper;


class CheckCustomerSso
{

    /**
     * @param string $email
     * @param string $domainSso
     * @return bool
     */

    public function checkCustomerSsoId(string $email, string $domainSso)
    {
        return (bool)preg_match('/' . $domainSso . '$/', $email, $matches, PREG_OFFSET_CAPTURE);
    }
}