<?php

namespace Pg\Sso\Helper;

use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Api\AttributeInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Pg\Sso\Model\Config\Source\Attribute;

/**
 * Class Auth
 * @package   Pg\Sso\Helper
 * @copyright Copyright (c) 2018 Orba Sp. z o.o.  (http://orba.co)
 */
class Auth
{
    /** @var SessionManagerInterface */
    protected $session;

    /**
     * Auth constructor.
     *
     * @param SessionManagerInterface $session
     */
    public function __construct(
        SessionManagerInterface $session
    ) {
        $this->session = $session;
    }

    /**
     * @param bool $isSso
     */
    public function setSsoFlag($isSso = true)
    {
        $this->session->setSsoFlag($isSso);
    }

    /**
     * @return bool
     */
    public function getSsoFlag()
    {
        return $this->session->getSsoFlag() ?? false;
    }

    /**
     * @param CustomerInterface $customer
     *
     * @return bool
     */
    public function isSsoOnlyCustomer(CustomerInterface $customer): bool
    {
        return $this->getSsoId($customer) != '';
    }

    /**
     * @param CustomerInterface $customer
     *
     * @return string
     */
    protected function getSsoId(CustomerInterface $customer): string
    {
        $ssoId = $customer->getCustomAttribute(Attribute::SSO_ID);
        if ($ssoId instanceof AttributeInterface) {
            return (string) $ssoId->getValue();
        } else {
            return '';
        }
    }
}
